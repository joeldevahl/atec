local ep = string.gsub(ModuleFilename(), "build.lua", "")
Import(PathJoin(ep, "build/engine.lua"))

AddConfig("debug", "configs/debug.lua")
AddConfig("release", "configs/release.lua")

if engine.host.family == "unix" then
	if engine.host.platform == "linux" then
		AddTarget("linux_x86_64", "targets/linux_x86_64.lua")
	elseif engine.host.platform == "macosx" then
		AddTarget("osx_x86_64", "targets/osx_x86_64.lua")
	end
elseif engine.host.family == "windows" then
	AddTarget("winx64", "targets/winx64.lua")
end

AddStep("init", "steps/init.lua")
AddStep("addtools", "steps/addtools.lua")
AddStep("patch", "steps/patch.lua")
AddStep("build", "steps/build.lua")
AddStep("buildjob", "steps/buildjob.lua")
AddStep("buildtest", "steps/buildtest.lua")

function AddATECDefault()
	AddUnitDir(PathJoin(engine.path, "units"))
	AddUnitDir(PathJoin(engine.path, "externals"))
end
