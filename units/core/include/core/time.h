#ifndef CORE_TIME_H
#define CORE_TIME_H

#include <core/types.h>

uint64_t time_current();
uint64_t time_frequency();

#endif // CORE_TIME_H
