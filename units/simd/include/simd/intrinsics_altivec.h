#ifndef SIMD_INTRINSICS_ALTIVEC_H
#define SIMD_INTRINSICS_ALTIVEC_H

#ifdef __cplusplus
extern "C" {
#endif

#define simd_mask(x, y, z, w) simd_load_uint32(x * 0xffffffff, y * 0xffffffff, z * 0xffffffff, w * 0xffffffff)

force_inline v128_t simd_add(v128_t v1, v128_t v2)
{
	return vec_add(v1, v2);
}

force_inline v128_t simd_add_uint32(v128_t v1, v128_t v2)
{
	return (v128_t)vec_add((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_sub(v128_t v1, v128_t v2)
{
	return vec_sub(v1, v2);
}

force_inline v128_t simd_sub_uint32(v128_t v1, v128_t v2)
{
	return (v128_t)vec_sub((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_mul(v128_t v1, v128_t v2)
{
	return vec_madd(v1, v2, simd_zero());
}

force_inline v128_t simd_mul_uint32(v128_t v1, v128_t v2)
{
	return (v128_t)vec_mulo(v1, v2);
}

force_inline v128_t simd_div(v128_t v1, v128_t v2)
{
	return vec_madd(v1, simd_rc(v2), simd_zero());
}

force_inline v128_t simd_madd(v128_t v1, v128_t v2, v128_t v3)
{
	return vec_madd(v1, v2, v3);
}

force_inline v128_t simd_dp3(v128_t v1, v128_t v2)
{
//	TODO: Clean this function up
	const v128_t d = simd_mul(v1, v2);
	v128_t res = simd_splat(d, 0);
	res = simd_add(res, simd_splat(d, 1));
	res = simd_add(res, simd_splat(d, 2));
	return res;
}

force_inline v128_t simd_dp4(v128_t v1, v128_t v2)
{
//	TODO: Clean this function up
	const v128_t d = simd_mul(v1, v2);
	v128_t res = simd_splat(d, 0);
	res = simd_add(res, simd_splat(d, 1));
	res = simd_add(res, simd_splat(d, 2));
	res = simd_add(res, simd_splat(d, 3));
	return res;
}

force_inline v128_t simd_sqrt(v128_t v)
{
	return vec_madd(v, simd_rsq(v), simd_zero());
}

force_inline v128_t simd_rce(v128_t v)
{
	return vec_re(v);
}

force_inline v128_t simd_rc(v128_t v)
{
	v128_t e = vec_re(v); // initial estimate
	v128_t r = vec_madd(vec_nmsub(e, v, simd_scalar(1.0f)), e, e); // one step of newton-raphson
	vector __bool int not_nan = vec_cmpeq(r, r); // did we get a NaN in the previous step?
	return vec_sel(e, r, not_nan); // use the estimate (-Inf or Inf) instead of NaN
}

force_inline v128_t simd_rsqe(v128_t v)
{
	return vec_rsqrte(v);
}

force_inline v128_t simd_rsq(v128_t v)
{
	v128_t zero = simd_zero();
	v128_t half = simd_scalar(0.5f);
	v128_t one =  simd_scalar(1.0f);
	v128_t e = vec_rsqrte(v); // initial estimate
	v128_t es = vec_madd(e, e, zero);
	v128_t eh = vec_madd(e, half, zero);
	return vec_madd(vec_nmsub(v, es, one), eh, e); // one step newton-raphson
}

force_inline v128_t simd_min(v128_t v1, v128_t v2)
{
	return vec_min(v1, v2);
}

force_inline v128_t simd_max(v128_t v1, v128_t v2)
{
	return vec_max(v1, v2);
}

force_inline v128_t simd_cmp_ge(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmpge(v1, v2);
}

force_inline v128_t simd_cmp_ge_uint32(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmpge((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_cmp_g(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmpgt(v1, v2);
}

force_inline v128_t simd_cmp_g_uint32(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmpgt((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_cmp_le(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmple(v1, v2);
}

force_inline v128_t simd_cmp_le_uint32(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmple((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_cmp_l(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmplt(v1, v2);
}

force_inline v128_t simd_cmp_l_uint32(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmplt((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_cmp_eq(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmpeq(v1, v2);
}

force_inline v128_t simd_cmp_eq_uint32(v128_t v1, v128_t v2)
{
	return (vector float)vec_cmpeq((vector unsigned int)v1, (vector unsigned int)v2);
}

force_inline v128_t simd_rot_r(v128_t v)
{
	vector unsigned char p = (vector unsigned char){
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7,
				 8,  9, 10, 11
				 };
	return vec_perm(v, v, p);
}

force_inline v128_t simd_rot_r2(v128_t v)
{
	vector unsigned char p = (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7
				 };
	return vec_perm(v, v, p);
}

force_inline v128_t simd_rot_l(v128_t v)
{
	vector unsigned char p = (vector unsigned char){
				 4,  5,  6,  7,
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3
				 };
	return vec_perm(v, v, p);
}

force_inline v128_t simd_rot_l2(v128_t v)
{
	vector unsigned char p = (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7
				 };
	return vec_perm(v, v, p);
}

force_inline v128_t simd_splat(v128_t v, uint8_t i)
{
	switch(i)
	{
	case 0:
		return vec_splat(v, 0);
	case 1:
		return vec_splat(v, 1);
	case 2:
		return vec_splat(v, 2);
	}

	return vec_splat(v, 3);
}

// simd_mergeLow(vLoad(0,1,2,3), simd_load(4,5,6,7)) = (0.000000 4.000000 1.000000 5.000000)
force_inline v128_t simd_merge_low(v128_t l, v128_t h)
{
	return vec_mergel(l, h);
}

// simd_mergeHigh(vLoad(0,1,2,3), simd_load(4,5,6,7)) = (2.000000 6.000000 3.000000 7.000000)
force_inline v128_t simd_merge_high(v128_t l, v128_t h)
{
	return vec_mergeh(l, h);
}

force_inline v128_t simd_mux_low(v128_t l, v128_t h)
{
	vector unsigned char p = (vector unsigned char){
				 0,  1,  2,  3,
				 4,  5,  6,  7,
				16, 17, 18, 18,
				20, 21, 22, 23
				};
	return vec_perm(l, h, p);
}

force_inline v128_t simd_mux_high(v128_t l, v128_t h)
{
	vector unsigned char p = (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				24, 25, 26, 27,
				28, 29, 30, 31
				};
	return vec_perm(l, h, p);
}

force_inline v128_t simd_select_mask(v128_t m, v128_t v1, v128_t v2)
{
	return vec_sel(v2, v1, (vector unsigned int)m);
}

force_inline v128_t simd_mask_w(v128_t _v, v128_t _w)
{
	return simd_select_mask(simd_mask(1,1,1,0), _v, _w);
}

force_inline v128_t simd_or(v128_t v1, v128_t v2)
{
	return vec_or(v1, v2);
}

force_inline v128_t simd_and(v128_t v1, v128_t v2)
{
	return vec_and(v1, v2);
}

force_inline v128_t simd_not(v128_t v)
{
	return vec_xor(v, simd_scalar_uint32(0xffffffff));
}

force_inline v128_t simd_shift_left_byte(v128_t v, uint32_t bytes)
{
	switch(bytes)
	{
		case 0:	return vec_sld(v,v, 0);
		case 1:	return vec_sld(v,v, 1);
		case 2:	return vec_sld(v,v, 2);
		case 3:	return vec_sld(v,v, 3);
	}
}

force_inline uint32_t simd_extract_uint32(v128_t v, uint8_t i)
{
	uint32_t lu[4];
	simd_store_uint32(lu, v);
	return lu[i];
}

force_inline fp32_t simd_extract(v128_t v, uint8_t i)
{
	fp32_t lf[4];
	simd_store(lf, v);
	return lf[i];
//	return vec_extract(v, i);
}

force_inline v128_t simd_conv_fp32int32(v128_t v)
{
	return (vector float)vec_cts(v, 0);
}

force_inline v128_t simd_conv_fp32uint32(v128_t v)
{
	return (vector float)vec_ctu(v, 0);
}

force_inline v128_t simd_conv_int32fp32(v128_t v)
{
	return vec_ctf((vector unsigned int)v, 0);
}

force_inline v128_t simd_conv_scale_fp32int32(v128_t v, v128_t s)
{
	return simd_mul((vector float)vec_ctu(v, 0), s);
}

force_inline v128_t simd_inf()
{
	return simd_scalar(FP32_INF);
}

force_inline v128_t simd_ninf()
{
	return simd_scalar(FP32_NINF);
}

force_inline v128_t simd_zero()
{
	return (v128_t){0.0f, 0.0f, 0.0f, 0.0f};
}

force_inline v128_t simd_scalar(fp32_t f)
{
	return (vector float){f, f, f, f};
}

force_inline v128_t simd_scalar_uint32(uint32_t u)
{
	return (v128_t)((vector unsigned int){u, u, u, u});
}

force_inline v128_t simd_load(fp32_t x, fp32_t y, fp32_t z, fp32_t w)
{
	return (v128_t){x, y, z, w};
}

force_inline v128_t simd_load_uint32(uint32_t x, uint32_t y, uint32_t z, uint32_t w)
{
	return (v128_t)((vector unsigned int){x, y, z, w});
}

force_inline v128_t simd_load_ptr(fp32_t *p)
{
	return vec_ld(0, p);
}

force_inline v128_t simd_load_ptr_uint32(uint32_t *p)
{
	return (vector float)vec_ld(0, p);
}

force_inline void simd_store(fp32_t *p, v128_t v)
{
	vec_st(v, 0, p);
}

force_inline void simd_store_uint32(uint32_t *p, v128_t v)
{
	vec_st((vector unsigned int)v, 0, p);
}

#ifdef __cplusplus
}
#endif

#endif
