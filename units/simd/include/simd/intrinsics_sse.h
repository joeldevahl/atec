#ifndef SIMD_INTRINSICS_SSE_H
#define SIMD_INTRINSICS_SSE_H

#define D_SHUFFLE_MSK(_i0, _i1, _i2, _i3) ((v128)_MM_SHUFFLE(3-_i0, 3-_i1, 3-_i2, 3-_i3))

#define simd_mask(x, y, z, w) simd_load_uint32(x * 0xffffffff, y * 0xffffffff, z * 0xffffffff, w * 0xffffffff)

#define simd_shuffle1(v, _i0, _i1, _i2, _i3) _mm_shuffle_ps(v, v, _MM_SHUFFLE(_i3, _i2, _i1, _i0))

force_inline v128 simd_splat(v128 v, uint8_t i)
{
	switch(i)
	{
	case 0:
		return _mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0));
	case 1:
		return _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1));
	case 2:
		return _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2));
	}

	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3));
}

force_inline v128 simd_scalar(float f)
{
	return _mm_set1_ps(f);
}

force_inline v128 simd_inf()
{
	return simd_scalar(F32_INF);
}

force_inline v128 simd_ninf()
{
	return simd_scalar(F32_NINF);
}

force_inline v128 simd_zero()
{
	return  _mm_setzero_ps();
}

force_inline v128 simd_scalar_uint32(uint32_t u)
{
	const uint32_t align(16) p[4] = {u, u, u, u};
	return _mm_castsi128_ps(_mm_load_si128((__m128i*)p));
}

force_inline v128 simd_load(float x, float y, float z, float w)
{
	const float align(16) p[4] = {x,y,z,w};
	return _mm_load_ps(p);
}

force_inline v128 simd_load_uint32(uint32_t x, uint32_t y, uint32_t z, uint32_t w)
{
	const uint32_t align(16) p[4] = {x,y,z,w};
	return _mm_castsi128_ps(_mm_load_si128((__m128i*)p));
}

force_inline v128 simd_load_ptr(float *p)
{
	return _mm_load_ps(p);
}

force_inline v128 simd_load_ptr_uint32(uint32_t *p)
{
	return _mm_castsi128_ps(_mm_load_si128((__m128i*)p));
}

force_inline void simd_store(float *p, v128 v)
{
	_mm_store_ps(p, v);
}

force_inline void simd_store_uint32(uint32_t *p, v128 v)
{
	_mm_store_si128((__m128i*)p, *((__m128i*)&v));
}

force_inline v128 simd_add(v128 v1, v128 v2)
{
	return _mm_add_ps(v1, v2);
}

force_inline v128 simd_add_uint32(v128 v1, v128 v2)
{
	return _mm_castsi128_ps(_mm_add_epint32(_mm_castps_si128(v1), _mm_castps_si128(v2)));
}

force_inline v128 simd_sub(v128 v1, v128 v2)
{
	return _mm_sub_ps(v1, v2);
}

force_inline v128 simd_mul(v128 v1, v128 v2)
{
	return _mm_mul_ps(v1, v2);
}

force_inline v128 simd_div(v128 v1, v128 v2)
{
	return _mm_div_ps(v1, v2);
}

force_inline v128 simd_madd(v128 v1, v128 v2, v128 v3)
{
	return _mm_add_ps(_mm_mul_ps(v1, v2), v3);
}

force_inline v128 simd_dp3(v128 v1, v128 v2)
{
//	TODO: Clean this function up
#if SIMD_SSE >= 4
	return _mm_dp_ps(v1, v2, 0x77);
#else
	const v128 d = simd_mul(v1, v2);
	v128 res = simd_splat(d, 0);
	res = simd_add(res, simd_splat(d, 1));
	res = simd_add(res, simd_splat(d, 2));
	return res;
#endif
}

force_inline v128 simd_dp4(v128 v1, v128 v2)
{
//	TODO: Clean this function up
#if SIMD_SSE >= 4
	return _mm_dp_ps(v1, v2, 0xff);
#else
	const v128 d = simd_mul(v1, v2);
	v128 res = simd_splat(d, 0);
	res = simd_add(res, simd_splat(d, 1));
	res = simd_add(res, simd_splat(d, 2));
	res = simd_add(res, simd_splat(d, 3));
	return res;
#endif
}

force_inline v128 simd_sqrt(v128 v)
{
	return _mm_sqrt_ps(v);
}

force_inline v128 simd_rce(v128 v)
{
	return _mm_rcp_ps(v);
}

force_inline v128 simd_rc(v128 v)
{
//	TODO: Is this fast or slow?
	return simd_div(simd_scalar(1.0f), v);
}

force_inline v128 simd_rsqe(v128 v)
{
	return _mm_rsqrt_ps(v);
}

force_inline v128 simd_rsq(v128 v)
{
//	TODO: Truly horrible
	return simd_div(simd_scalar(1.0f), _mm_sqrt_ps(v));
}

force_inline v128 simd_min(v128 v1, v128 v2)
{
	return _mm_min_ps(v1, v2);
}

force_inline v128 simd_max(v128 v1, v128 v2)
{
	return _mm_max_ps(v1, v2);
}

force_inline v128 simd_cmp_ge(v128 v1, v128 v2)
{
	return _mm_cmpge_ps(v1, v2);
}

force_inline v128 simd_cmp_g(v128 v1, v128 v2)
{
	return _mm_cmpgt_ps(v1, v2);
}

force_inline v128 simd_cmp_le(v128 v1, v128 v2)
{
	return _mm_cmple_ps(v1, v2);
}

force_inline v128 simd_cmp_l(v128 v1, v128 v2)
{
	return _mm_cmplt_ps(v1, v2);
}

force_inline v128 simd_cmp_eq(v128 v1, v128 v2)
{
	return _mm_cmpeq_ps(v1, v2);
}

force_inline v128 simd_rot_r(v128 v)
{
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 0, 1, 2));
}

force_inline v128 simd_rot_r2(v128 v)
{
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 3, 0, 1));
}

force_inline v128 simd_rot_l(v128 v)
{
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 2, 3, 0));
}

force_inline v128 simd_rot_l2(v128 v)
{
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 3, 0, 1));
}


// vMergeLow(vLoad(0,1,2,3), vLoad(4,5,6,7)) = (0.000000 4.000000 1.000000 5.000000)
force_inline v128 simd_merge_low(v128 l, v128 h)
{
	return _mm_unpackhi_ps(l, h);
}

// vMergeHigh(vLoad(0,1,2,3), vLoad(4,5,6,7)) = (2.000000 6.000000 3.000000 7.000000)
force_inline v128 simd_merge_high(v128 l, v128 h)
{
	return _mm_unpacklo_ps(l, h);
}

force_inline v128 simd_mux_low(v128 l, v128 h)
{
	return _mm_movelh_ps(l, h);
}

force_inline v128 simd_mux_high(v128 l, v128 h)
{
	return _mm_movehl_ps(l, h);
}

force_inline v128 simd_or(v128 v1, v128 v2)
{
	return _mm_castsi128_ps(_mm_or_si128(_mm_castps_si128(v1), _mm_castps_si128(v2)));
}

force_inline v128 simd_and(v128 v1, v128 v2)
{
	return _mm_castsi128_ps(_mm_and_si128(_mm_castps_si128(v1), _mm_castps_si128(v2)));
}

force_inline v128 simd_not(v128 v)
{
	return _mm_xor_ps(v, simd_scalar_uint32(0xffffffff));
}

force_inline v128 simd_select_mask(v128 m, v128 v1, v128 v2)
{
	return simd_or(simd_and(m, v1), _mm_andnot_ps(m, v2));
}

force_inline v128 simd_mask_w(v128 v, v128 w)
{
	return simd_select_mask(simd_mask(1,1,1,0), v, w);
}


force_inline v128 simd_shift_left_byte(v128 v, uint32_t bytes)
{
	switch(bytes)
	{
	case 0: return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 0));
	case 1: return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 1));
	case 2: return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 2));
	}
	return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 3));
}

force_inline uint32_t simd_extract_uint32(v128 v, uint8_t i)
{
	switch(i)
	{
		case 0: return _mm_cvtsi128_sint32(_mm_shuffle_epint32(*((__m128i*)&v), _MM_SHUFFLE(0,0,0,0)));
	 	case 1: return _mm_cvtsi128_sint32(_mm_shuffle_epint32(*((__m128i*)&v), _MM_SHUFFLE(0,0,0,1)));
		case 2: return _mm_cvtsi128_sint32(_mm_shuffle_epint32(*((__m128i*)&v), _MM_SHUFFLE(0,0,0,2)));
	}
	return _mm_cvtsi128_sint32(_mm_shuffle_epint32(*((__m128i*)&v), _MM_SHUFFLE(0,0,0,3)));
}

force_inline float simd_extract(v128 v, uint8_t i)
{
	switch(i)
	{
		case 0:	return _mm_cvtss_f32(v);
		case 1:	return _mm_cvtss_f32(_mm_shuffle_ps(v, v, _MM_SHUFFLE(0,0,0,1)));
		case 2:	return _mm_cvtss_f32(_mm_shuffle_ps(v, v, _MM_SHUFFLE(0,0,0,2)));
	}
	return _mm_cvtss_f32(_mm_shuffle_ps(v, v, _MM_SHUFFLE(0,0,0,3)));
}

force_inline v128 simd_conv_f32_int32(v128 v)
{
	return _mm_castsi128_ps(_mm_cvtps_epint32(v));
}

force_inline v128 simd_conv_f32_uint32(v128 v)
{
	return _mm_castsi128_ps(_mm_cvttps_epint32(simd_max(v, simd_scalar(0.0f))));
}

force_inline v128 simd_conv_s32_f32(v128 v)
{
	return _mm_cvtepint32_ps(_mm_castps_si128(v));
}

#endif
