#ifndef SIMD_INTRINSICS_H
#define SIMD_INTRINSICS_H

#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef F32_INF
#       define F32_INF  static_cast<float>(HUGE_VAL)
#endif

#ifndef F32_NINF
#       define F32_NINF static_cast<float>(-HUGE_VAL)
#endif

//#define SIMD_NONE
//#define SIMD_ALTIVEC
//#define SIMD_SSE 1
//#define SIMD_SSE 2
//#define SIMD_SSE 3
//#define SIMD_SSE 4

#if defined(SIMD_NONE)
#	error "SIMD_NONE not supported for now."
#elif defined(SIMD_ALTIVEC)
#	include <altivec.h>
#	undef bool
#	undef pixel
	typedef vector fp32_t v128_t;



#elif defined(SIMD_SPU)
	typedef vector fp32_t v128_t;



#elif defined(SIMD_SSE)
#	include <xmmintrin.h>

#	if SIMD_SSE >= 2
#		include <emmintrin.h>
#	endif

#	if SIMD_SSE >= 3
#		include <pmmintrin.h>
#	endif

#	if SIMD_SSE >= 4
#		include <smmintrin.h>
#	endif

	typedef __m128 v128_t;

#else
	#error SIMD math not implemented for this platform
#endif

#include <core/types.h>

/*-----------------------------------------------------------------*\
 | ARITHMETIC OPERATION
\*-----------------------------------------------------------------*/

// These functions has variants for different types stored in v128_t
force_inline v128_t vAdd		(v128_t v1, v128_t v2);
force_inline v128_t vAdd_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vSub		(v128_t v1, v128_t v2);
force_inline v128_t vSub_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vMul		(v128_t v1, v128_t v2);
force_inline v128_t vMul_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vDiv		(v128_t v1, v128_t v2);
force_inline v128_t vDiv_uint32	(v128_t v1, v128_t v2);

// These functions are defined for v128_t as 4x fp32_t only
force_inline v128_t vMAdd		(v128_t v1, v128_t v2, v128_t v3);
force_inline v128_t vDP3		(v128_t v1, v128_t v2);
force_inline v128_t vDP4		(v128_t v1, v128_t v2);
force_inline v128_t vSqrt		(v128_t v);
force_inline v128_t vRce		(v128_t v);
force_inline v128_t vRc			(v128_t v);
force_inline v128_t vRsqe		(v128_t v);
force_inline v128_t vRsq		(v128_t v);

/*-----------------------------------------------------------------*\
 | COMPARSION OPERATIONS
\*-----------------------------------------------------------------*/

force_inline v128_t vCmpGE			(v128_t v1, v128_t v2);
force_inline v128_t vCmpGE_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vCmpG			(v128_t v1, v128_t v2);
force_inline v128_t vCmpG_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vCmpLE			(v128_t v1, v128_t v2);
force_inline v128_t vCmpLE_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vCmpL			(v128_t v1, v128_t v2);
force_inline v128_t vCmpL_uint32	(v128_t v1, v128_t v2);
force_inline v128_t vCmpEQ			(v128_t v1, v128_t v2);
force_inline v128_t vCmpEQ_uint32	(v128_t v1, v128_t v2);

/*-----------------------------------------------------------------*\
 | BIT AND BYTE TWIDDLING
\*-----------------------------------------------------------------*/

// Agnostic to the type stored in v128_t
force_inline v128_t vRotR		(v128_t v);
force_inline v128_t vRotR2		(v128_t v);
force_inline v128_t vRotL		(v128_t v);
force_inline v128_t vRotL2		(v128_t v);
force_inline v128_t vSplat		(v128_t v, uint8_t i);
force_inline v128_t vMuxLow		(v128_t l, v128_t h);
force_inline v128_t vMuxHigh	(v128_t l, v128_t h);
force_inline v128_t vMergeLow	(v128_t l, v128_t h);
force_inline v128_t vMergeHigh	(v128_t l, v128_t h);
force_inline v128_t vOr			(v128_t v1, v128_t v2);
force_inline v128_t vAnd		(v128_t v1, v128_t v2);
force_inline v128_t vNot		(v128_t v);
force_inline v128_t vShiftLeftByte(v128_t v, uint32_t bytes);

force_inline v128_t vSelMsk		(v128_t m, v128_t v1, v128_t v2);
force_inline v128_t vMskW		(v128_t v, v128_t w);

/*-----------------------------------------------------------------*\
 | CONVERT, EXTRACT, LOAD AND STORE
\*-----------------------------------------------------------------*/

force_inline fp32_t vExtract			(v128_t v, uint8_t i);
force_inline uint32_t vExtract_uint32	(v128_t v, uint8_t i);

force_inline v128_t vConv_int32_fp32	(v128_t v);
force_inline v128_t vConv_fp32_int32	(v128_t v);
force_inline v128_t vConvScale_fp32_int32(v128_t v, v128_t scale);

force_inline v128_t vInf		();
force_inline v128_t vNInf		();
force_inline v128_t vZero		();
force_inline v128_t vScalar		(fp32_t f);
force_inline v128_t vScalar_uint32(uint32_t u);
force_inline v128_t vLoad		(fp32_t x, fp32_t y, fp32_t z, fp32_t w);
force_inline v128_t vLoad_uint32(uint32_t x, uint32_t y, uint32_t z, uint32_t w);
force_inline v128_t vLoad_ptr		(fp32_t *p);
force_inline v128_t vLoad_uint32_ptr(uint32_t *p);
force_inline void vStore		(fp32_t *p, v128_t v);
force_inline void vStore_uint32	(uint32_t *p, v128_t v);

/*-----------------------------------------------------------------*\
 | THE IMPLEMENTATION
\*-----------------------------------------------------------------*/


// macros

#if defined(SIMD_ALTIVEC)

#elif defined(SIMD_SPU)
#define D_SHUFFLE_MSK(_i0, _i1, _i2, _i3) ((v128_t)((vector unsigned char){ _i0*4, _i0*4+1, _i0*4+2, _i0*4+3,\
	_i1*4, _i1*4+1, _i1*4+2, _i1*4+3,\
	_i2*4+16, _i2*4+1+16, _i2*4+2+16, _i2*4+3+16,\
	_i3*4+16, _i3*4+1+16, _i3*4+2+16, _i3*4+3+16}))
#elif defined(SIMD_SSE)
#define D_SHUFFLE_MSK(_i0, _i1, _i2, _i3) ((v128_t)_MM_SHUFFLE(3-_i0, 3-_i1, 3-_i2, 3-_i3))
#else
	
#endif


#if defined(SIMD_ALTIVEC)
#define vShuffle2(v1, v2, _i0, _i1, _i2, _i3) \
		vec_perm(v1, v2, (vector unsigned char){ \
					_i0*4, _i0*4+1, _i0*4+2, _i0*4+3,\
					_i1*4, _i1*4+1, _i1*4+2, _i1*4+3,\
					_i2*4, _i2*4+1, _i2*4+2, _i2*4+3,\
					_i3*4, _i3*4+1, _i3*4+2, _i3*4+3,\
					})
#elif defined(SIMD_SPU)
	#define vShuffle2(v1, v2, _i0, _i1, _i2, _i3) \
		spu_shuffle(v1, v2, (vector unsigned char){ \
					_i0*4, _i0*4+1, _i0*4+2, _i0*4+3,\
					_i1*4, _i1*4+1, _i1*4+2, _i1*4+3,\
					_i2*4, _i2*4+1, _i2*4+2, _i2*4+3,\
					_i3*4, _i3*4+1, _i3*4+2, _i3*4+3,\
					})
#elif defined(SIMD_SSE)
#else
#endif

#if defined(SIMD_ALTIVEC)
	#define vMask(x, y, z, w) vLoad_uint32_t(x * 0xffffffff, y * 0xffffffff, z * 0xffffffff, w * 0xffffffff)
#elif defined(SIMD_SPU)
	#define vMask(x, y, z, w) ((v128_t) spu_maskb((u16)((~(1-x)&0xf000) | (~(1-y)&0xf00) | (~(1-z)&0xf0) | (~(1-w)&0xf))))
#elif defined(SIMD_SSE)
	#define vMask(x, y, z, w) vLoad_uint32_t(x * 0xffffffff, y * 0xffffffff, z * 0xffffffff, w * 0xffffffff)
#else
	#define vMask(x, y, z, w) vLoad_uint32_t(x * 0xffffffff, y * 0xffffffff, z * 0xffffffff, w * 0xffffffff)
#endif

#if defined(SIMD_ALTIVEC) || defined(SIMD_SPU)
	#define vShuffle1(v, _i0, _i1, _i2, _i3) \
		vShuffle2(v, v, _i0, _i1, _i2, _i3)
#elif defined(SIMD_SSE)
	#define vShuffle1(v, _i0, _i1, _i2, _i3) \
		_mm_shuffle_ps(v, v, _MM_SHUFFLE(_i3, _i2, _i1, _i0))
#else
#endif


/*
	Function: vAdd
		Element wise addition of two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		The two vectors added together.
*/
force_inline v128_t vAdd(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_add(v1, v2);
#elif defined(SIMD_SPU)
	return spu_add(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_add_ps(v1, v2);
#else
	return v128_t(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z, v1.w+v2.w);
#endif
}

force_inline v128_t vAdd_uint32_t(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return (v128_t)vec_add((vector unsigned int)v1, (vector unsigned int)v2);
#elif defined(SIMD_SPU)
	return (v128_t)spu_add((vector unsigned int)v1, (vector unsigned int)v2);
#elif defined(SIMD_SSE)
	return  _mm_castsi128_ps(_mm_add_epi32(_mm_castps_si128(v1), _mm_castps_si128(v2)));
#else
	return v128_t(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z, v1.w+v2.w);
#endif
}

/*
	Function: vSub
		Element wise subtraction of two vectors.
*/
force_inline v128_t vSub(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_sub(v1, v2);
#elif defined(SIMD_SPU)
	return spu_sub(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_sub_ps(v1, v2);
#else
	return v128_t(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z, v1.w-v2.w);
#endif
}

/*
	Function: vMul
		Element wise multiplication of two vectors.
*/
force_inline v128_t vMul(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_madd(v1, v2, vZero());
#elif defined(SIMD_SPU)
	return spu_mul(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_mul_ps(v1, v2);
#else
	return v128_t(v1.x*v2.x, v1.y*v2.y, v1.z*v2.z, v1.w*v2.w);
#endif
}

/*
	Function: vDiv
		Element wise division of two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		The first vector divided by the second.
*/
force_inline v128_t vDiv(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_madd(v1, vRc(v2), vZero());
#elif defined(SIMD_SPU)
	return spu_mul(v1, vRc(v2));
#elif defined(SIMD_SSE)
	return _mm_div_ps(v1, v2);
#else
	return v128_t(v1.x/v2.x, v1.y/v2.y, v1.z/v2.z, v1.w/v2.w);
#endif
}

/*
	Function: vMadd
		Multiplies two vectors and adds a third. Operates element wise.

	Parameters:
		v1 - first vector
		v2 - second vector
		v3 - third vector (add)

	Returns:
		(v1 * v2) + v3
*/
force_inline v128_t vMAdd(v128_t v1, v128_t v2, v128_t v3)
{
#if defined(SIMD_ALTIVEC)
	return vec_madd(v1, v2, v3);
#elif defined(SIMD_SPU)
	return spu_madd(v1, v2, v3);
#elif defined(SIMD_SSE)
	return _mm_add_ps(_mm_mul_ps(v1, v2), v3);
#else
	return v128_t(v1.x*v2.x+v3.x, v1.y*v2.y+v3.y, v1.z*v2.z+v3.z, v1.w*v2.w+v3.w);
#endif
}

/*
	Function: vDP3
		3 element dot product (scalar product) between two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		Vector containing the dot product in all fields.
*/
force_inline v128_t vDP3(v128_t v1, v128_t v2)
{
//	TODO: Clean this function up
#if defined(SIMD_ALTIVEC)
	const v128_t d = vMul(v1, v2);
	v128_t res = vSplat(d, 0);
	res = vAdd(res, vSplat(d, 1));
	res = vAdd(res, vSplat(d, 2));
	return res;
#elif defined(SIMD_SPU)
	const v128_t d = vMul(v1, v2);
	v128_t res = vSplat(d, 0);
	res = vAdd(res, vSplat(d, 1));
	res = vAdd(res, vSplat(d, 2));
	return res;
#elif defined(SIMD_SSE)
#	if SIMD_SSE >= 4
	return _mm_dp_ps(v1, v2, 0x77);
#	else
	const v128_t d = vMul(v1, v2);
	v128_t res = vSplat(d, 0);
	res = vAdd(res, vSplat(d, 1));
	res = vAdd(res, vSplat(d, 2));
	return res;
#	endif
#else
	return v128_t(v1.x*v2.x + v1.y*v2.y + v1.z*v2.z);
#endif
}

/*
	Function: vDP4
		4 element dot product (scalar product) between two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		Vector containing the dot product in all fields.
*/
force_inline v128_t vDP4(v128_t v1, v128_t v2)
{
//	TODO: Clean this function up
#if defined(SIMD_ALTIVEC)
	const v128_t d = vMul(v1, v2);
	v128_t res = vSplat(d, 0);
	res = vAdd(res, vSplat(d, 1));
	res = vAdd(res, vSplat(d, 2));
	res = vAdd(res, vSplat(d, 3));
	return res;
#elif defined(SIMD_SPU)
	const v128_t d = vMul(v1, v2);
	v128_t res = vSplat(d, 0);
	res = vAdd(res, vSplat(d, 1));
	res = vAdd(res, vSplat(d, 2));
	res = vAdd(res, vSplat(d, 3));
	return res;
#elif defined(SIMD_SSE)
#	if SIMD_SSE >= 4
	return _mm_dp_ps(v1, v2, 0xff);
#	else
	const v128_t d = vMul(v1, v2);
	v128_t res = vSplat(d, 0);
	res = vAdd(res, vSplat(d, 1));
	res = vAdd(res, vSplat(d, 2));
	res = vAdd(res, vSplat(d, 3));
	return res;
#	endif
#else
	return v128_t(v1.x*v2.x + v1.y*v2.y + v1.z*v2.z + v1.w*v2.w);
#endif
}


/*
	Function: vSqrt
		Element wise square root of vector.
*/
force_inline v128_t vSqrt(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_madd(v, vRsq(v), vZero());
#elif defined(SIMD_SPU)
	return spu_madd(v, vRsq(v), vZero());
#elif defined(SIMD_SSE)
	return _mm_sqrt_ps(v);
#else
	return v128_t(sqrt(v.x), sqrt(v.y), sqrt(v.z), sqrt(v.w));
#endif
}

/*
	Function: vRce
		Element wise reciprocal (1/x) estimate.
*/
force_inline v128_t vRce(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_re(v);
#elif defined(SIMD_SPU)
	return spu_re(v);
#elif defined(SIMD_SSE)
	return _mm_rcp_ps(v);
#else
	return v128_t(1.0f/v.x, 1.0f/v.y, 1.0f/v.z, 1.0f/v.w);
#endif
}

/*
	Function: vRc
		Element wise reciprocal (1/x).
		Calulated using the reciprocal estimate and performing a one step Newton-Raphson refinement.
*/
force_inline v128_t vRc(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	v128_t e = vec_re(v); // initial estimate
	v128_t r = vec_madd(vec_nmsub(e, v, vScalar(1.0f)), e, e); // one step of newton-raphson
	vector __bool int not_nan = vec_cmpeq(r, r); // did we get a NaN in the previous step?
	return vec_sel(e, r, not_nan); // use the estimate (-Inf or Inf) instead of NaN
#elif defined(SIMD_SPU)
	v128_t e = spu_re(v); // initial estimate
	v128_t r = spu_madd(spu_nmsub(e, v, vScalar(1.0f)), e, e); // one step of newton-raphson
	vector unsigned not_nan = spu_cmpeq(r, r); // did we get a NaN in the previous step?
	return spu_sel(e, r, not_nan); // use the estimate (-Inf or Inf) instead of NaN
#elif defined(SIMD_SSE)
//	TODO: Is this fast or slow?
	return vDiv(vScalar(1.0f), v);
#else
	return v128_t(1.0f/v.x, 1.0f/v.y, 1.0f/v.z, 1.0f/v.w);
#endif
}

/*
	Function: vRsqe
		Element wise reciprocal square root (1/sqrt(x)) estimate.
*/
force_inline v128_t vRsqe(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_rsqrte(v);
#elif defined(SIMD_SPU)
	return spu_rsqrte(v);
#elif defined(SIMD_SSE)
	return _mm_rsqrt_ps(v);
#else
	return v128_t(1.0f/sqrt(v.x), 1.0f/sqrt(v.y), 1.0f/sqrt(v.z), 1.0f/sqrt(v.w));
#endif
}

/*
	Function: vRsq
		Element wise reciprocal square root (1/sqrt(x)).
		Calulated using the reciprocal square root estimate and performing a one step Newton-Raphson refinement.
*/
force_inline v128_t vRsq(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	v128_t zero = vZero();
	v128_t half = vScalar(0.5f);
	v128_t one =  vScalar(1.0f);
	v128_t e = vec_rsqrte(v); // initial estimate
	v128_t es = vec_madd(e, e, zero);
	v128_t eh = vec_madd(e, half, zero);
	return vec_madd(vec_nmsub(v, es, one), eh, e); // one step newton-raphson
#elif defined(SIMD_SPU)
	v128_t zero = vZero();
	v128_t half = vScalar(0.5f);
	v128_t one =  vScalar(1.0f);
	v128_t e = spu_rsqrte(v); // initial estimate
	v128_t es = spu_madd(e, e, zero);
	v128_t eh = spu_madd(e, half, zero);
	return spu_madd(spu_nmsub(v, es, one), eh, e); // one step newton-raphson
#elif defined(SIMD_SSE)
//	TODO: Truly horrible
	return vDiv(vScalar(1.0f), _mm_sqrt_ps(v));
#else
	return v128_t(1.0f/sqrt(v.x), 1.0f/sqrt(v.y), 1.0f/sqrt(v.z), 1.0f/sqrt(v.w));
#endif
}

#define MIN(x,y) x <= y ? x : y
#define MAX(x,y) x >= y ? x : y

/*
	Function: vMin
		Element wise min of two vectors
*/
force_inline v128_t vMin(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_min(v1, v2);
#elif defined(SIMD_SPU)
	return spu_sel(v1, v2, spu_cmpgt(v1, v2));
#elif defined(SIMD_SSE)
	return _mm_min_ps(v1, v2);
#else
	return v128_t(
			MIN(v1.x, v2.x),
			MIN(v1.y, v2.y),
			MIN(v1.z, v2.z),
			MIN(v2.w, v2.w)
			);
#endif
}

/*
	Function: vMax
		Element wise max of two vectors
*/
force_inline v128_t vMax(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_max(v1, v2);
#elif defined(SIMD_SPU)
	return spu_sel(v2, v1, spu_cmpgt(v1, v2));
#elif defined(SIMD_SSE)
	return _mm_max_ps(v1, v2);
#else
	return v128_t(
			MAX(v1.x, v2.x),
			MAX(v1.y, v2.y),
			MAX(v1.z, v2.z),
			MAX(v2.w, v2.w)
			);
#endif
}

force_inline v128_t vCmpGE(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_cmpge(v1, v2);
#elif defined(SIMD_SPU)
	return (vector float)spu_or(spu_cmpgt(v1, v2), spu_cmpeq(v1, v2));
#elif defined(SIMD_SSE)
	return _mm_cmpge_ps(v1, v2);
#else
	return v128_t(
			v1.x >= v2.x ? 1 : 0,
			v1.y >= v2.z ? 1 : 0,
			v1.z >= v2.y ? 1 : 0,
			v1.w >= v2.w ? 1 : 0
			);
#endif
}

force_inline v128_t vCmpG(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_cmpgt(v1, v2);
#elif defined(SIMD_SPU)
	return (vector float)spu_cmpgt(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_cmpgt_ps(v1, v2);
#else
	return v128_t(
			v1.x > v2.x ? 1 : 0,
			v1.y > v2.z ? 1 : 0,
			v1.z > v2.y ? 1 : 0,
			v1.w > v2.w ? 1 : 0
			);
#endif
}

force_inline v128_t vCmpLE(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_cmple(v1, v2);
#elif defined(SIMD_SPU)
	return (vector float)spu_xor(spu_cmpgt(v1, v2), (vector unsigned int)spu_splats((unsigned char)0xff));
#elif defined(SIMD_SSE)
	return _mm_cmple_ps(v1, v2);
#else
	return v128_t(
			v1.x <= v2.x ? 1 : 0,
			v1.y <= v2.z ? 1 : 0,
			v1.z <= v2.y ? 1 : 0,
			v1.w <= v2.w ? 1 : 0
			);
#endif
}

force_inline v128_t vCmpL(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_cmplt(v1, v2);
#elif defined(SIMD_SPU)
	return (vector float)spu_nor(spu_cmpgt(v1, v2), spu_cmpeq(v1, v2));
#elif defined(SIMD_SSE)
	return _mm_cmplt_ps(v1, v2);
#else
	return v128_t(
			v1.x < v2.x ? 1 : 0,
			v1.y < v2.z ? 1 : 0,
			v1.z < v2.y ? 1 : 0,
			v1.w < v2.w ? 1 : 0
			);
#endif
}

force_inline v128_t vCmpEQ(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_cmpeq(v1, v2);
#elif defined(SIMD_SPU)
	return (vector float)spu_cmpeq(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_cmpeq_ps(v1, v2);
#else
	return v128_t(
			v1.x == v2.x ? 1 : 0,
			v1.y == v2.z ? 1 : 0,
			v1.z == v2.y ? 1 : 0,
			v1.w == v2.w ? 1 : 0
			);
#endif
}

force_inline v128_t vRotR(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_perm(v, v, (vector unsigned char){
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7,
				 8,  9, 10, 11
				 });
#elif defined(SIMD_SPU)
	return spu_shuffle(v, v, (vector unsigned char){
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7,
				 8,  9, 10, 11
				 });
#elif defined(SIMD_SSE)
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 0, 1, 2));
#else
	return v128_t(v.w, v.x, v.y, v.z);
#endif
}

force_inline v128_t vRotR2(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_perm(v, v, (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7
				 });
#elif defined(SIMD_SPU)
	return spu_shuffle(v, v, (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7
				 });
#elif defined(SIMD_SSE)
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 3, 0, 1));
#else
	return v128_t(v.z, v.w, v.x, v.y);
#endif
}

force_inline v128_t vRotL(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_perm(v, v, (vector unsigned char){
				 4,  5,  6,  7,
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3
				 });
#elif defined(SIMD_SPU)
	return spu_shuffle(v, v, (vector unsigned char){
				 4,  5,  6,  7,
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3
				 });
#elif defined(SIMD_SSE)
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 2, 3, 0));
#else
	return v128_t(v.y, v.z, v.w, v.x);
#endif
}

force_inline v128_t vRotL2(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_perm(v, v, (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7
				 });
#elif defined(SIMD_SPU)
	return spu_shuffle(v, v, (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				 0,  1,  2,  3,
				 4,  5,  6,  7
				 });
#elif defined(SIMD_SSE)
	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 3, 0, 1));
#else
	return v128_t(v.z, v.w, v.x, v.y);
#endif
}

force_inline v128_t vSplat(v128_t v, uint8_t i)
{
#if defined(SIMD_ALTIVEC)
	switch(i)
	{
	case 0:
		return vec_splat(v, 0);
	case 1:
		return vec_splat(v, 1);
	case 2:
		return vec_splat(v, 2);
	}

	return vec_splat(v, 3);
#elif defined(SIMD_SPU)
	return spu_splats(spu_extract(v, i));
#elif defined(SIMD_SSE)
	switch(i)
	{
	case 0:
		return _mm_shuffle_ps(v, v, _MM_SHUFFLE(0, 0, 0, 0));
	case 1:
		return _mm_shuffle_ps(v, v, _MM_SHUFFLE(1, 1, 1, 1));
	case 2:
		return _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 2, 2, 2));
	}

	return _mm_shuffle_ps(v, v, _MM_SHUFFLE(3, 3, 3, 3));
#else
	switch(i)
	{
	case 0:
		return v128_t(v.x);
	case 1:
		return v128_t(v.y);
	case 2:
		return v128_t(v.z);
	}
	
	return v128_t(v.w);
#endif
}

// vMergeLow(vLoad(0,1,2,3), vLoad(4,5,6,7)) = (0.000000 4.000000 1.000000 5.000000)
force_inline v128_t vMergeLow(v128_t l, v128_t h)
{
#if defined(SIMD_ALTIVEC)
	return vec_mergel(l, h);
#elif defined(SIMD_SPU)
	return spu_shuffle(l, h, (vector unsigned char){
				 8,  9, 10, 11,
				24, 25, 26, 27,
				12, 13, 14, 15,
				28, 29, 30, 31
				});
#elif defined(SIMD_SSE)
	return _mm_unpackhi_ps(l, h);
#else
	return v128_t(l.z, h.z, l.w, h.w);
#endif
}

// vMergeHigh(vLoad(0,1,2,3), vLoad(4,5,6,7)) = (2.000000 6.000000 3.000000 7.000000)
force_inline v128_t vMergeHigh(v128_t l, v128_t h)
{
#if defined(SIMD_ALTIVEC)
	return vec_mergeh(l, h);
#elif defined(SIMD_SPU)
	return spu_shuffle(l, h, (vector unsigned char){
				 0,  1,  2,  3,
				16, 17, 18, 18,
				 4,  5,  6,  7,
				20, 21, 22, 23
				});	
#elif defined(SIMD_SSE)
	return _mm_unpacklo_ps(l, h);
#else
	return v128_t(l.x, h.x, l.y, h.y);
#endif
}

force_inline v128_t vMuxLow(v128_t l, v128_t h)
{
#if defined(SIMD_ALTIVEC)
	return vec_perm(l, h, (vector unsigned char){
				 0,  1,  2,  3,
				 4,  5,  6,  7,
				16, 17, 18, 18,
				20, 21, 22, 23
				});
#elif defined(SIMD_SPU)
	return spu_shuffle(l, h, (vector unsigned char){
				 0,  1,  2,  3,
				 4,  5,  6,  7,
				16, 17, 18, 18,
				20, 21, 22, 23
				});
#elif defined(SIMD_SSE)
	return _mm_movelh_ps(l, h);
#else
	return v128_t(l.x, l.y, h.x, h.y);
#endif
}

force_inline v128_t vMuxHigh(v128_t l, v128_t h)
{
#if defined(SIMD_ALTIVEC)
	return vec_perm(l, h, (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				24, 25, 26, 27,
				28, 29, 30, 31
				});
#elif defined(SIMD_SPU)
	return spu_shuffle(l, h, (vector unsigned char){
				 8,  9, 10, 11,
				12, 13, 14, 15,
				24, 25, 26, 27,
				28, 29, 30, 31
				});
#elif defined(SIMD_SSE)
	return _mm_movehl_ps(l, h);
#else
	return v128_t(l.z, l.w, h.z, h.w);
#endif
}

force_inline v128_t vSelMsk(v128_t m, v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_sel(v2, v1, (vector unsigned int)m);
#elif defined(SIMD_SPU)
	return spu_sel(v2, v1, (vector unsigned int)m);
#elif defined(SIMD_SSE)
	return vOr(vAnd(m, v1), _mm_andnot_ps(m, v2));
#else
	#error not defined yet
#endif
}


force_inline v128_t vMskW(v128_t _v, v128_t _w)
{
	return vSelMsk(vMask(1,1,1,0), _v, _w);
}

force_inline v128_t vOr(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_or(v1, v2);
#elif defined(SIMD_SPU)
	return spu_or(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_castsi128_ps(_mm_or_si128(_mm_castps_si128(v1), _mm_castps_si128(v2)));
#else
	return v128_t(v1.x | v2.x, v1.x | v2.x, v1.z | v2.z, v1.w | v2.w);
#endif
}

force_inline v128_t vAnd(v128_t v1, v128_t v2)
{
#if defined(SIMD_ALTIVEC)
	return vec_and(v1, v2);
#elif defined(SIMD_SPU)
	return spu_and(v1, v2);
#elif defined(SIMD_SSE)
	return _mm_castsi128_ps(_mm_and_si128(_mm_castps_si128(v1), _mm_castps_si128(v2)));
#else
	return v128_t(v1.x & v2.x, v1.x & v2.x, v1.z & v2.z, v1.w & v2.w);
#endif
}

force_inline v128_t vNot(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_xor(v, vScalar_uint32_t(0xffffffff));
#elif defined(SIMD_SPU)
	return spu_nand(v, v);
#elif defined(SIMD_SSE)
	return _mm_xor_ps(v, vScalar_uint32_t(0xffffffff));
#else
	return v128_t(~v.x, ~v.x, ~v.z, ~v.w);
#endif
}

force_inline v128_t vShiftLeftByte(v128_t v, uint32_t Bytes)
{
#if defined(SIMD_ALTIVEC)
	switch(Bytes)
	{
		case 0:	return vec_sld(v,v, 0);
		case 1:	return vec_sld(v,v, 1);
		case 2:	return vec_sld(v,v, 2);
		case 3:	return vec_sld(v,v, 3);
	}
#elif defined(SIMD_SPU)
	#error not defined yet
#elif defined(SIMD_SSE)
	switch(Bytes)
	{
	case 0: return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 0));
	case 1: return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 1));
	case 2: return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 2));
	}
	return _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(v), 3));
#else
	return v128_t(v.x << (Bytes*8), v.x << (Bytes*8), v.z << (Bytes*8), v.w << (Bytes*8));
#endif
}

force_inline uint32_t vExtract_uint32_t(v128_t v, uint8_t i)
{
#if defined(SIMD_ALTIVEC)
	uint32_t lU[4];
	vStore_uint32_t(&lU[0], v);
	return lU[i];
#elif defined(SIMD_SPU)
	return spu_extract((vector unsigned int)v, i);
#elif defined(SIMD_SSE)
	switch(i)
	{
		case 0: return _mm_cvtsi128_si32(_mm_shuffle_epi32((__m128i&)v, _MM_SHUFFLE(0,0,0,0)));
	 	case 1: return _mm_cvtsi128_si32(_mm_shuffle_epi32((__m128i&)v, _MM_SHUFFLE(0,0,0,1)));
		case 2: return _mm_cvtsi128_si32(_mm_shuffle_epi32((__m128i&)v, _MM_SHUFFLE(0,0,0,2)));
		case 3: return _mm_cvtsi128_si32(_mm_shuffle_epi32((__m128i&)v, _MM_SHUFFLE(0,0,0,3)));
	}

#else
#endif
}


force_inline fp32_t vExtract(v128_t v, uint8_t i)
{
#if defined(SIMD_ALTIVEC)
	fp32_t lF[4];
	vStore(&lF[0], v);
	return lF[i];
//	return vec_extract(v, i);
#elif defined(SIMD_SPU)
	return spu_extract(v, i);
#elif defined(SIMD_SSE)
	switch(i)
	{
		case 0:	return _mm_cvtss_fp32_t(v);
		case 1:	return _mm_cvtss_fp32_t(_mm_shuffle_ps(v, v, _MM_SHUFFLE(0,0,0,1)));
		case 2:	return _mm_cvtss_fp32_t(_mm_shuffle_ps(v, v, _MM_SHUFFLE(0,0,0,2)));
		case 3:	return _mm_cvtss_fp32_t(_mm_shuffle_ps(v, v, _MM_SHUFFLE(0,0,0,3)));
	}

#else
#endif
}

force_inline v128_t vConv_floats32(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_cts(v, 0);
#elif defined(SIMD_SPU)
	return (vector float)spu_convts(v, 0);
#elif defined(SIMD_SSE)
	return _mm_castsi128_ps(_mm_cvtps_epi32(v));
#else
#endif
}

force_inline v128_t vConv_floatuint32_t(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_ctu(v, 0);
#elif defined(SIMD_SPU)
	return (vector float)spu_convtu(v, 0);
#elif defined(SIMD_SSE)
	return _mm_castsi128_ps(_mm_cvttps_epi32(vMax(v, vScalar(0.0f))));
#else
#endif
}

force_inline v128_t vConv_s32float(v128_t v)
{
#if defined(SIMD_ALTIVEC)
	return vec_ctf((vector unsigned int)v, 0);
#elif defined(SIMD_SPU)
	return spu_convtf((vector unsigned int)v, 0);
#elif defined(SIMD_SSE)
	return _mm_cvtepi32_ps(_mm_castps_si128(v));
#else
#endif
}

force_inline v128_t vInf()
{
#if defined(SIMD_ALTIVEC)
	return vScalar(F32_INF);
#elif defined(SIMD_SPU)
	return vScalar(F32_INF);
#elif defined(SIMD_SSE)
	return vScalar(F32_INF);
#else
	return v128_t(F32_INF);
#endif
}

force_inline v128_t vNInf()
{
#if defined(SIMD_ALTIVEC)
	return vScalar(F32_NINF);
#elif defined(SIMD_SPU)
	return vScalar(F32_NINF);
#elif defined(SIMD_SSE)
	return vScalar(F32_NINF);
#else
	return v128_t(F32_NINF);
#endif
}

force_inline v128_t vZero()
{
#if defined(SIMD_ALTIVEC)
	return (v128_t){0.0f, 0.0f, 0.0f, 0.0f};
#elif defined(SIMD_SPU)
	return (v128_t){0.0f, 0.0f, 0.0f, 0.0f};
#elif defined(SIMD_SSE)
	return  _mm_setzero_ps();
#else
	return v128_t(0.0f);
#endif
}

force_inline v128_t vScalar(fp32_t f)
{
#if defined(SIMD_ALTIVEC)
	return (v128_t){f, f, f, f};
#elif defined(SIMD_SPU)
	return spu_splats(f);
#elif defined(SIMD_SSE)
	return _mm_set1_ps(f);
#else
	return v128_t(f);
#endif
}

force_inline v128_t vScalar_uint32_t(uint32_t u)
{
#if defined(SIMD_ALTIVEC)
	return (v128_t)((vector unsigned int){u, u, u, u});
#elif defined(SIMD_SPU)
	return (v128_t)spu_splats(u);
#elif defined(SIMD_SSE)
	const uint32_t align(16) p[4] = {u, u, u, u};
	return _mm_castsi128_ps(_mm_load_si128((__m128i*)p));
#else
	return v128_t(u);
#endif
}

force_inline v128_t vLoad(fp32_t x, fp32_t y, fp32_t z, fp32_t w)
{
#if defined(SIMD_ALTIVEC)
	return (v128_t){x, y, z, w};
#elif defined(SIMD_SPU)
	return (v128_t){x, y, z, w};
#elif defined(SIMD_SSE)
	const fp32_t align(16) p[4] = {x,y,z,w};
	return _mm_load_ps(p);
#else
	return v128_t(x, y, z, w);
#endif
}

force_inline v128_t vLoad_uint32_t(uint32_t x, uint32_t y, uint32_t z, uint32_t w)
{
#if defined(SIMD_ALTIVEC)
	return (v128_t)((vector unsigned int){x, y, z, w});
#elif defined(SIMD_SPU)
	return (v128_t)((vector unsigned int){x, y, z, w});
#elif defined(SIMD_SSE)
	const uint32_t align(16) p[4] = {x,y,z,w};
	return _mm_castsi128_ps(_mm_load_si128((__m128i*)p));
#else
	return v128_t(x, y, z, w);
#endif
}

force_inline v128_t vLoad(fp32_t *p)
{
#if defined(SIMD_ALTIVEC)
	return vec_ld(0, p);
#elif defined(SIMD_SPU)
	vector fp32_t r = *(vector fp32_t *) p;
	return r;
#elif defined(SIMD_SSE)
	return _mm_load_ps(p);
#else
	return v128_t(p[0], p[1], p[2], p[3]);
#endif
}

force_inline v128_t vLoad_uint32_t(uint32_t *p)
{
#if defined(SIMD_ALTIVEC)
	return (vector float)vec_ld(0, p);
#elif defined(SIMD_SPU)
	vector unsigned int r = *(vector unsigned int *) p;
	return (vector float)r;
#elif defined(SIMD_SSE)
	return _mm_castsi128_ps(_mm_load_si128((__m128i*)p));
#else
	return v128_t(p[0], p[1], p[2], p[3]);
#endif
}

force_inline void vStore(fp32_t *p, v128_t v)
{
#if defined(SIMD_ALTIVEC)
	vec_st(v, 0, p);
#elif defined(SIMD_SPU)
	fp32_t * pv = (fp32_t *)&v;
	p[0] = pv[0];
	p[1] = pv[1];
	p[2] = pv[2];
	p[3] = pv[3];
#elif defined(SIMD_SSE)
	_mm_store_ps(p, v);
#else
	p[0] = v.x; p[1] = v.y; p[2] = v.z; p[3] = v.w;
#endif
}

force_inline void vStore_uint32(uint32_t *p, v128_t v)
{
#if defined(SIMD_ALTIVEC)
	vec_st((vector unsigned int)v, 0, p);
#elif defined(SIMD_SPU)
	unsigned int * pv = (unsigned int *)&v;
	p[0] = pv[0];
	p[1] = pv[1];
	p[2] = pv[2];
	p[3] = pv[3];
#elif defined(SIMD_SSE)
	_mm_store_si128((__m128i*)p, (__m128i&)v);
#else
	p[0] = v.x; p[1] = v.y; p[2] = v.z; p[3] = v.w;
#endif
}

#ifdef __cplusplus
}
#endif

#endif
