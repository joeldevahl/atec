#ifndef SIMD_INTRINSICS_H
#define SIMD_INTRINSICS_H

#include <math.h>

//#define SIMD_NONE
//#define SIMD_ALTIVEC
//#define SIMD_SSE 1
//#define SIMD_SSE 2
//#define SIMD_SSE 3
//#define SIMD_SSE 4

#if defined(SIMD_NONE)
#	error "SIMD_NONE not supported for now."
#elif defined(SIMD_ALTIVEC)
#	include <altivec.h>
#	undef bool
#	undef pixel
	typedef vector float v128;



#elif defined(SIMD_SPU)
	typedef vector float v128;



#elif defined(SIMD_SSE)
#	include <xmmintrin.h>

#	if SIMD_SSE >= 2
#		include <emmintrin.h>
#	endif

#	if SIMD_SSE >= 3
#		include <pmmintrin.h>
#	endif

#	if SIMD_SSE >= 4
#		include <smmintrin.h>
#	endif

	typedef __m128 v128;



#else
#	error SIMD math not implemented for this platform
#endif

#include <core/types.h>

/*-----------------------------------------------------------------*\
 | ARITHMETIC OPERATION
\*-----------------------------------------------------------------*/

/*
	Function: simd_add
		Element wise addition of two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		The two vectors added together.
*/
force_inline v128 simd_add(v128 v1, v128 v2);
force_inline v128 simd_add_uint32(v128 v1, v128 v2);

/*
	Function: simd_sub
		Element wise subtraction of two vectors.
*/
force_inline v128 simd_sub(v128 v1, v128 v2);
force_inline v128 simd_sub_uint32(v128 v1, v128 v2);

/*
	Function: simd_mul
		Element wise multiplication of two vectors.
*/
force_inline v128 simd_mul(v128 v1, v128 v2);
force_inline v128 simd_mul_uint32(v128 v1, v128 v2);

/*
	Function: simd_div
		Element wise division of two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		The first vector divided by the second.
*/
force_inline v128 simd_div(v128 v1, v128 v2);
//force_inline v128 simd_div_uint32(v128 v1, v128 v2);

/*
	Function: simd_madd
		Multiplies two vectors and adds a third. Operates element wise.

	Parameters:
		v1 - first vector
		v2 - second vector
		v3 - third vector (add)

	Returns:
		(v1 * v2) + v3
*/
force_inline v128 simd_madd(v128 v1, v128 v2, v128 v3);

/*
	Function: simd_dP3
		3 element dot product (scalar product) between two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		Vector containing the dot product in all fields.
*/
force_inline v128 simd_dp3(v128 v1, v128 v2);

/*
	Function: simd_dP4
		4 element dot product (scalar product) between two vectors.

	Parameters:
		v1 - first vector
		v2 - second vector

	Returns:
		Vector containing the dot product in all fields.
*/
force_inline v128 simd_dp4(v128 v1, v128 v2);

/*
	Function: simd_sqrt
		Element wise square root of vector.
*/
force_inline v128 simd_sqrt(v128 v);

/*
	Function: simd_rce
		Element wise reciprocal (1/x) estimate.
*/
force_inline v128 simd_rce(v128 v);

/*
	Function: simd_rc
		Element wise reciprocal (1/x).
		Calulated using the reciprocal estimate and performing a one step Newton-Raphson refinement.
*/
force_inline v128 simd_rc(v128 v);

/*
	Function: simd_rsqe
		Element wise reciprocal square root (1/sqrt(x)) estimate.
*/
force_inline v128 simd_rsqe(v128 v);

/*
	Function: simd_rsq
		Element wise reciprocal square root (1/sqrt(x)).
		Calulated using the reciprocal square root estimate and performing a one step Newton-Raphson refinement.
*/
force_inline v128 simd_rsq(v128 v);

/*
	Function: simd_min
		Element wise min of two vectors
*/
force_inline v128 simd_min(v128 v1, v128 v2);

/*
	Function: simd_max
		Element wise max of two vectors
*/
force_inline v128 simd_max(v128 v1, v128 v2);

/*-----------------------------------------------------------------*\
 | COMPARSION OPERATIONS
\*-----------------------------------------------------------------*/

force_inline v128 simd_cmp_ge(v128 v1, v128 v2);
force_inline v128 simd_cmp_ge_uint32(v128 v1, v128 v2);
force_inline v128 simd_cmp_g(v128 v1, v128 v2);
force_inline v128 simd_cmp_g_uint32(v128 v1, v128 v2);
force_inline v128 simd_cmp_le(v128 v1, v128 v2);
force_inline v128 simd_cmp_le_uint32(v128 v1, v128 v2);
force_inline v128 simd_cmp_l(v128 v1, v128 v2);
force_inline v128 simd_cmp_l_uint32(v128 v1, v128 v2);
force_inline v128 simd_cmp_eq(v128 v1, v128 v2);
force_inline v128 simd_cmp_eq_uint32(v128 v1, v128 v2);

/*-----------------------------------------------------------------*\
 | BIT AND BYTE TWIDDLING
\*-----------------------------------------------------------------*/

// Agnostic to the type stored in v128
force_inline v128 simd_rot_r(v128 v);
force_inline v128 simd_rot_r2(v128 v);
force_inline v128 simd_rot_l(v128 v);
force_inline v128 simd_rot_l2(v128 v);
force_inline v128 simd_splat(v128 v, uint8_t i);
force_inline v128 simd_mux_low(v128 l, v128 h);
force_inline v128 simd_mux_high(v128 l, v128 h);
force_inline v128 simd_merge_low(v128 l, v128 h);
force_inline v128 simd_merge_high(v128 l, v128 h);
force_inline v128 simd_or(v128 v1, v128 v2);
force_inline v128 simd_and(v128 v1, v128 v2);
force_inline v128 simd_not(v128 v);
force_inline v128 simd_shift_left_byte(v128 v, uint32_t bytes);

force_inline v128 simd_select_mask(v128 m, v128 v1, v128 v2);
force_inline v128 simd_mask_w	(v128 v, v128 w);

/*-----------------------------------------------------------------*\
 | CONVERT, EXTRACT, LOAD AND STORE
\*-----------------------------------------------------------------*/

force_inline float simd_extract(v128 v, uint8_t i);
force_inline uint32_t simd_extract_uint32(v128 v, uint8_t i);

force_inline v128 simd_conv_int32fp32(v128 v);
force_inline v128 simd_conv_fp32int32(v128 v);
force_inline v128 simd_conv_scale_fp32int32(v128 v, v128 scale);

force_inline v128 simd_inf();
force_inline v128 simd_ninf();
force_inline v128 simd_zero();
force_inline v128 simd_scalar(float f);
force_inline v128 simd_scalar_uint32(uint32_t u);
force_inline v128 simd_load(float x, float y, float z, float w);
force_inline v128 simd_load_uint32(uint32_t x, uint32_t y, uint32_t z, uint32_t w);
force_inline v128 simd_load_ptr(float *p);
force_inline v128 simd_load_ptr_uint32(uint32_t *p);
force_inline void simd_store(float *p, v128 v);
force_inline void simd_store_uint32(uint32_t *p, v128 v);

#if defined(SIMD_ALTIVEC)
#	include <simd/intrinsics_altivec.h>
#elif defined(SIMD_SPU)
#	include <simd/intrinsics_spu.h>
#elif defined(SIMD_SSE)
#	include <simd/intrinsics_sse.h>
#endif

#endif
