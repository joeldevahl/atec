#include <memory/allocator.h>
#include <unittest/unittest.h>
#include <container/cobjpool.h>

#define NUM_ELEMS 1024

UNITTEST(container, cobjpool_create_destroy)
{
	cobjpool_t<uint16_t, uint16_t> pool(&allocator_default, NUM_ELEMS);
	TEST(pool.capacity() == NUM_ELEMS);

	uint16_t ids[NUM_ELEMS];
	for(uint16_t i = 0; i < NUM_ELEMS; ++i)
	{
		ids[i] =  pool.alloc_handle();
	}

	for(uint16_t i = 0; i < NUM_ELEMS; ++i)
	{
		pool.free_handle(ids[i]);
	}
}
