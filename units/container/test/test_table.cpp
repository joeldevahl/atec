#include <memory/allocator.h>
#include <unittest/unittest.h>
#include <container/table.h>

UNITTEST(container, table_create_destroy)
{
	table_t<uint16_t, uint16_t> table(&allocator_default, 8, 2);
	TEST(table.capacity() == 8);

	table.insert(1, 32);
	uint16_t* tmp = table.fetch(1);
	ASSERT(tmp != NULL);
	ASSERT(*tmp == 32);

	uint16_t val = 33;
	table[1] = val;
	val = table[1];
	ASSERT(val == 33);
}
