function Txt2Hex(settings, ...)
	local txt2hex = settings.txt2hex

	local outfiles = {}
	for infile in TableWalk({...}) do
		local outfile = GetOutputNameWithoutExt(infile) .. "." .. PathFileExt(infile) .. ".hex"
		local cmd = txt2hex.exe .. " " .. infile .. " > " .. outfile
		AddJob(outfile, "txt2hex " .. infile, cmd, infile, txt2hex.exe)
		table.insert(outfiles, outfile)
	end

	return outfiles
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.txt2hex = {}
		settings.txt2hex.exe = GetHostBinary("txt2hex")
	end)
end

function Unit.Init(self)
	self:DefaultInit()
	self.executable = true
	self.static_library = false
end
