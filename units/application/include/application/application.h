#ifndef APPLICATION_APPLICATION_H
#define APPLICATION_APPLICATION_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct application_create_params_s
{
	struct allocator_s* allocator;
	int argc;
	char** argv;
} application_create_params_t;

typedef struct application_s application_t;

application_t* application_create(application_create_params_t* params);
void application_destroy(application_t* app);

void application_update(application_t* app);

int application_get_argc(application_t* app);
char** application_get_argv(application_t* app);
bool application_is_running(application_t* app);

#ifdef __cplusplus
}
#endif

#endif // APPLICATION_APPLICATION_H
