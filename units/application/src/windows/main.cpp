#include <core/windows/windows.h>
#include <core/assert.h>
#include <memory/allocator.h>

#include <application/application.h>

int atec_main(application_t* app);

int APIENTRY WinMain(HINSTANCE _In_ hInstance, HINSTANCE _In_opt_ hPrevInstance, LPSTR _In_ lpCmdLine, int _In_ nCmdShow)
{
	application_create_params_t params;
	application_t* app;

	const size_t MAX_ARGS = 32;
	static char* argv[MAX_ARGS];
	int argc = 1;
	argv[0] = "appname.exe"; // TODO: get from hInstance?

	char* iter = lpCmdLine;
	while(*iter)
	{
		ASSERT(argc < MAX_ARGS);
		argv[argc++] = iter++;
		while(*iter != '\0' && *iter != ' ')
			++iter;

		if(*iter == ' ')
		{
			*iter = '\0';
			++iter;
		}
	}

	params.allocator = &allocator_default;
	params.argc = argc;
	params.argv = argv;
	app = application_create(&params);

	int val = atec_main(app);

	application_destroy(app);

	return val;
}
