#include <stdio.h>

#include <unittest/unittest.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <job/system.h>
#include <thread/thread.h>

UNITTEST(job, simple)
{
	for(int i = 0; i < 10; ++i)
	{
		job_system_t* system;

		job_system_create_params_t params;
		params.alloc = &allocator_default;
		params.num_threads = 1;
		params.num_queue_slots = 16;

		system = job_system_create(&params);
		TEST(system != NULL);
		job_system_destroy(system);
	}
}

struct test_job_context
{
	volatile uint32_t* signal0;
	volatile uint32_t* signal1;
	volatile uint32_t* signal2;
	volatile uint32_t* signal3;
};

static void single_job_test(void* data)
{
	struct test_job_context* context = (struct test_job_context*)data;
	*context->signal0 = 1;
}

UNITTEST(job, single_job)
{
	job_system_t* system;
	volatile uint32_t signal = 0;
	struct test_job_context context = { &signal, NULL, NULL, NULL };
	job_system_create_params_t params;
	params.alloc = &allocator_default;
	params.num_threads = 1;
	params.num_queue_slots = 16;
	system = job_system_create(&params);
	TEST(system != NULL);

	job_system_kick_ptr(system, single_job_test, &context, sizeof(context));

	while(signal == 0)
		thread_yield();

	job_system_destroy(system);
}

UNITTEST(job, bundle)
{
	job_system_t* system;
	volatile uint32_t signal = 0;
	test_job_context context = { &signal, NULL, NULL, NULL };
	job_system_result_t res;
	job_system_create_params_t params;
	params.alloc= &allocator_default;
	params.num_threads = 1;
	params.num_queue_slots = 16;
	system = job_system_create(&params);
	TEST(system != NULL);

	res = job_system_load_bundle(system, "local/debug/osx_ppc/libtestjob.dylib");
	TEST(res == JOB_SYSTEM_OK);
	res = job_system_load_bundle(system, "local/debug/osx_ppc/libtestjob.dylib");
	TEST(res == JOB_SYSTEM_OK);

	res = job_system_kick(system, hash_string("simple"), &context, sizeof(context));
	TEST(res == JOB_SYSTEM_OK);

	while(signal == 0)
		thread_yield();

	job_system_destroy(system);
}
