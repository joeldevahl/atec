#include <core/types.h>

struct test_job_context
{
	volatile uint32_t* signal0;
	volatile uint32_t* signal1;
	volatile uint32_t* signal2;
	volatile uint32_t* signal3;
};

extern "C" void simple(void* data)
{
	struct test_job_context* context = (struct test_job_context*)data;
	*context->signal0 = 1;
}
