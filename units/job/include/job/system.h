#ifndef JOB_SYSTEM_H
#define JOB_SYSTEM_H

#include <core/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct job_system_s job_system_t;

typedef struct job_system_create_params_s
{
	struct allocator_s* alloc;
	uint8_t num_threads;
	uint8_t num_queue_slots;
	uint8_t max_bundles;
} job_system_create_params_t;

typedef void (*job_function_t)(void*);

typedef struct job_ptr_descriptor_s
{
	job_function_t function;
	const char* name;
} job_ptr_descriptor_t;

typedef enum 
{
	JOB_SYSTEM_OK = 0,
	JOB_SYSTEM_COULD_NOT_LOAD_BUNDLE,
	JOB_SYSTEM_COULD_NOT_FIND_JOB_TABLE,
	JOB_SYSTEM_JOB_NOT_FOUND,
} job_system_result_t;

job_system_t* job_system_create(const job_system_create_params_t* params);
void job_system_destroy(job_system_t* system);

job_system_result_t job_system_load_bundle(job_system_t* system, const char* path);

job_system_result_t job_system_kick(job_system_t* system, uint32_t name, void* arg, size_t arg_size);
job_system_result_t job_system_kick_ptr(job_system_t* system, job_function_t function, void* arg, size_t arg_size);

#ifdef __cplusplus
}
#endif

#endif // JOB_SYSTEM_H
