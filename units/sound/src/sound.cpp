#include <sound/sound.h>

struct sound_s
{
	allocator_t* allocator;
};

sound_t* sound_create(allocator_t* allocator)
{
	sound_t* sound = (sound_t*)ALLOCATOR_ALLOC(allocator, sizeof(sound_t), ALIGNOF(sound_t));
	sound->allocator = allocator;
	return sound;
}

void sound_destroy(sound_t* sound)
{
	ALLOCATOR_FREE(sound->allocator, sound);
}
