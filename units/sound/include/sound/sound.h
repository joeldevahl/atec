#ifndef SOUND_SOUND_H
#define SOUND_SOUND_H

#include <core/types.h>
#include <memory/allocator.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sound_s sound_t;

sound_t* sound_create(allocator_t* allocator);
void sound_destroy(sound_t* sound);

#ifdef __cplusplus
}
#endif

#endif // SOUND_SOUND_H
