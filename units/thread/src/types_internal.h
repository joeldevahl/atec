#ifndef THREAD_TYPES_INTERNAL_H
#define THREAD_TYPES_INTERNAL_H

#ifdef FAMILY_WINDOWS
#	include <core/windows/windows.h>
#elif defined(FAMILY_UNIX)
#	include <pthread.h>
#else
#	error not implemented for this platform
#endif

#if defined(FAMILY_WINDOWS)

struct condition_s
{
	CONDITION_VARIABLE condition;
};

struct mutex_s
{
	CRITICAL_SECTION mutex;
};

#elif defined(FAMILY_UNIX)

struct condition_s
{
	pthread_cond_t condition;
};

struct mutex_s
{
	pthread_mutex_t mutex;
};

#else
#	error not implemented for this platform
#endif


#endif // THREAD_TYPES_INTERNAL_H
