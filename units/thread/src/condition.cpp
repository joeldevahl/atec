#include <core/assert.h>
#include <thread/condition.h>

#include "types_internal.h"

#include <cstdlib>

condition_t* condition_create()
{
	condition_t* condition = (condition_t*)malloc(sizeof(condition_t));
#if defined(FAMILY_WINDOWS)
	InitializeConditionVariable(&condition->condition);
#elif defined(FAMILY_UNIX)
	pthread_cond_init(&condition->condition, NULL);
#else
#	error not implemented for this platform
#endif
	return condition;
}

void condition_destroy(condition_t* condition)
{
#if defined(FAMILY_WINDOWS)

	// Conditions are not deleted on windows
#elif defined(FAMILY_UNIX)
	pthread_cond_destroy(&condition->condition);
#else
#	error not implemented for this platform
#endif
	free(condition);
}

void condition_wait(condition_t* condition, mutex_t* mutex)
{
#if defined(FAMILY_WINDOWS)
		SleepConditionVariableCS(&condition->condition, &mutex->mutex, INFINITE);
#elif defined(FAMILY_UNIX)
		pthread_cond_wait(&condition->condition, &mutex->mutex);
#else
#	error not implemented for this platform
#endif
}

void condition_signal(condition_t* condition)
{
#if defined(FAMILY_WINDOWS)
	WakeConditionVariable(&condition->condition);
#elif defined(FAMILY_UNIX)
	pthread_cond_signal(&condition->condition);
#else
#	error not implemented for this platform
#endif
}

void condition_broadcast(condition_t* condition)
{
#if defined(FAMILY_WINDOWS)
	WakeAllConditionVariable(&condition->condition);
#elif defined(FAMILY_UNIX)
	pthread_cond_broadcast(&condition->condition);
#else
#	error not implemented for this platform
#endif
}
