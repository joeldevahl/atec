#ifndef THREAD_THERAD_H
#define THREAD_THERAD_H

#include <thread/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*thread_main_func_t)(void*);

thread_t* thread_create(const char * name, thread_main_func_t func, void * data);
void thread_destroy(thread_t* thread);
void thread_join(thread_t* thread);
void thread_exit();
void thread_yield();

#ifdef __cplusplus
}
#endif

#endif // THREAD_THERAD_H
