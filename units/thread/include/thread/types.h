#ifndef THREAD_TYPES_H
#define THREAD_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct thread_s thread_t;

typedef struct condition_s condition_t;

typedef struct mutex_s mutex_t;

#ifdef __cplusplus
}
#endif

#endif // THREAD_TYPES_H
