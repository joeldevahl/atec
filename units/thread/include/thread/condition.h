#ifndef THREAD_CONDITION_H
#define THREAD_CONDITION_H

#include <thread/types.h>

#ifdef __cplusplus
extern "C" {
#endif

condition_t* condition_create();

void condition_destroy(condition_t* condition);

void condition_wait(condition_t* condition, mutex_t* mutex);

void condition_signal(condition_t* condition);

void condition_broadcast(condition_t* condition);

#ifdef __cplusplus
}
#endif

#endif // THREAD_CONDITION_H
