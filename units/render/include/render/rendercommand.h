#ifndef RENDER_RENDERCOMMAND_H
#define RENDER_RENDERCOMMAND_H

#include <render/types.h>

typedef enum
{
	RENDER_COMMAND_BEGIN_PASS = 0,
	RENDER_COMMAND_END_PASS,
	RENDER_COMMAND_BLIT,
	RENDER_COMMAND_DRAW,
	RENDER_COMMAND_QUERY,
	MAX_RENDER_COMMANDS,
} render_command_t;

typedef struct render_command_header_s
{
	render_command_t command : 16;
	uint16_t size;
} render_command_header_t;

typedef struct ALIGN(16) render_command_begin_pass_s
{
	render_command_header_t header;

	vgpu_render_pass_t* render_pass;
} render_command_begin_pass_t;

typedef struct ALIGN(16) render_command_end_pass_s
{
	render_command_header_t header;
} render_command_end_pass_t;

typedef struct ALIGN(16) render_command_blit_s
{
	render_command_header_t header;

	vgpu_texture_t* src;
} render_command_blit_t;

typedef struct ALIGN(16) render_command_draw_s
{
	render_command_header_t header;

	vgpu_pipeline_t* pipeline;

	render_resource_slot_binding_t resource_slot_bindings[VGPU_MAX_RESOURCE_SLOTS];
	vgpu_buffer_t* instance_data;
	uint32_t instance_data_offset;

	vgpu_buffer_t* index_buffer;
	vgpu_data_type_t index_type;

	uint32_t instance_count;
	uint32_t draw_offset;
	uint32_t draw_count;
} render_command_draw_t;

typedef struct ALIGN(16) render_command_query_s
{
	render_command_header_t header;

	render_pipeline_set_t* pipeline_set;
	vgpu_resource_table_t* global_resource_table;

	size_t num_instance_lists;
	size_t* instance_list_lengths;
	render_instance_t** instance_lists;

	uint64_t and_mask;
	uint64_t not_mask;
} render_command_query_t;

#endif // RENDER_RENDERCOMMAND_H
