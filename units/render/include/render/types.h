#ifndef RENDER_TYPES_H
#define RENDER_TYPES_H

#include <core/types.h>
#include <core/defines.h>
#include <vgpu.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct render_s render_t;
typedef struct render_pipeline_set_s render_pipeline_set_t;

typedef struct render_pipeline_set_part_s
{
	vgpu_pipeline_t* pipeline;
	uint64_t flags;
} render_pipeline_set_part_t;

typedef struct render_resource_slot_binding_s
{
	vgpu_resource_type_t type;
	union
	{
		vgpu_resource_table_t* resource_table;
		struct
		{
			vgpu_buffer_t* buffer;
			size_t offset;
			size_t num_bytes;
		} buffer;
	};
} render_resource_slot_binding_t;

typedef struct render_instance_s
{
	vgpu_data_type_t index_type;
	vgpu_buffer_t* index_buffer;

	uint32_t instance_count;
	uint32_t draw_offset;
	uint32_t draw_count;

	render_resource_slot_binding_t resource_slot_bindings[VGPU_MAX_RESOURCE_SLOTS - 1];
	uint64_t flags;
} render_instance_t;

typedef struct render_sort_key_s
{
	uint64_t key;
	const render_instance_t* instance;
} render_sort_key_t;

#ifdef __cplusplus
}
#endif

#endif // RENDER_TYPES_H
