#ifndef RENDER_RENDER_H
#define RENDER_RENDER_H

#include <render/types.h>
#include <render/rendercommand.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************\
 *
 *  Render structures
 *
\******************************************************************************/

typedef struct render_create_params_s
{
	struct allocator_s* allocator;
	struct vgpu_device_s* device;
	struct vgpu_command_list_s* command_list;
} render_create_params_t;

typedef const void* (*render_command_handler_t)(render_t* render, const void* start, const void* end, void* user_pointer);

typedef struct render_stats_s
{
	size_t last_consumed;
	size_t max_consumed;
	uint64_t total_cpu_draw_time;
	uint64_t device_cpu_draw_time;
} render_stats_t;

/******************************************************************************\
 *
 *  Render operations
 *
\******************************************************************************/

render_t* render_create(const render_create_params_t* params);

void render_destroy(render_t* render);

void render_get_frame_stats(render_t* render, render_stats_t* out_stats);

/******************************************************************************\
 *
 *  Pipeline set handling
 *
\******************************************************************************/

render_pipeline_set_t* render_create_pipeline_set(render_t* render, const render_pipeline_set_part_t* parts, size_t num_parts);

void render_destroy_pipeline_set(render_t* render, render_pipeline_set_t* pipeline_set);

/******************************************************************************\
 *
 *  Command handling
 *
\******************************************************************************/

void render_register_command_handler(render_t* render, render_command_t command, render_command_handler_t handler, void* user_pointer);

void render_replace_command_handler(render_t* render, render_command_t command, render_command_handler_t handler, void* user_pointer);

void render_process_commands(render_t* render, const void* start, const void* end);

#ifdef __cplusplus
}
#endif

#endif // RENDER_RENDER_H
