#include <algorithm>

#include <render/types.h>
#include <render/render.h>
#include <render/rendercommand.h>

#include <vgpu.h>

#include <core/assert.h>
#include <core/defines.h>
#include <core/time.h>

#include <memory/allocator.h>
#include <memory/memory.h>

#include <container/array.h>

struct render_pipeline_set_s
{
	vgpu_pipeline_t* pipelines[32];
	uint64_t flags[32];
	size_t num_pipelines;
};

#define RENDER_MAX_COMMAND_HANDLERS 256

struct render_s
{
	allocator_t* main_heap;
	allocator_t* frame_heap;

	vgpu_device_t* device;
	vgpu_command_list_t* command_list;

	render_command_handler_t handlers[RENDER_MAX_COMMAND_HANDLERS];
	void* handler_user_pointers[RENDER_MAX_COMMAND_HANDLERS];

	render_stats_t stats;
};

static const void* render_handler_begin_pass(render_t* render, const void* start, const void* end, void* user_pointer);
static const void* render_handler_end_pass(render_t* render, const void* start, const void* end, void* user_pointer);
static const void* render_handler_blit(render_t* render, const void* start, const void* end, void* user_pointer);
static const void* render_handler_draw(render_t* render, const void* start, const void* end, void* user_pointer);
static const void* render_handler_query(render_t* render, const void* start, const void* end, void* user_pointer);

render_t* render_create(const render_create_params_t* params)
{
	render_t* render = ALLOCATOR_ALLOC_TYPE(params->allocator, render_t);
	render->main_heap = params->allocator;
	render->frame_heap = allocator_incheap_create(params->allocator, 10 * 1024 * 1024);
	render->device = params->device;
	render->command_list = params->command_list;

	memory_zero(render->handlers, sizeof(render->handlers));
	memory_zero(render->handler_user_pointers, sizeof(render->handler_user_pointers));
	render_register_command_handler(render, RENDER_COMMAND_BEGIN_PASS, render_handler_begin_pass, NULL);
	render_register_command_handler(render, RENDER_COMMAND_END_PASS, render_handler_end_pass, NULL);
	render_register_command_handler(render, RENDER_COMMAND_BLIT, render_handler_blit, NULL);
	render_register_command_handler(render, RENDER_COMMAND_DRAW, render_handler_draw, NULL);
	render_register_command_handler(render, RENDER_COMMAND_QUERY, render_handler_query, NULL);

	memory_zero(&render->stats, sizeof(render->stats));

	return render;
}
void render_destroy(render_t* render)
{
	ALLOCATOR_FREE(render->main_heap, render);
}

void render_get_frame_stats(render_t* render, render_stats_t* out_stats)
{
	memory_copy(out_stats, &render->stats, sizeof(*out_stats));
}

render_pipeline_set_t* render_create_pipeline_set(render_t* render, const render_pipeline_set_part_t* parts, size_t num_parts)
{
	ASSERT(parts, "must provide parts to pipeline set");
	render_pipeline_set_t* pipeline_set = ALLOCATOR_ALLOC_TYPE(render->main_heap, render_pipeline_set_t);
	ASSERT(num_parts < ARRAY_LENGTH(pipeline_set->pipelines), "too many parts for pipeline set");

	for(size_t i = 0; i < num_parts; ++i)
	{
		pipeline_set->pipelines[i] = parts[i].pipeline;
		pipeline_set->flags[i] = parts[i].flags;
	}
	pipeline_set->num_pipelines = num_parts;

	return pipeline_set;
}

void render_destroy_pipeline_set(render_t* render, render_pipeline_set_t* pipeline_set)
{
	ALLOCATOR_FREE(render->main_heap, pipeline_set);
}

static bool render_sort_predicate(const render_sort_key_t& l, const render_sort_key_t& r)
{
	return l.key < r.key;
}

static const void* render_handler_begin_pass(render_t* render, const void* start, const void* end, void* user_pointer)
{
	vgpu_device_t* device = render->device;
	const uint8_t* ptr = (const uint8_t*)start;
	const render_command_header_t* header = (const render_command_header_t*)ptr;
	ASSERT(header->command == RENDER_COMMAND_BEGIN_PASS);
	const render_command_begin_pass_t* begin_pass = (const render_command_begin_pass_t*)ptr;

	uint64_t start_time = time_current();

	vgpu_begin_render_pass(device, begin_pass->render_pass);

	uint64_t end_time = time_current();
	render->stats.device_cpu_draw_time += end_time - start_time;

	return ptr + header->size;
}

static const void* render_handler_end_pass(render_t* render, const void* start, const void* end, void* user_pointer)
{
	vgpu_device_t* device = render->device;
	const uint8_t* ptr = (const uint8_t*)start;
	const render_command_header_t* header = (const render_command_header_t*)ptr;
	ASSERT(header->command == RENDER_COMMAND_END_PASS);
	const render_command_end_pass_t* end_pass = (const render_command_end_pass_t*)ptr;

	uint64_t start_time = time_current();

	vgpu_end_render_pass(device);

	uint64_t end_time = time_current();
	render->stats.device_cpu_draw_time += end_time - start_time;

	return ptr + header->size;
}

static const void* render_handler_blit(render_t* render, const void* start, const void* end, void* user_pointer)
{
	vgpu_command_list_t* command_list = render->command_list;
	const uint8_t* ptr = (const uint8_t*)start;
	const render_command_header_t* header = (const render_command_header_t*)ptr;
	ASSERT(header->command == RENDER_COMMAND_BLIT);
	const render_command_blit_t* blit = (const render_command_blit_t*)ptr;

	uint64_t start_time = time_current();

	//vgpu_blit(command_list, blit->src);
	
	uint64_t end_time = time_current();
	render->stats.device_cpu_draw_time += end_time - start_time;

	return ptr + header->size;
}

static const void* render_handler_draw(render_t* render, const void* start, const void* end, void* user_pointer)
{
	vgpu_command_list_t* command_list = render->command_list;
	const uint8_t* ptr = (const uint8_t*)start;
	const render_command_header_t* header = (const render_command_header_t*)ptr;
	while (ptr < end && header->command == RENDER_COMMAND_DRAW)
	{
		const render_command_draw_t* batch = (const render_command_draw_t*)header;

		vgpu_cmd_set_pipeline(command_list, batch->pipeline);

		for (int i = 0; i < ARRAY_LENGTH(batch->resource_slot_bindings); ++i)
		{
			switch (batch->resource_slot_bindings[i].type)
			{
			case VGPU_RESOURCE_TABLE:
				vgpu_cmd_set_resource_table(command_list, i, batch->resource_slot_bindings[i].resource_table);
				break;
			case VGPU_RESOURCE_BUFFER:
				vgpu_cmd_set_buffer(command_list, i,
					batch->resource_slot_bindings[i].buffer.buffer,
					batch->resource_slot_bindings[i].buffer.offset,
					batch->resource_slot_bindings[i].buffer.num_bytes);
				break;
			default:
				break;
			}
		}

		if (batch->index_buffer != nullptr)
		{
			vgpu_cmd_set_index_buffer(command_list, batch->index_type, batch->index_buffer);
			vgpu_cmd_draw_indexed(command_list, 0, batch->instance_count, batch->draw_offset, batch->draw_count, 0);
		}
		else
		{
			vgpu_cmd_draw(command_list, 0, batch->instance_count, batch->draw_offset, batch->draw_count);
		}

		ptr += header->size;
		header = (const render_command_header_t*)ptr;
	}
	return ptr;
}

static const void* render_handler_query(render_t* render, const void* start, const void* end, void* user_pointer)
{
	vgpu_command_list_t* command_list = render->command_list;
	const uint8_t* ptr = (const uint8_t*)start;
	const render_command_header_t* header = (const render_command_header_t*)ptr;
	while(ptr < end && header->command == RENDER_COMMAND_QUERY)
	{
		const render_command_query_t* pass = (const render_command_query_t*)header;

		size_t num_instances = 0;
		for(size_t il = 0; il < pass->num_instance_lists; ++il)
		{
			num_instances += pass->instance_list_lengths[il];
		}

		render_sort_key_t* sort_list = (render_sort_key_t*)ALLOCATOR_ALLOC(render->frame_heap, sizeof(render_sort_key_t) * num_instances, ALIGNOF(render_sort_key_t)); // TODO: worst case allocation

		size_t count = 0;
		uint64_t and_mask = pass->and_mask;
		uint64_t not_mask = pass->not_mask;
		for(size_t il = 0; il < pass->num_instance_lists; ++il)
		{
			size_t n = pass->instance_list_lengths[il];
			const render_instance_t* l = pass->instance_lists[il];

			for(size_t i = 0; i < n; ++i)
			{
				// TODO: better flag matching, probably user defined
				const render_instance_t* instance = l + i;
				uint64_t flags = instance->flags;
				uint64_t combined = ((flags & and_mask) == and_mask) & ((flags & not_mask) == 0);
				if(combined)
				{
					sort_list[count].key = 0; // TODO: better and dynamic sort order
					sort_list[count].instance = instance;
					count++;
				}
			}
		}

		std::sort(sort_list, sort_list + count, render_sort_predicate);

		render_pipeline_set_t* pipeline_set = pass->pipeline_set;

		// shadow state to avoid some API calls
		vgpu_pipeline_t* prev_pipeline = nullptr;
		render_resource_slot_binding_t prev_resource_slot_bindings[VGPU_MAX_RESOURCE_SLOTS - 1];
		memset(prev_resource_slot_bindings, 0, sizeof(prev_resource_slot_bindings));
		vgpu_data_type_t prev_index_type = (vgpu_data_type_t)-1;
		vgpu_buffer_t* prev_index_buffer = nullptr;

		bool needs_reset = true;
		for(size_t i = 0; i < count; ++i)
		{
			const render_instance_t* instance = sort_list[i].instance;

			// TODO: better searching, probably user defined
			vgpu_pipeline_t* pipeline = nullptr;
			for(size_t p = 0; p < pipeline_set->num_pipelines; ++p)
			{
				if((instance->flags & pipeline_set->flags[p]) == instance->flags)
				{
					pipeline = pipeline_set->pipelines[p];
					break;
				}
			}

			uint64_t start_time = time_current();

			bool pipeline_changed = pipeline != prev_pipeline;
			if (pipeline_changed)
			{
				vgpu_cmd_set_pipeline(command_list, pipeline);
				prev_pipeline = pipeline;
				needs_reset = true;
			}

			// TODO: should invalid check be done here or inside?

			for (int i = 0; i < ARRAY_LENGTH(instance->resource_slot_bindings); ++i)
			{
				bool changed = memcmp(&prev_resource_slot_bindings[i], &instance->resource_slot_bindings[i], sizeof(prev_resource_slot_bindings[i])) != 0;
				if (needs_reset || changed)
				{
					switch (instance->resource_slot_bindings[i].type)
					{
					case VGPU_RESOURCE_TABLE:
						vgpu_cmd_set_resource_table(command_list, i, instance->resource_slot_bindings[i].resource_table);
						break;
					case VGPU_RESOURCE_BUFFER:
						vgpu_cmd_set_buffer(command_list, i,
							instance->resource_slot_bindings[i].buffer.buffer,
							instance->resource_slot_bindings[i].buffer.offset,
							instance->resource_slot_bindings[i].buffer.num_bytes);
						break;
					default:
						break;
					}
					memcpy(&prev_resource_slot_bindings[i], &instance->resource_slot_bindings[i], sizeof(prev_resource_slot_bindings[i]));
				}
			}

			if (needs_reset)
			{
				vgpu_cmd_set_resource_table(command_list, 3, pass->global_resource_table);
				needs_reset = false;
			}

			if(instance->index_buffer != nullptr)
			{
				if (prev_index_buffer != instance->index_buffer || prev_index_type != instance->index_type)
				{
					vgpu_cmd_set_index_buffer(command_list, instance->index_type, instance->index_buffer);
					prev_index_buffer = instance->index_buffer;
					prev_index_type = instance->index_type;
				}
				vgpu_cmd_draw_indexed(command_list, 0, instance->instance_count, instance->draw_offset, instance->draw_count, 0);
			}
			else
			{
				vgpu_cmd_draw(command_list, 0, instance->instance_count, instance->draw_offset, instance->draw_count);
			}

			uint64_t end_time = time_current();
			render->stats.device_cpu_draw_time += end_time - start_time;
		}

		ptr += header->size;
		header = (const render_command_header_t*)ptr;
	}
	return ptr;
}

void render_register_command_handler(render_t* render, render_command_t command, render_command_handler_t handler, void* user_pointer)
{
	ASSERT((int)command < RENDER_MAX_COMMAND_HANDLERS, "invalid command enum");
	ASSERT(render->handlers[command] == NULL, "handler already registered to this command");
	ASSERT(render->handler_user_pointers[command] == NULL, "handler already registered to this command");
	render_replace_command_handler(render, command, handler, user_pointer);
}

void render_replace_command_handler(render_t* render, render_command_t command, render_command_handler_t handler, void* user_pointer)
{
	ASSERT((int)command < RENDER_MAX_COMMAND_HANDLERS, "invalid command enum");
	render->handlers[command] = handler;
	render->handler_user_pointers[command] = user_pointer;
}

void render_process_commands(render_t* render, const void* start, const void* end)
{
	render->stats.device_cpu_draw_time = 0;

	uint64_t draw_start = time_current();
	const uint8_t* ptr = (const uint8_t*)start;
	while(ptr < end)
	{
		const render_command_header_t* header = (const render_command_header_t*)ptr;
		uint8_t command = header->command;
		void* user_pointer = render->handler_user_pointers[command];
		render_command_handler_t handler = render->handlers[command];
		ASSERT(handler);
		ptr = (const uint8_t*)handler(render, header, end, user_pointer);
	}
	uint64_t draw_end = time_current();

	size_t consumed = allocator_incheap_bytes_consumed(render->frame_heap);

	render->stats.last_consumed = consumed;
	if(render->stats.max_consumed < consumed)
		render->stats.max_consumed = consumed;
	render->stats.total_cpu_draw_time = draw_end - draw_start;

	allocator_incheap_reset(render->frame_heap);
}
