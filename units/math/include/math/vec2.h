#ifndef MATH_VEC2_H
#define MATH_VEC2_H

#ifdef __cplusplus

#include <math/types.h>

force_inline vec2_t operator+(vec2_t v1, vec2_t v2) { return vec2_t(v1.x+v2.x,v1.y+v2.y); }
force_inline vec2_t operator-(vec2_t v1, vec2_t v2) { return vec2_t(v1.x-v2.x,v1.y-v2.y); }
force_inline vec2_t operator*(vec2_t v1, vec2_t v2) { return vec2_t(v1.x*v2.x,v1.y*v2.y); }
force_inline vec2_t operator/(vec2_t v1, vec2_t v2) { return vec2_t(v1.x/v2.x,v1.y/v2.y); }

force_inline vec2_t operator+(vec2_t v, float s) { return vec2_t(v.x+s, v.y+s); }
force_inline vec2_t operator-(vec2_t v, float s) { return vec2_t(v.x-s, v.y-s); }
force_inline vec2_t operator*(vec2_t v, float s) { return vec2_t(v.x*s, v.y*s); }
force_inline vec2_t operator/(vec2_t v, float s) { return vec2_t(v.x/s, v.y/s); }

force_inline float dot(vec2_t v1, vec2_t v2)
{
	return v1.x*v2.x + v1.y*v2.y;
}

#endif

#endif //MATH_VEC2_H
