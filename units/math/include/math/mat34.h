#ifndef MATH_MAT34_H
#define MATH_MAT34_H

#ifdef __cplusplus

#include <math/vec3.h>
#include <math/vec4.h>

mat34_t mat34_t::identity()
{
	return mat34_t(1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f);
}

vec4_t row(const mat34_t& m, const int i)
{
	return vec4_t(m.c[0].v[i], m.c[1].v[i], m.c[2].v[i], m.c[3].v[i]);
}

vec3_t col(const mat34_t& m, const int i)
{
	return m.c[i];
}

#endif

#endif //MATH_MAT34_H
