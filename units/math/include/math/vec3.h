#ifndef MATH_VEC3_H
#define MATH_VEC3_H

#ifdef __cplusplus

#include <math/sysmath.h>
#include <math/types.h>

#include <core/defines.h>

FORCE_INLINE vec3_t operator+(vec3_t v1, vec3_t v2) { return vec3_t(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z); }
FORCE_INLINE vec3_t operator-(vec3_t v1, vec3_t v2) { return vec3_t(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z); }
FORCE_INLINE vec3_t operator*(vec3_t v1, vec3_t v2) { return vec3_t(v1.x*v2.x, v1.y*v2.y, v1.z*v2.z); }
FORCE_INLINE vec3_t operator/(vec3_t v1, vec3_t v2) { return vec3_t(v1.x/v2.x, v1.y/v2.y, v1.z/v2.z); }

FORCE_INLINE vec3_t operator+(vec3_t v, float s) { return vec3_t(v.x+s, v.y+s, v.z+s); }
FORCE_INLINE vec3_t operator-(vec3_t v, float s) { return vec3_t(v.x-s, v.y-s, v.z-s); }
FORCE_INLINE vec3_t operator*(vec3_t v, float s) { return vec3_t(v.x*s, v.y*s, v.z*s); }
FORCE_INLINE vec3_t operator/(vec3_t v, float s) { return vec3_t(v.x/s, v.y/s, v.z/s); }

FORCE_INLINE float dot(vec3_t v1, vec3_t v2)
{
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

FORCE_INLINE vec3_t cross(vec3_t v1, vec3_t v2)
{
	return vec3_t(v1.y*v2.z - v1.z*v2.y,
				v1.z*v2.x - v1.x*v2.z,
				v1.x*v2.y - v1.y*v2.x);
}

FORCE_INLINE float length_sqr(vec3_t v)
{
	return dot(v, v);
}

FORCE_INLINE float length(vec3_t v)
{
	return sqrtf(dot(v, v));
}

FORCE_INLINE vec3_t normalize(vec3_t v)
{
	return v / length(v);
}

#endif

#endif //MATH_VEC3_H
