#ifndef MATH_MAT4_H
#define MATH_MAT4_H

#ifdef __cplusplus

#include <core/assert.h>
#include <math/sysmath.h>
#include <math/vec3.h>
#include <math/vec4.h>

inline vec4_t operator*(const mat4_t& m, const vec4_t v)
{
	return vec4_t(m.c[0].x*v.x + m.c[1].x*v.y + m.c[2].x*v.z + m.c[3].x*v.w,
				m.c[0].y*v.x + m.c[1].y*v.y + m.c[2].y*v.z + m.c[3].y*v.w,
				m.c[0].z*v.x + m.c[1].z*v.y + m.c[2].z*v.z + m.c[3].z*v.w,
				m.c[0].w*v.x + m.c[1].w*v.y + m.c[2].w*v.z + m.c[3].w*v.w);
}

inline vec3_t operator*(const mat4_t& m, const vec3_t v)
{
	vec4_t temp(m.c[0].x*v.x + m.c[1].x*v.y + m.c[2].x*v.z + m.c[3].x,
			  m.c[0].y*v.x + m.c[1].y*v.y + m.c[2].y*v.z + m.c[3].y,
			  m.c[0].z*v.x + m.c[1].z*v.y + m.c[2].z*v.z + m.c[3].z,
			  m.c[0].w*v.x + m.c[1].w*v.y + m.c[2].w*v.z + m.c[3].w);
	return vec3_t(temp.x / temp.w, temp.y / temp.w, temp.z / temp.w);
}

inline mat4_t operator*(const mat4_t& m1, const mat4_t& m2)
{
	return mat4_t(m1 * m2.c[0],
				m1 * m2.c[1],
				m1 * m2.c[2],
				m1 * m2.c[3]);
}

inline mat4_t operator*(const mat4_t& m, const float s)
{
	return mat4_t(m.c[0] * s,
				m.c[1] * s,
				m.c[2] * s,
				m.c[3] * s);
}

inline mat4_t operator/(const mat4_t& m, const float s)
{
	return mat4_t(m.c[0] / s,
				m.c[1] / s,
				m.c[2] / s,
				m.c[3] / s);
}

inline mat4_t transpose(const mat4_t& m)
{
	return mat4_t(m.c[0].x, m.c[0].y, m.c[0].z, m.c[0].w,
				m.c[1].x, m.c[1].y, m.c[1].z, m.c[1].w,
				m.c[2].x, m.c[2].y, m.c[2].z, m.c[2].w,
				m.c[3].x, m.c[3].y, m.c[3].z, m.c[3].w);
}

inline mat4_t inverse(const mat4_t& m)
{
	float sf00 = m.c[2].v[2] * m.c[3].v[3] - m.c[3].v[2] * m.c[2].v[3];
	float sf01 = m.c[2].v[1] * m.c[3].v[3] - m.c[3].v[1] * m.c[2].v[3];
	float sf02 = m.c[2].v[1] * m.c[3].v[2] - m.c[3].v[1] * m.c[2].v[2];
	float sf03 = m.c[2].v[0] * m.c[3].v[3] - m.c[3].v[0] * m.c[2].v[3];
	float sf04 = m.c[2].v[0] * m.c[3].v[2] - m.c[3].v[0] * m.c[2].v[2];
	float sf05 = m.c[2].v[0] * m.c[3].v[1] - m.c[3].v[0] * m.c[2].v[1];
	float sf06 = m.c[1].v[2] * m.c[3].v[3] - m.c[3].v[2] * m.c[1].v[3];
	float sf07 = m.c[1].v[1] * m.c[3].v[3] - m.c[3].v[1] * m.c[1].v[3];
	float sf08 = m.c[1].v[1] * m.c[3].v[2] - m.c[3].v[1] * m.c[1].v[2];
	float sf09 = m.c[1].v[0] * m.c[3].v[3] - m.c[3].v[0] * m.c[1].v[3];
	float sf10 = m.c[1].v[0] * m.c[3].v[2] - m.c[3].v[0] * m.c[1].v[2];
	float sf11 = m.c[1].v[1] * m.c[3].v[3] - m.c[3].v[1] * m.c[1].v[3];
	float sf12 = m.c[1].v[0] * m.c[3].v[1] - m.c[3].v[0] * m.c[1].v[1];
	float sf13 = m.c[1].v[2] * m.c[2].v[3] - m.c[2].v[2] * m.c[1].v[3];
	float sf14 = m.c[1].v[1] * m.c[2].v[3] - m.c[2].v[1] * m.c[1].v[3];
	float sf15 = m.c[1].v[1] * m.c[2].v[2] - m.c[2].v[1] * m.c[1].v[2];
	float sf16 = m.c[1].v[0] * m.c[2].v[3] - m.c[2].v[0] * m.c[1].v[3];
	float sf17 = m.c[1].v[0] * m.c[2].v[2] - m.c[2].v[0] * m.c[1].v[2];
	float sf18 = m.c[1].v[0] * m.c[2].v[1] - m.c[2].v[0] * m.c[1].v[1];

	mat4_t inv(
			+ m.c[1].v[1] * sf00 - m.c[1].v[2] * sf01 + m.c[1].v[3] * sf02,
			- m.c[1].v[0] * sf00 + m.c[1].v[2] * sf03 - m.c[1].v[3] * sf04,
			+ m.c[1].v[0] * sf01 - m.c[1].v[1] * sf03 + m.c[1].v[3] * sf05,
			- m.c[1].v[0] * sf02 + m.c[1].v[1] * sf04 - m.c[1].v[2] * sf05,

			- m.c[0].v[1] * sf00 + m.c[0].v[2] * sf01 - m.c[0].v[3] * sf02,
			+ m.c[0].v[0] * sf00 - m.c[0].v[2] * sf03 + m.c[0].v[3] * sf04,
			- m.c[0].v[0] * sf01 + m.c[0].v[1] * sf03 - m.c[0].v[3] * sf05,
			+ m.c[0].v[0] * sf02 - m.c[0].v[1] * sf04 + m.c[0].v[2] * sf05,

			+ m.c[0].v[1] * sf06 - m.c[0].v[2] * sf07 + m.c[0].v[3] * sf08,
			- m.c[0].v[0] * sf06 + m.c[0].v[2] * sf09 - m.c[0].v[3] * sf10,
			+ m.c[0].v[0] * sf11 - m.c[0].v[1] * sf09 + m.c[0].v[3] * sf12,
			- m.c[0].v[0] * sf08 + m.c[0].v[1] * sf10 - m.c[0].v[2] * sf12,

			- m.c[0].v[1] * sf13 + m.c[0].v[2] * sf14 - m.c[0].v[3] * sf15,
			+ m.c[0].v[0] * sf13 - m.c[0].v[2] * sf16 + m.c[0].v[3] * sf17,
			- m.c[0].v[0] * sf14 + m.c[0].v[1] * sf16 - m.c[0].v[3] * sf18,
			+ m.c[0].v[0] * sf15 - m.c[0].v[1] * sf17 + m.c[0].v[2] * sf18);

	float d = 
		+ m.c[0].v[0] * inv.c[0].v[0] 
		+ m.c[0].v[1] * inv.c[1].v[0] 
		+ m.c[0].v[2] * inv.c[2].v[0] 
		+ m.c[0].v[3] * inv.c[3].v[0];

	return inv / d;
}

inline mat4_t translate(float x, float y, float z)
{
	return mat4_t(1.0f, 0.0f, 0.0f, x,
				0.0f, 1.0f, 0.0f, y,
				0.0f, 0.0f, 1.0f, z,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t translate(vec3_t v)
{
	return mat4_t(1.0f, 0.0f, 0.0f, v.x,
				0.0f, 1.0f, 0.0f, v.y,
				0.0f, 0.0f, 1.0f, v.z,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t scale(float x, float y, float z)
{
	return mat4_t(x,    0.0f, 0.0f, 0.0f,
				0.0f, y,    0.0f, 0.0f,
				0.0f, 0.0f, z,    0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t scale(vec3_t v)
{
	return mat4_t(v.x,  0.0f, 0.0f, 0.0f,
				0.0f, v.y,  0.0f, 0.0f,
				0.0f, 0.0f, v.z,  0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t rotate_x(float angle)
{
	float c = cosf(angle);
	float s = sinf(angle);
	return mat4_t(1.0f, 0.0f, 0.0f, 0.0f,
				0.0f,    c,   -s, 0.0f,
				0.0f,    s,    c, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t rotate_y(float angle)
{
	float c = cosf(angle);
	float s = sinf(angle);
	return mat4_t(   c, 0.0f,   -s, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				  -s, 0.0f,    c, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t rotate_z(float angle)
{
	float c = cosf(angle);
	float s = sinf(angle);
	return mat4_t(   c,   -s, 0.0f, 0.0f,
				   s,    c, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
}

inline mat4_t perspective(float vfov, float aspect, float znear, float zfar)
{
	float f = 1.0f/tanf(vfov*((float)M_PI/360.0f));

	return mat4_t(f/aspect, 0.0f,	0.0f, 0.0f,
				0.0f, f, 0.0f, 0.0f,
				0.0f, 0.0f,	(zfar+znear)/(znear-zfar), (2.0f*zfar*znear)/(znear-zfar),
				0.0f, 0.0f, -1.0f, 0.0f);
}

inline mat4_t ortho(float left, float right, float bottom, float top, float near, float far)
{
	return mat4_t(2.0f/(right-left), 0.0f, 0.0f, -((right+left)/(right-left)),
				0.0f, 2.0f/(top-bottom), 0.0f,-((top+bottom)/(top-bottom)),
				0.0f, 0.0f, -2.0f/(far-near), -((far+near)/(far-near)),
				0.0f, 0.0f, -1.0f, 0.0f);
}

inline mat4_t lookat(vec3_t eye_pos, vec3_t lookat_pos, vec3_t up_vec)
{
	vec3_t back = normalize(eye_pos - lookat_pos);
	vec3_t up = normalize(up_vec - back* dot(back, up_vec));
	vec3_t right = normalize(cross(up, back));
	return mat4_t(right.x, right.y, right.z, -eye_pos.x,
				up.x,    up.y,    up.z,    -eye_pos.y,
				back.x,  back.y,  back.z,  -eye_pos.z,
				0.0f, 0.0f, 0.0f, 1.0f);
}

#endif

#endif //MATH_MAT4_H
