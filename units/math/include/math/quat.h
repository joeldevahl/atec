#ifndef MATH_QUAT_H
#define MATH_QUAT_H

#ifdef __cplusplus

#include <math/types.h>

static quat_t quat_t::identity()
{
	return quat_t(1.0f, 0.0f, 0.0f, 0.0f);
}

force_inline quat_t operator*(const quat_t l, const quat_t r)
{
    return quat_t(l.r * r.r - dot(l.v, r.v),
		    l.r * r.v + l.v * r.r + cross(l.v, r.v));
}

force_inline float dot(const quat_t l, const quat_t r)
{
	return l.r * r.r + dot(l.v * r.v);
}

force_inline float length(const quat_t q)
{
	return sqrtf(dot(q, q));
}

force_inline quat_t normalize(const quat_t q)
{
	float l = length(q);
	return quat_t(q.r / l, q.v / l);
}

force_inline invert(const quat_t q)
{
	float d = dot(q, q);
	return quat_t(q.r / d, -q.v / d);
}

force_inline mat3_t to_mat3(const quat_t q)
{
    return mat3_t(1.0f - 2 * (q.v.y * q.v.y + q.v.z * q.v.z),
				2.0f     * (q.v.x * q.v.y + q.v.z * q.r),
				2.0f     * (q.v.z * q.v.x - q.v.y * q.r),
				2.0f     * (q.v.x * q.v.y - q.v.z * q.r),
				1.0f - 2 * (q.v.z * q.v.z + q.v.x * q.v.x),
				2.0f     * (q.v.y * q.v.z + q.v.x * q.r),
				2.0f     * (q.v.z * q.v.x + q.v.y * q.r),
				2.0f     * (q.v.y * q.v.z - q.v.x * q.r),
				1.0f - 2 * (q.v.y * q.v.y + q.v.x * q.v.x));
}

force_inline mat34_t to_mat34(const quat_t q)
{
	return mat34_t(1.0f - 2 * (q.v.y * q.v.y + q.v.z * q.v.z),
				 2.0f     * (q.v.x * q.v.y + q.v.z * q.r),
				 2.0f     * (q.v.z * q.v.x - q.v.y * q.r),
				 0.0f,
				 2.0f     * (q.v.x * q.v.y - q.v.z * q.r),
				 1.0f - 2 * (q.v.z * q.v.z + q.v.x * q.v.x),
				 2.0f     * (q.v.y * q.v.z + q.v.x * q.r),
				 0.0f,
				 2.0f     * (q.v.z * q.v.x + q.v.y * q.r),
				 2.0f     * (q.v.y * q.v.z - q.v.x * q.r),
				 1.0f - 2 * (q.v.y * q.v.y + q.v.x * q.v.x),
				 0.0f);
}

force_inline mat4_t to_mat4(const quat_t q)
{
    return mat4_t(1.0f - 2 * (q.v.y * q.v.y + q.v.z * q.v.z),
				2.0f     * (q.v.x * q.v.y + q.v.z * q.r),
				2.0f     * (q.v.z * q.v.x - q.v.y * q.r),
				0.0f,
				2.0f     * (q.v.x * q.v.y - q.v.z * q.r),
				1.0f - 2 * (q.v.z * q.v.z + q.v.x * q.v.x),
				2.0f     * (q.v.y * q.v.z + q.v.x * q.r),
				0.0f,
				2.0f     * (q.v.z * q.v.x + q.v.y * q.r),
				2.0f     * (q.v.y * q.v.z - q.v.x * q.r),
				1.0f - 2 * (q.v.y * q.v.y + q.v.x * q.v.x),
				0.0f,
				0.0f,
				0.0f,
				0.0f,
				1.0f);
}

#endif

#endif // MATH_QUAT_H
