#ifndef MATH_VEC4_H
#define MATH_VEC4_H

#ifdef __cplusplus

#include <math/types.h>

FORCE_INLINE vec4_t operator+(vec4_t v1, vec4_t v2) { return vec4_t(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z, v1.w+v2.w); }
FORCE_INLINE vec4_t operator-(vec4_t v1, vec4_t v2) { return vec4_t(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z, v1.w-v2.w); }
FORCE_INLINE vec4_t operator*(vec4_t v1, vec4_t v2) { return vec4_t(v1.x*v2.x, v1.y*v2.y, v1.z*v2.z, v1.w*v2.w); }
FORCE_INLINE vec4_t operator/(vec4_t v1, vec4_t v2) { return vec4_t(v1.x/v2.x, v1.y/v2.y, v1.z/v2.z, v1.w/v2.w); }

FORCE_INLINE vec4_t operator+(vec4_t v, float s) { return vec4_t(v.x+s, v.y+s, v.z+s, v.w+s); }
FORCE_INLINE vec4_t operator-(vec4_t v, float s) { return vec4_t(v.x-s, v.y-s, v.z-s, v.w-s); }
FORCE_INLINE vec4_t operator*(vec4_t v, float s) { return vec4_t(v.x*s, v.y*s, v.z*s, v.w*s); }
FORCE_INLINE vec4_t operator/(vec4_t v, float s) { return vec4_t(v.x/s, v.y/s, v.z/s, v.w/s); }

#endif

#endif //MATH_VEC4_H
