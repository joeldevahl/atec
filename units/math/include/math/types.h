#ifndef MATH_TYPES_H
#define MATH_TYPES_H

typedef struct vec2_s
{
	union
	{
		struct
		{
			float x;
			float y;
		};
		float v[2];
	};

#ifdef __cplusplus
	vec2_s() {}
	vec2_s(float s) : x(s), y(s) {}
	vec2_s(float v0, float v1) : x(v0), y(v1) {}
	vec2_s(const vec2_s& o) : x(o.x), y(o.y) {}
#endif
} vec2_t;

typedef struct vec3_s
{
	union
	{
		struct
		{
			float x;
			float y;
			float z;
		};
		float v[3];
	};

#ifdef __cplusplus
	vec3_s() {}
	vec3_s(float s) : x(s), y(s), z(s) {}
	vec3_s(float v0, float v1, float v2) : x(v0), y(v1), z(v2) {}
	vec3_s(const vec3_s& o) : x(o.x), y(o.y), z(o.z) {}
#endif
} vec3_t;

typedef struct vec4_s
{
	union
	{
		struct
		{
			float x;
			float y;
			float z;
			float w;
		};
		float v[4];
	};

#ifdef __cplusplus
	vec4_s() {}
	vec4_s(float s) : x(s), y(s), z(s), w(s) {}
	vec4_s(float v0, float v1, float v2, float v3) : x(v0), y(v1), z(v2), w(v3) {}
	vec4_s(const vec4_s& o) : x(o.x), y(o.y), z(o.z), w(o.w) {}
#endif
} vec4_t;

typedef struct mat2_s
{
	vec2_t c[2];

#ifdef __cplusplus
	mat2_s() {}

	mat2_s(float s)
	{
		c[0] = vec2_t(s);
		c[1] = vec2_t(s);
	}

	/*
	* m00 m01
	* m10 m11
	*/
	mat2_s(float m00, float m01, float m10, float m11)
	{
		c[0] = vec2_t(m00, m10);
		c[1] = vec2_t(m01, m11);
	}

	mat2_s(vec2_t c0, vec2_t c1)
	{
		c[0] = c0;
		c[1] = c1;
	}

	mat2_s(const mat2_s& o)
	{
		c[0] = o.c[0];
		c[1] = o.c[1];
	}
#endif
} mat2_t;

typedef struct mat3_s
{
	vec3_t c[3];

#ifdef __cplusplus
	mat3_s() {}

	mat3_s(float s)
	{
		c[0] = vec3_t(s);
		c[1] = vec3_t(s);
		c[2] = vec3_t(s);
	}

	mat3_s(vec3_t c0, vec3_t c1, vec3_t c2)
	{
		c[0] = c0;
		c[1] = c1;
		c[2] = c2;
	}

	/*
	* m00 m01 m02
	* m10 m11 m12
	* m20 m21 m22
	*/
	mat3_s(float m00, float m01, float m02,
		 float m10, float m11, float m12,
		 float m20, float m21, float m22)
	{
		c[0] = vec3_t(m00, m10, m20);
		c[1] = vec3_t(m01, m11, m21);
		c[2] = vec3_t(m02, m12, m22);
	}

	mat3_s(const mat3_s& o)
	{
		c[0] = o.c[0];
		c[1] = o.c[1];
		c[2] = o.c[2];
	}

	static mat3_s identity();
#endif
} mat3_t;

typedef struct mat34_s
{
	vec3_t c[4];

#ifdef __cplusplus
	mat34_s() {}

	mat34_s(float s)
	{
		c[0] = vec3_t(s);
		c[1] = vec3_t(s);
		c[2] = vec3_t(s);
		c[3] = vec3_t(s);
	}

	mat34_s(vec3_t c0, vec3_t c1, vec3_t c2, vec3_t c3)
	{
		c[0] = c0;
		c[1] = c1;
		c[2] = c2;
		c[3] = c3;
	}

	/*
	* m00 m01 m02 m03
	* m10 m11 m12 m13
	* m20 m21 m22 m23
	*/
	mat34_s(float m00, float m01, float m02, float m03,
		  float m10, float m11, float m12, float m13,
		  float m20, float m21, float m22, float m23)
	{
		c[0] = vec3_t(m00, m10, m20);
		c[1] = vec3_t(m01, m11, m21);
		c[2] = vec3_t(m02, m12, m22);
		c[3] = vec3_t(m03, m13, m23);
	}

	mat34_s(const mat34_s& o)
	{
		c[0] = o.c[0];
		c[1] = o.c[1];
		c[2] = o.c[2];
		c[3] = o.c[3];
	}

	static mat34_s identity();
#endif
} mat34_t;

typedef struct mat4_s
{
	vec4_t c[4];

#ifdef __cplusplus
	mat4_s() {}

	mat4_s(float s)
	{
		c[0] = vec4_t(s);
		c[1] = vec4_t(s);
		c[2] = vec4_t(s);
		c[3] = vec4_t(s);
	}

	/*
	* m00 m01 m02 m03
	* m10 m11 m12 m13
	* m20 m21 m22 m23
	* m30 m31 m32 m33
	*/
	mat4_s(float m00, float m01, float m02, float m03,
		 float m10, float m11, float m12, float m13,
		 float m20, float m21, float m22, float m23,
		 float m30, float m31, float m32, float m33)
	{
		c[0] = vec4_t(m00, m10, m20, m30);
		c[1] = vec4_t(m01, m11, m21, m31);
		c[2] = vec4_t(m02, m12, m22, m32);
		c[3] = vec4_t(m03, m13, m23, m33);
	}

	mat4_s(vec4_t c0, vec4_t c1, vec4_t c2, vec4_t c3)
	{
		c[0] = c0;
		c[1] = c1;
		c[2] = c2;
		c[3] = c3;
	}

	mat4_s(const mat4_s& o)
	{
		c[0] = o.c[0];
		c[1] = o.c[1];
		c[2] = o.c[2];
		c[3] = o.c[3];
	}

	static mat4_s identity()
	{
		return mat4_s(1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f);
	}
#endif
} mat4_t;

typedef struct quat_s
{
	float r;
	vec3_t v;

#ifdef __cplusplus
	quat_s() {}
	quat_s(float s, float i, float j, float k) : r(s), v(i, j, k) {}
	quat_s(float s, vec3_t d) : r(s), v(d) {}
	quat_s(const quat_s& o) : r(o.r), v(o.v) {}

	static quat_s identity();
#endif
} quat_t;

#endif // MATH_TYPES_H
