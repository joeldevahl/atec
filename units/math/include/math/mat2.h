#ifndef MATH_MAT2_H
#define MATH_MAT2_H

#ifdef __cplusplus

#include <core/assert.h>
#include <math/vec2.h>
#include <math/sysmath.h>

force_inline vec2_t operator*(const mat2_t m, const vec2_t v)
{
	return vec2_t(m.c[0].x*v.x + m.c[1].x*v.y,
				m.c[0].y*v.x + m.c[1].y*v.y);
}

force_inline mat2_t operator*(const mat2_t m1, const mat2_t m2)
{
	return mat2_t(m1 * m2.c[0],
				m1 * m2.c[1]);
}

force_inline float determinant(const mat2_t m)
{
	return m.c[0].x*m.c[1].y - m.c[1].x*m.c[0].y;
}

force_inline mat2_t inverse(const mat2_t m)
{
	float d = determinant(m);
	ASSERT(d != 0.0f, "cannot invert degenerate matrix (determinant is zero)\n");

	float inv_d = 1.0f / d;
	return mat2_t( inv_d * m.c[1].y, -inv_d * m.c[1].x,
				-inv_d * m.c[0].y,  inv_d * m.c[0].x);
}

force_inline mat2_t rotation(float angle)
{
	float c = cosf(angle), s = sinf(angle);
	return mat2_t(c, -s,
				s,  c);
}

#endif

#endif //MATH_MAT2_H
