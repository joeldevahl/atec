#ifndef MATH_MAT3_H
#define MATH_MAT3_H

#ifdef __cplusplus

#include <math/vec3.h>

#include <core/defines.h>

FORCE_INLINE mat3_t gen_on_from_z(mat3_t m)
{
	mat3_t res;
	res.c[2] = m.c[2];

	vec3_t y_guess = cross(res.c[2], vec3_t(1.0f, 0.0f, 0.0f));
	if(dot(y_guess, y_guess) < 0.01)
		y_guess = cross(res.c[2], vec3_t(0.0f, 1.0f, 0.0f));
	res.c[1] = normalize(y_guess);
	res.c[0] = cross(res.c[1], res.c[2]);

	return res;
}

#endif

#endif //MATH_MAT3_H
