#ifndef MEMORY_MEMORY_H
#define MEMORY_MEMORY_H

#include <core/types.h>
#include <core/defines.h>

#ifdef __cplusplus
extern "C" {
#endif

void memory_copy(void* RESTRICT dst, const void* RESTRICT src, size_t num_bytes);

void memory_zero(void* dst, uint32_t num_bytes);

void memory_set(void* dst, uint32_t num_bytes, uint32_t val);

#ifdef __cplusplus
}
#endif

#endif //MEMORY_MEMORY_H
