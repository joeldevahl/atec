#include <core/assert.h>
#include <core/windows/windows.h>
#include <memory/allocator.h>
#include <memory/memory.h>
#include <input/input.h>
#include <input/keys.h>

static const input_key_t translate_keyboard_input[256] = {

	/* 0x00 */
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_BACK,
	INPUT_KEY_TAB,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_CLEAR,
	INPUT_KEY_RETURN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0x10 */
	INPUT_KEY_SHIFT,
	INPUT_KEY_CONTROL,
	INPUT_KEY_MENU,
	INPUT_KEY_PAUSE,
	INPUT_KEY_CAPITAL,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_ESCAPE,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0x20 */
	INPUT_KEY_SPACE,
	INPUT_KEY_PRIOR,
	INPUT_KEY_NEXT,
	INPUT_KEY_END,
	INPUT_KEY_HOME,
	INPUT_KEY_LEFT,
	INPUT_KEY_UP,
	INPUT_KEY_RIGHT,
	INPUT_KEY_DOWN,
	INPUT_KEY_SELECT,
	INPUT_KEY_PRINT,
	INPUT_KEY_EXECUTE,
	INPUT_KEY_SNAPSHOT,
	INPUT_KEY_INSERT,
	INPUT_KEY_DELETE,
	INPUT_KEY_HELP,

	/* 0x30 */
	INPUT_KEY_0,
	INPUT_KEY_1,
	INPUT_KEY_2,
	INPUT_KEY_3,
	INPUT_KEY_4,
	INPUT_KEY_5,
	INPUT_KEY_6,
	INPUT_KEY_7,
	INPUT_KEY_8,
	INPUT_KEY_9,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0x40 */
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_A,
	INPUT_KEY_B,
	INPUT_KEY_C,
	INPUT_KEY_D,
	INPUT_KEY_E,
	INPUT_KEY_F,
	INPUT_KEY_G,
	INPUT_KEY_H,
	INPUT_KEY_I,
	INPUT_KEY_J,
	INPUT_KEY_K,
	INPUT_KEY_L,
	INPUT_KEY_M,
	INPUT_KEY_N,
	INPUT_KEY_O,

	/* 0x50 */
	INPUT_KEY_P,
	INPUT_KEY_Q,
	INPUT_KEY_R,
	INPUT_KEY_S,
	INPUT_KEY_T,
	INPUT_KEY_U,
	INPUT_KEY_V,
	INPUT_KEY_W,
	INPUT_KEY_X,
	INPUT_KEY_Y,
	INPUT_KEY_Z,
	INPUT_KEY_LWIN,
	INPUT_KEY_RWIN,
	INPUT_KEY_APPS,
	INPUT_KEY_SLEEP,

	/* 0x60 */
	INPUT_KEY_NUMPAD0,
	INPUT_KEY_NUMPAD1,
	INPUT_KEY_NUMPAD2,
	INPUT_KEY_NUMPAD3,
	INPUT_KEY_NUMPAD4,
	INPUT_KEY_NUMPAD5,
	INPUT_KEY_NUMPAD6,
	INPUT_KEY_NUMPAD7,
	INPUT_KEY_NUMPAD8,
	INPUT_KEY_NUMPAD9,
	INPUT_KEY_MULTIPLY,
	INPUT_KEY_ADD,
	INPUT_KEY_SEPARATOR,
	INPUT_KEY_SUBTRACT,
	INPUT_KEY_DECIMAL,
	INPUT_KEY_DIVIDE,

	/* 0x70 */
	INPUT_KEY_F1,
	INPUT_KEY_F2,
	INPUT_KEY_F3,
	INPUT_KEY_F4,
	INPUT_KEY_F5,
	INPUT_KEY_F6,
	INPUT_KEY_F7,
	INPUT_KEY_F8,
	INPUT_KEY_F9,
	INPUT_KEY_F10,
	INPUT_KEY_F11,
	INPUT_KEY_F12,
	INPUT_KEY_F13,
	INPUT_KEY_F14,
	INPUT_KEY_F15,
	INPUT_KEY_F16,

	/* 0x80 */
	INPUT_KEY_F17,
	INPUT_KEY_F18,
	INPUT_KEY_F19,
	INPUT_KEY_F20,
	INPUT_KEY_F21,
	INPUT_KEY_F22,
	INPUT_KEY_F23,
	INPUT_KEY_F24,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0x90 */
	INPUT_KEY_NUMLOCK,
	INPUT_KEY_SCROLL,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0xA0 */
	INPUT_KEY_LSHIFT,
	INPUT_KEY_RSHIFT,
	INPUT_KEY_LCONTROL,
	INPUT_KEY_RCONTROL,
	INPUT_KEY_LMENU,
	INPUT_KEY_RMENU,
	INPUT_KEY_BROWSER_BACK,
	INPUT_KEY_BROWSER_FORWARD,
	INPUT_KEY_BROWSER_REFRESH,
	INPUT_KEY_BROWSER_STOP,
	INPUT_KEY_BROWSER_SEARCH,
	INPUT_KEY_BROWSER_FAVORITES,
	INPUT_KEY_BROWSER_HOME,
	INPUT_KEY_VOLUME_MUTE,
	INPUT_KEY_VOLUME_DOWN,
	INPUT_KEY_VOLUME_UP,

	/* 0xB0 */
	INPUT_KEY_MEDIA_NEXT_TRACK,
	INPUT_KEY_MEDIA_PREV_TRACK,
	INPUT_KEY_MEDIA_STOP,
	INPUT_KEY_MEDIA_PLAY_PAUSE,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0xC0 */
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0xD0 */
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0xE0 */
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,

	/* 0xF0 */
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
};

struct input_s
{
	allocator_t* allocator;

	HHOOK wndproc_hook;
	HHOOK getmsg_hook;

	uint32_t num_keyboards;
	uint32_t num_mice;
	uint32_t num_gamepads;
	input_keyboard_state_t keyboard_states[INPUT_MAX_KEYBOARDS];
	input_mouse_state_t mouse_states[INPUT_MAX_MICE];
	input_gamepad_state_t gamepad_states[INPUT_MAX_GAMEPADS];
};

static LRESULT CALLBACK input_wndproc_hook(int code, WPARAM wparam, LPARAM lparam)
{
	CWPSTRUCT* hook = (CWPSTRUCT*)lparam; (void)hook;
	return CallNextHookEx(NULL, code, wparam, lparam);
}

static LRESULT CALLBACK input_getmsg_hook(int code, WPARAM wparam, LPARAM lparam)
{
	MSG* hook = (MSG*)lparam;

	switch(hook->message)
	{
		case WM_INPUT:
		{
			RAWINPUT raw;
			UINT size = sizeof(raw);
			HRESULT hr = GetRawInputData((HRAWINPUT)hook->lParam, RID_INPUT, &raw, &size, sizeof(RAWINPUTHEADER));
			ASSERT(SUCCEEDED(hr));

			if (raw.header.dwType == RIM_TYPEKEYBOARD)
			{
				const RAWKEYBOARD& rawKB = raw.data.keyboard;
				UINT virtualKey = rawKB.VKey;
				UINT scanCode = rawKB.MakeCode;
				UINT flags = rawKB.Flags;

				if (virtualKey == 255)
				{
					// discard "fake keys" which are part of an escaped sequence
					break;
				}
				else if (virtualKey == VK_SHIFT)
				{
					// correct left-hand / right-hand SHIFT
					virtualKey = MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
				}
				else if (virtualKey == VK_NUMLOCK)
				{
					// correct PAUSE/BREAK and NUM LOCK silliness, and set the extended bit
					scanCode = (MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC) | 0x100);
				}

				// e0 and e1 are escape sequences used for certain special keys, such as PRINT and PAUSE/BREAK.
				// see http://www.win.tue.nl/~aeb/linux/kbd/scancodes-1.html
				const bool isE0 = ((flags & RI_KEY_E0) != 0);
				const bool isE1 = ((flags & RI_KEY_E1) != 0);

				if (isE1)
				{
					// for escaped sequences, turn the virtual key into the correct scan code using MapVirtualKey.
					// however, MapVirtualKey is unable to map VK_PAUSE (this is a known bug), hence we map that by hand.
					if (virtualKey == VK_PAUSE)
						scanCode = 0x45;
					else
						scanCode = MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC);
				}

			}
			break;
		}
	}

	return CallNextHookEx(NULL, code, wparam, lparam);
}

input_t* input_create(const input_create_params_t* params)
{
	input_t* input = ALLOCATOR_ALLOC_TYPE(params->allocator, input_t);
	memory_zero(input, sizeof(*input));
	input->allocator = params->allocator;

	RAWINPUTDEVICE rid;
	rid.usUsagePage = 0x01;
	rid.usUsage = 0x06;
	rid.dwFlags = RIDEV_NOLEGACY;
	rid.hwndTarget = (HWND)params->window;
	RegisterRawInputDevices(&rid, 1, sizeof(rid));

	input->wndproc_hook = SetWindowsHookEx(WH_CALLWNDPROC, input_wndproc_hook, GetModuleHandle(NULL), GetCurrentThreadId());
	ASSERT(input->wndproc_hook != NULL);

	input->getmsg_hook = SetWindowsHookEx(WH_GETMESSAGE, input_getmsg_hook, GetModuleHandle(NULL), GetCurrentThreadId());
	ASSERT(input->getmsg_hook != NULL);

	return input;
}

void input_destroy(input_t* input)
{
	UnhookWindowsHookEx(input->getmsg_hook);
	UnhookWindowsHookEx(input->wndproc_hook);
	ALLOCATOR_FREE(input->allocator, input);
}

void input_update(input_t* input)
{
}

void input_keyboard_state(input_t* input, uint32_t index, input_keyboard_state_t* keyboard)
{
	memory_copy(keyboard, &input->keyboard_states[index], sizeof(input_keyboard_state_t));
}

void input_mouse_state(input_t* input, uint32_t index, input_mouse_state_t* mouse)
{
	memory_copy(mouse, &input->mouse_states[index], sizeof(input_mouse_state_t));
}

void input_gamepad_state(input_t* input, uint32_t index, input_gamepad_state_t* gamepad)
{
	memory_copy(gamepad, &input->gamepad_states[index], sizeof(input_gamepad_state_t));
}
