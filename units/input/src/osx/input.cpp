#include <IOKit/hid/IOHIDLib.h>

#include <core/assert.h>
#include <memory/memory.h>
#include <input/input.h>
#include <input/keys.h>

static const input_key_t translate_input[256] = {
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_UNKNOWN,
	INPUT_KEY_A,
	INPUT_KEY_B,
	INPUT_KEY_C,
	INPUT_KEY_D,
	INPUT_KEY_E,
	INPUT_KEY_F,
	INPUT_KEY_G,
	INPUT_KEY_H,
	INPUT_KEY_I,
	INPUT_KEY_J,
	INPUT_KEY_K,
	INPUT_KEY_L,
	INPUT_KEY_M,
	INPUT_KEY_N,
	INPUT_KEY_O,
	INPUT_KEY_P,
	INPUT_KEY_Q,
	INPUT_KEY_R,
	INPUT_KEY_S,
	INPUT_KEY_T,
	INPUT_KEY_U,
	INPUT_KEY_V,
	INPUT_KEY_W,
	INPUT_KEY_X,
	INPUT_KEY_Y,
	INPUT_KEY_Z,
	INPUT_KEY_1,
	INPUT_KEY_2,
	INPUT_KEY_3,
	INPUT_KEY_4,
	INPUT_KEY_5,
	INPUT_KEY_6,
	INPUT_KEY_7,
	INPUT_KEY_8,
	INPUT_KEY_9,
	INPUT_KEY_0,
	INPUT_KEY_ENTER,
	INPUT_KEY_ESCAPE,
	INPUT_KEY_BACKSPACE,
	INPUT_KEY_TAB,
	INPUT_KEY_SPACEBAR,
	INPUT_KEY_HYPHEN,
	INPUT_KEY_EQUALSIGN,
	INPUT_KEY_OPENBRACKET,
	INPUT_KEY_CLOSEBRACKET,
	INPUT_KEY_BACKSLASH,
	INPUT_KEY_NONUSPOUND,
	INPUT_KEY_SEMICOLON,
	INPUT_KEY_QUOTE,
	INPUT_KEY_GRAVEACCENT_AND_TILDE,
	INPUT_KEY_COMMA,
	INPUT_KEY_PERIOD,
	INPUT_KEY_SLASH,
	INPUT_KEY_CAPSLOCK,
	INPUT_KEY_F1,
	INPUT_KEY_F2,
	INPUT_KEY_F3,
	INPUT_KEY_F4,
	INPUT_KEY_F5,
	INPUT_KEY_F6,
	INPUT_KEY_F7,
	INPUT_KEY_F8,
	INPUT_KEY_F9,
	INPUT_KEY_F10,
	INPUT_KEY_F11,
	INPUT_KEY_F12,
	INPUT_KEY_PRINTSCREEN,
	INPUT_KEY_SCROLLLOCK,
	INPUT_KEY_PAUSE,
	INPUT_KEY_INSERT,
	INPUT_KEY_HOME,
	INPUT_KEY_PAGEUP,
	INPUT_KEY_DELETEFORWARD,
	INPUT_KEY_END,
	INPUT_KEY_PAGEDOWN,
	INPUT_KEY_RIGHTARROW,
	INPUT_KEY_LEFTARROW,
	INPUT_KEY_DOWNARROW,
	INPUT_KEY_UPARROW,
	INPUT_KEY_KP_NUMLOCK,
	INPUT_KEY_KP_SLASH,
	INPUT_KEY_KP_ASTERISK,
	INPUT_KEY_KP_HYPHEN,
	INPUT_KEY_KP_PLUS,
	INPUT_KEY_KP_ENTER,
	INPUT_KEY_KP_1,
	INPUT_KEY_KP_2,
	INPUT_KEY_KP_3,
	INPUT_KEY_KP_4,
	INPUT_KEY_KP_5,
	INPUT_KEY_KP_6,
	INPUT_KEY_KP_7,
	INPUT_KEY_KP_8,
	INPUT_KEY_KP_9,
	INPUT_KEY_KP_0,
	INPUT_KEY_KP_PERIOD,
	INPUT_KEY_NONUSBACKSLASH,
	INPUT_KEY_APPLICATION,
	INPUT_KEY_POWER,
	INPUT_KEY_KP_EQUALSIGN,
	INPUT_KEY_F13,
	INPUT_KEY_F14,
	INPUT_KEY_F15,
	INPUT_KEY_F16,
	INPUT_KEY_F17,
	INPUT_KEY_F18,
	INPUT_KEY_F19,
	INPUT_KEY_F20,
	INPUT_KEY_F21,
	INPUT_KEY_F22,
	INPUT_KEY_F23,
	INPUT_KEY_F24,
	INPUT_KEY_EXECUTE,
	INPUT_KEY_HELP,
	INPUT_KEY_MENU,
	INPUT_KEY_SELECT,
	INPUT_KEY_STOP,
	INPUT_KEY_AGAIN,
	INPUT_KEY_UNDO,
	INPUT_KEY_CUT,
	INPUT_KEY_COPY,
	INPUT_KEY_PASTE,
	INPUT_KEY_FIND,
	INPUT_KEY_MUTE,
	INPUT_KEY_VOLUMEUP,
	INPUT_KEY_VOLUMEDOWN,
	INPUT_KEY_LOCKINGCAPSLOCK,
	INPUT_KEY_LOCKINGNUMLOCK,
	INPUT_KEY_LOCKINGSCROLLLOCK,
	INPUT_KEY_KP_COMMA,
	INPUT_KEY_KP_EQUALSIGNAS400,
	INPUT_KEY_INTERNATIONAL1,
	INPUT_KEY_INTERNATIONAL2,
	INPUT_KEY_INTERNATIONAL3,
	INPUT_KEY_INTERNATIONAL4,
	INPUT_KEY_INTERNATIONAL5,
	INPUT_KEY_INTERNATIONAL6,
	INPUT_KEY_INTERNATIONAL7,
	INPUT_KEY_INTERNATIONAL8,
	INPUT_KEY_INTERNATIONAL9,
	INPUT_KEY_LANG1,
	INPUT_KEY_LANG2,
	INPUT_KEY_LANG3,
	INPUT_KEY_LANG4,
	INPUT_KEY_LANG5,
	INPUT_KEY_LANG6,
	INPUT_KEY_LANG7,
	INPUT_KEY_LANG8,
	INPUT_KEY_LANG9,
	INPUT_KEY_ALTERNATEERASE,
	INPUT_KEY_SYSREQORATTENTION,
	INPUT_KEY_CANCEL,
	INPUT_KEY_CLEAR,
	INPUT_KEY_PRIOR,
	INPUT_KEY_RETURN,
	INPUT_KEY_SEPARATOR,
	INPUT_KEY_OUT,
	INPUT_KEY_OPER,
	INPUT_KEY_CLEARORAGAIN,
	INPUT_KEY_CRSELORPROPS,
	INPUT_KEY_EXSEL,

	INPUT_KEY_LEFTCONTROL,
	INPUT_KEY_LEFTSHIFT,
	INPUT_KEY_LEFTALT,
	INPUT_KEY_LEFTGUI,
	INPUT_KEY_RIGHTCONTROL,
	INPUT_KEY_RIGHTSHIFT,
	INPUT_KEY_RIGHTALT,
	INPUT_KEY_RIGHTGUI,
};

struct input_device_t
{
	uint32_t usage;
	uint32_t is_trackpad : 1;
	uint32_t : 31;
	IOHIDDeviceRef device;
	input_t* context;
	union
	{
		input_keyboard_state_t* keyboard;
		input_mouse_state_t* mouse;
		input_gamepad_state_t* gamepad;
	} state;
};

#define INPUT_MAX_DEVICES 32

struct input_s
{
	allocator_t* allocator;

	IOHIDManagerRef hid_manager;
	uint32_t num_devices;
	input_device_t devices[INPUT_MAX_DEVICES];

	int32_t trackpad_keyboard;
	int32_t trackpad_mouse;

	uint32_t num_keyboards;
	uint32_t num_mice;
	uint32_t num_gamepads;
	input_keyboard_state_t keyboard_states[INPUT_MAX_KEYBOARDS];
	input_mouse_state_t mouse_states[INPUT_MAX_MICE];
	input_gamepad_state_t gamepad_states[INPUT_MAX_GAMEPADS];
};

static void enum_callback(void* in_context, IOReturn res, void* sender, IOHIDDeviceRef device)
{
	input_t* context = (input_t*)in_context;

	if(res == kIOReturnSuccess)
	{
		int parsed_usage = 0;

		CFArrayRef arr = (CFArrayRef)IOHIDDeviceGetProperty(device, CFSTR(kIOHIDDeviceUsagePairsKey));
		CFIndex index = CFArrayGetCount(arr);
		int i;
		for(i = 0; i < index; ++i)
		{
			CFDictionaryRef dict = (CFDictionaryRef)CFArrayGetValueAtIndex(arr, i);
			CFNumberRef upk = (CFNumberRef)CFDictionaryGetValue(dict, CFSTR(kIOHIDDeviceUsagePageKey));
			CFNumberRef uk = (CFNumberRef)CFDictionaryGetValue(dict, CFSTR(kIOHIDDeviceUsageKey));
			int upki = -1, uki = -1;
			CFNumberGetValue(upk, kCFNumberIntType, &upki);
			CFNumberGetValue(uk, kCFNumberIntType, &uki);
			if(upki == kHIDPage_GenericDesktop)
			{
				switch(uki)
				{
					case kHIDUsage_GD_Keyboard:
					case kHIDUsage_GD_Mouse:
					case kHIDUsage_GD_GamePad:
					case kHIDUsage_GD_Joystick:
						parsed_usage = uki;
				}
			}
		}


		if(parsed_usage)
		{
			CFComparisonResult cres = CFStringCompare((CFStringRef)IOHIDDeviceGetProperty(device, CFSTR(kIOHIDProductKey)), CFSTR("Apple Internal Keyboard / Trackpad"), 0);
			context->devices[context->num_devices].is_trackpad = cres == kCFCompareEqualTo;
			context->devices[context->num_devices].usage = parsed_usage;
			context->devices[context->num_devices].device = device;
			context->devices[context->num_devices].context = context;
			context->num_devices++;
		}
	}
}

typedef void (*input_callback_t)(void *in_context, IOReturn res, void *sender, IOHIDValueRef val);

static void keyboard_callback(void *in_context, IOReturn res, void *sender, IOHIDValueRef val)
{
	if(res == kIOReturnSuccess)
	{
		input_device_t* device = (input_device_t*)in_context;

		IOHIDElementRef elem = IOHIDValueGetElement(val);
		int16_t page = IOHIDElementGetUsagePage(elem);

		if(page == kHIDPage_KeyboardOrKeypad)
		{
			int16_t usage = translate_input[IOHIDElementGetUsage(elem)];
			if(usage != INPUT_KEY_UNKNOWN)
			{
				CFIndex value = IOHIDValueGetIntegerValue(val);
				device->state.keyboard->keys[usage] = value ? INPUT_PRESSED : INPUT_RELEASED;
			}
		}
	}
}

static void mouse_callback(void *in_context, IOReturn res, void *sender, IOHIDValueRef val)
{
	if(res == kIOReturnSuccess)
	{
		input_device_t* device = (input_device_t*)in_context;

		IOHIDElementRef elem = IOHIDValueGetElement(val);
		int16_t page = IOHIDElementGetUsagePage(elem);

		if(page == kHIDPage_GenericDesktop)
		{
			int16_t usage = IOHIDElementGetUsage(elem);
			CFIndex value = IOHIDValueGetIntegerValue(val);
			if(usage == kHIDUsage_GD_X)
			{
				device->state.mouse->x = value;
			}
			else if(usage == kHIDUsage_GD_Y)
			{
				device->state.mouse->y = value;
			}
			else if(usage == kHIDUsage_GD_Wheel)
			{
				// TODO
			}
			else
			{
				ASSERT(0, "unsupported usage");
			}
		}
		else if(page == kHIDPage_Button)
		{
			int16_t button = IOHIDElementGetUsage(elem) - 1;
			if(button >= 0 && button < INPUT_MAX_MOUSE_BUTTONS)
			{
				CFIndex value = IOHIDValueGetIntegerValue(val);
				device->state.mouse->button[button] = value ? INPUT_PRESSED : INPUT_RELEASED;
			}
		}
		else
		{
			ASSERT(0, "unsupported page");
		}
	}
}

static void gamepad_callback(void *in_context, IOReturn res, void *sender, IOHIDValueRef val)
{
	if(res == kIOReturnSuccess)
	{
		input_device_t* device = (input_device_t*)in_context;

		IOHIDElementRef elem = IOHIDValueGetElement(val);
		int16_t page = IOHIDElementGetUsagePage(elem);
		if(page == kHIDPage_GenericDesktop)
		{
			int16_t usage = IOHIDElementGetUsage(elem);
			if(usage == kHIDUsage_GD_Pointer)
			{
				if(device->state.gamepad == NULL)
				{
					input_t* context = device->context;
					device->state.gamepad = &context->gamepad_states[context->num_gamepads++];
				}
			}
			else if(device->state.gamepad)
			{
				int index = -1;
				switch(usage)
				{
					case kHIDUsage_GD_X:
						index = 0;
						break;
					case kHIDUsage_GD_Y:
						index = 1;
						break;
					case kHIDUsage_GD_Z:
						index = 2;
						break;
					case kHIDUsage_GD_Rx:
						index = 3;
						break;
					case kHIDUsage_GD_Ry:
						index = 4;
						break;
					case kHIDUsage_GD_Rz:
						index = 5;
						break;
				}
				if(index >= 0 && index < INPUT_MAX_GAMEPAD_AXES)
				{
					CFIndex value = IOHIDValueGetIntegerValue(val);
					device->state.gamepad->axis[index] = value;
				}
			}
		}
		else if(page == kHIDPage_Button)
		{
			int16_t button = IOHIDElementGetUsage(elem) - 1;
			if(button >= 0 && button < INPUT_MAX_GAMEPAD_BUTTONS)
			{
				CFIndex value = IOHIDValueGetIntegerValue(val);
				device->state.gamepad->button[button] = value ? INPUT_PRESSED : INPUT_RELEASED;
			}
		}
		else
		{
			ASSERT(0, "unsupported page");
		}
	}
}

static void unplugged_callback(void *ctx, IOReturn res, void *sender)
{
	ASSERT(0, "We cannot handle hotplug yet");
}

input_t* input_create(allocator_t* allocator)
{
	input_t* context = ALLOCATOR_ALLOC_TYPE(allocator, input_t);
	memory_zero(context, sizeof(input_t));
	context->trackpad_keyboard = -1;
	context->trackpad_mouse = -1;
	context->allocator = allocator;

	context->hid_manager = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
	ASSERT(CFGetTypeID(context->hid_manager) == IOHIDManagerGetTypeID(), "Could not create HIDManager");

	{
		int i;
		const uint32_t usages[] = {
			kHIDUsage_GD_Keyboard,
			kHIDUsage_GD_Mouse,
			kHIDUsage_GD_GamePad,
			kHIDUsage_GD_Joystick,
		};
		const uint32_t page = kHIDPage_GenericDesktop;

		CFMutableArrayRef matching_arr = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
		for(i = 0; i < ARRAY_LENGTH(usages); ++i)
		{
			CFMutableDictionaryRef matching_dict = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
			CFNumberRef page_number = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &page);
			CFDictionarySetValue(matching_dict, CFSTR(kIOHIDDeviceUsagePageKey), page_number);
			CFRelease(page_number);
			CFNumberRef usage_number = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &usages[i]);
			CFDictionarySetValue(matching_dict, CFSTR(kIOHIDDeviceUsageKey), usage_number);
			CFRelease(usage_number);
			CFArrayAppendValue(matching_arr, matching_dict);
			CFRelease(matching_dict);
		}

		CFRunLoopRef runloop = CFRunLoopGetCurrent();
		IOHIDManagerRegisterDeviceMatchingCallback(context->hid_manager, enum_callback, context);
		IOHIDManagerScheduleWithRunLoop(context->hid_manager, runloop, CFSTR("input_update"));
		IOHIDManagerSetDeviceMatchingMultiple(context->hid_manager, matching_arr);
		IOHIDManagerOpen(context->hid_manager, kIOHIDOptionsTypeNone);

		input_update(context);

		IOHIDManagerRegisterDeviceMatchingCallback(context->hid_manager, NULL, NULL);
		IOHIDManagerUnscheduleFromRunLoop(context->hid_manager, runloop, CFSTR("input_update"));
		CFRelease(matching_arr);

		for(i = 0; i < context->num_devices; ++i)
		{
			IOHIDDeviceRef dev = context->devices[i].device;
			if(IOHIDDeviceOpen(dev, kIOHIDOptionsTypeNone) == kIOReturnSuccess)
			{
				input_callback_t callback = NULL;
				switch(context->devices[i].usage)
				{
					case kHIDUsage_GD_Keyboard:
						if(context->devices[i].is_trackpad)
						{
							if(context->trackpad_keyboard == -1)
							{
								context->trackpad_keyboard = context->num_keyboards++;
							}
							context->devices[i].state.keyboard = &context->keyboard_states[context->trackpad_keyboard];
						}
						else
							context->devices[i].state.keyboard = &context->keyboard_states[context->num_keyboards++];
						callback = keyboard_callback;
						break;
					case kHIDUsage_GD_Mouse:
						if(context->devices[i].is_trackpad)
						{
							if(context->trackpad_mouse == -1)
							{
								context->trackpad_mouse = context->num_mice++;
							}
							context->devices[i].state.mouse = &context->mouse_states[context->trackpad_mouse];
						}
						else
							context->devices[i].state.mouse = &context->mouse_states[context->num_mice++];
						callback = mouse_callback;
						break;
					case kHIDUsage_GD_GamePad:
					case kHIDUsage_GD_Joystick:
						context->devices[i].state.gamepad = NULL; // we set this up later
						callback = gamepad_callback;
						break;
				}
				IOHIDDeviceRegisterRemovalCallback(dev, unplugged_callback, &context->devices[i]);
				IOHIDDeviceRegisterInputValueCallback(dev, callback, &context->devices[i]);
				IOHIDDeviceScheduleWithRunLoop(dev, runloop, CFSTR("input_update"));
			}
		}
	}

	return context;
}

void input_destroy(input_t* context)
{
	allocator_t* allocator = context->allocator;
	IOHIDManagerClose(context->hid_manager, kIOHIDOptionsTypeNone);
	CFRelease(context->hid_manager);

	ALLOCATOR_FREE(allocator, context);
}

void input_update(input_t* context)
{
	while(CFRunLoopRunInMode(CFSTR("input_update"), 0, TRUE) == kCFRunLoopRunHandledSource);
}

void input_keyboard_state(input_t* context, uint32_t index, input_keyboard_state_t* keyboard)
{
	memory_copy(keyboard, &context->keyboard_states[index], sizeof(input_keyboard_state_t));
}

void input_mouse_state(input_t* context, uint32_t index, input_mouse_state_t* mouse)
{
	memory_copy(mouse, &context->mouse_states[index], sizeof(input_mouse_state_t));
}

void input_gamepad_state(input_t* context, uint32_t index, input_gamepad_state_t* gamepad)
{
	memory_copy(gamepad, &context->gamepad_states[index], sizeof(input_gamepad_state_t));
}
