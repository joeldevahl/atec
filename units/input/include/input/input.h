#ifndef INPUT_INPUT_H
#define INPUT_INPUT_H

#include <core/types.h>
#include <input/keys.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	INPUT_RELEASED = 0,
	INPUT_PRESSED = 1,
} input_action_t;

typedef enum
{
	INPUT_MAX_KEYBOARDS = 8,
	INPUT_MAX_MICE = 8,
	INPUT_MAX_GAMEPADS = 8,

	INPUT_MAX_MOUSE_BUTTONS = 32,

	INPUT_MAX_GAMEPAD_AXES = 8,
	INPUT_MAX_GAMEPAD_BUTTONS = 32
} input_caps_t;

typedef struct input_keyboard_state_s
{
	input_action_t keys[INPUT_KEY_NUM_KEYS];
} input_keyboard_state_t;

typedef struct input_mouse_state_s
{
	uint8_t button[INPUT_MAX_MOUSE_BUTTONS];
	float x;
	float y;
	float wheel;
} input_mouse_state_t;

typedef struct input_gamepad_state_s
{
	int16_t axis[INPUT_MAX_GAMEPAD_AXES];
	uint8_t button[INPUT_MAX_GAMEPAD_BUTTONS];
} input_gamepad_state_t;

typedef struct input_s input_t;

typedef struct input_create_params_s
{
	struct allocator_s* allocator;
	void* window;
} input_create_params_t;

input_t* input_create(const input_create_params_t* params);
void input_destroy(input_t* input);

void input_update(input_t* input);

void input_keyboard_state(input_t* input, uint32_t index, input_keyboard_state_t* keyboard);
void input_mouse_state(input_t* input, uint32_t index, input_mouse_state_t* mouse);
void input_gamepad_state(input_t* input, uint32_t index, input_gamepad_state_t* gamepad);

#ifdef __cplusplus
}
#endif

#endif //CORE_INPUT_H
