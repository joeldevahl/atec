#ifndef SEXP_SEXP_H
#define SEXP_SEXP_H

#include <core/types.h>
#include <memory/allocator.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	SEXP_TYPE_FIXNUM,
	SEXP_TYPE_BOOLEAN,
	SEXP_TYPE_CHARACTER,
	SEXP_TYPE_STRING,
	SEXP_TYPE_CONS,
	SEXP_TYPE_NIL,
	SEXP_TYPE_SYMBOL,
	SEXP_TYPE_QUOTE,
} sexp_type_t;

typedef struct sexp_s
{
	sexp_type_t type;

	union
	{
		int32_t fixnum;
		int32_t boolean;
		int32_t character;
		char* string;
		struct
		{
			sexp_s* car;
			sexp_s* cdr;
		} cons;
		char* symbol;
		sexp_s* quote;
	};
} sexp_t;

sexp_t* sexp_read(allocator_t* alloc, const char* data);
void sexp_write(sexp_t* exp);

int sexp_is_nil(sexp_t* exp);
int sexp_is_fixnum(sexp_t* exp);
int sexp_is_boolean(sexp_t* exp);
int sexp_is_character(sexp_t* exp);
int sexp_is_string(sexp_t* exp);
int sexp_is_cons(sexp_t* exp);
sexp_t* sexp_car(sexp_t* exp);
sexp_t* sexp_cdr(sexp_t* exp);
void sexp_set_car(sexp_t* exp, sexp_t* val);
void sexp_set_cdr(sexp_t* exp, sexp_t* val);
int sexp_is_symbol(sexp_t* exp);
int sexp_is_quote(sexp_t* exp);

#ifdef __cplusplus
}
#endif

#endif // SCRIPT_SEXP_H
