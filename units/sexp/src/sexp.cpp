#include <core/assert.h>

#include "sexp_internal.h"

int sexp_is_nil(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_NIL;
}

int sexp_is_fixnum(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_FIXNUM;
}

int sexp_is_boolean(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_BOOLEAN;
}

int sexp_is_character(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_CHARACTER;
}

int sexp_is_string(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_STRING;
}

int sexp_is_cons(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_CONS;
}

sexp_t* sexp_car(sexp_t* exp)
{
	ASSERT(sexp_is_cons(exp), "taking car of non cons value");
	return exp->cons.car;
}

sexp_t* sexp_cdr(sexp_t* exp)
{
	ASSERT(sexp_is_cons(exp), "taking cdr of non cons value");
	return exp->cons.cdr;
}

void sexp_set_car(sexp_t* exp, sexp_t* val)
{
	ASSERT(sexp_is_cons(exp), "setting car of non cons value");
	exp->cons.car = val;
}

void sexp_set_cdr(sexp_t* exp, sexp_t* val)
{
	ASSERT(sexp_is_cons(exp), "setting cdr of non cons value");
	exp->cons.cdr = val;
}

int sexp_is_symbol(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_SYMBOL;
}

int sexp_is_quote(sexp_t* exp)
{
	return exp->type == SEXP_TYPE_QUOTE;
}
