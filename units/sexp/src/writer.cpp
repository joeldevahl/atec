#include <stdio.h>
#include <stdlib.h>

#include "sexp_internal.h"

static void sexp_write_list(sexp_t* exp)
{
	sexp_write(sexp_car(exp));
	if(!sexp_is_nil(sexp_cdr(exp)))
	{
		if(sexp_is_cons(sexp_cdr(exp)))
		{
			printf(" ");
			sexp_write_list(sexp_cdr(exp));
		}
		else
		{
			printf(" . ");
			sexp_write(sexp_cdr(exp));
		}
	}
}

void sexp_write(sexp_t* exp)
{
	if(!exp)
	{
		printf("()");
		return;
	}

	switch(exp->type)
	{
	case SEXP_TYPE_BOOLEAN:
		printf("#%c", exp->boolean ? 't' : 'f');
		break;
	case SEXP_TYPE_FIXNUM:
		printf("%d", exp->fixnum);
		break;
	case SEXP_TYPE_CHARACTER:
		printf("#\\%c", exp->character);
		break;
	case SEXP_TYPE_STRING:
		printf("\"%s\"", exp->string);
		break;
	case SEXP_TYPE_CONS:
		printf("(");
		sexp_write_list(exp);
		printf(")");
		break;
	case SEXP_TYPE_SYMBOL:
		printf("%s", exp->symbol);
		break;
	case SEXP_TYPE_NIL:
		printf("()");
		break;
	case SEXP_TYPE_QUOTE:
		printf("\'");
		sexp_write(exp->quote);
		break;
	default:
		printf("\nwrite: unknown type %d\n", (int)exp->type);
		exit(1);
	}
}
