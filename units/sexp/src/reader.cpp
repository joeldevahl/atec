#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <memory/allocator.h>
#include <memory/memory.h>

#include "sexp_internal.h"

struct sexp_read_ctx
{
	const char* start;
	const char* curr;
};

static sexp_t* sexp_alloc_sexp(allocator_t* alloc)
{
	return (sexp_t*)ALLOCATOR_ALLOC(alloc, sizeof(sexp_t), 16);
}

static sexp_t* sexp_make_fixnum(allocator_t* alloc, int32_t num)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_FIXNUM;
	exp->fixnum = num;
	return exp;
}

static sexp_t* sexp_make_boolean(allocator_t* alloc, int v)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_BOOLEAN;
	exp->boolean = v;
	return exp;
}

static sexp_t* sexp_make_character(allocator_t* alloc, char c)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_CHARACTER;
	exp->character = c;
	return exp;
}

static sexp_t* sexp_make_string(allocator_t* alloc, char* str, size_t len)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_STRING;
	exp->string = (char*)ALLOCATOR_ALLOC(alloc, sizeof(char)*(len+1), 1);
	memory_copy(exp->string, str, len);
	exp->string[len] = '\0';
	return exp;
}

static sexp_t* sexp_make_cons(allocator_t* alloc, sexp_t* car, sexp_t* cdr)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_CONS;
	exp->cons.car = car;
	exp->cons.cdr = cdr;
	return exp;
}

static sexp_t* sexp_make_nil(allocator_t* alloc)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_NIL;
	return exp;
}

static sexp_t* sexp_make_symbol(allocator_t* alloc, char* str, size_t len)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_SYMBOL;
	exp->symbol = (char*)ALLOCATOR_ALLOC(alloc, sizeof(char)*(len+1), 1);
	memory_copy(exp->symbol, str, len);
	exp->symbol[len] = '\0';
	return exp;
}

static sexp_t* sexp_make_quote(allocator_t* alloc, sexp_t* val)
{
	sexp_t* exp = sexp_alloc_sexp(alloc);
	exp->type = SEXP_TYPE_QUOTE;
	exp->quote = val;
	return exp;
}

static int sexp_getc(sexp_read_ctx* ctx)
{
	return *ctx->curr++;
}

static void sexp_ungetc(sexp_read_ctx* ctx)
{
	ctx->curr--;
}

static int sexp_peek(sexp_read_ctx* ctx)
{
	return ctx->curr[1];
}

static int sexp_isdelimiter(int c)
{
	return isspace(c) || c == EOF || c == '(' || c == ')' || c == '"' || c == ';';
}

static void sexp_skip_whitespace(sexp_read_ctx* ctx)
{
	int c;
	while((c = sexp_getc(ctx)) != EOF)
	{
		if(!isspace(c))
		{
			if(c == ';')
			{
				while((c = sexp_getc(ctx)) != '\0' && (c != '\n'));
			}
			else
			{
				sexp_ungetc(ctx);
				break;
			}
		}
	}
}

static sexp_t* sexp_read_internal(allocator_t* alloc, sexp_read_ctx* ctx);

static sexp_t* sexp_read_list(allocator_t* alloc, sexp_read_ctx* ctx)
{
	int c;
	sexp_t* car;
	sexp_t* cdr;
	sexp_skip_whitespace(ctx);
	c = sexp_getc(ctx);
	if(c == ')')
		return sexp_make_nil(alloc);

	sexp_ungetc(ctx);
	car = sexp_read_internal(alloc, ctx);
	sexp_skip_whitespace(ctx);
	c = sexp_getc(ctx);
	if(c == '.')
	{
		if(!sexp_isdelimiter(sexp_peek(ctx)))
		{
			printf("readList: bad pair\n");
			exit(1);
		}

		sexp_skip_whitespace(ctx);
		cdr = sexp_read_internal(alloc, ctx);

		c = sexp_getc(ctx);
		if(c != ')')
		{
			printf("readList: pair not terminated\n");
			exit(1);
		}

		return sexp_make_cons(alloc, car, cdr);
	}
	else
	{
		sexp_ungetc(ctx);
		cdr = sexp_read_list(alloc, ctx);
		return sexp_make_cons(alloc, car, cdr);
	}
}

#define BUFFER_SIZE 1024
static sexp_t* sexp_read_symbol(allocator_t* alloc, sexp_read_ctx* ctx)
{
	size_t i = 0;
	int c;
	char buffer[BUFFER_SIZE];

	while(!sexp_isdelimiter(c = sexp_getc(ctx)))
	{
		if(i < BUFFER_SIZE)
			buffer[i++] = c;
		else
		{
			printf("read: symbol too long to parse\n");
			exit(1);
		}
	}
	sexp_ungetc(ctx);
	return sexp_make_symbol(alloc, buffer, i);
}

static sexp_t* sexp_read_internal(allocator_t* alloc, sexp_read_ctx* ctx)
{
	int c;

	sexp_skip_whitespace(ctx);

	c = sexp_getc(ctx);
	if(isdigit(c) || (c == '-' && isdigit(sexp_peek(ctx))))
	{
		int32_t sign = 1;
		int32_t num = 0;

		if(c == '-')
			sign = -1;
		else
			sexp_ungetc(ctx);

		while(isdigit(c = sexp_getc(ctx)))
		{
			num = num*10 + (c-'0');
		}
		num *= sign;

		if(sexp_isdelimiter(c))
		{
			sexp_ungetc(ctx);
			return sexp_make_fixnum(alloc, num);
		}
		else
		{
			printf("read: bad number\n");
			exit(1);
		}
	}
	else if(c == '#')
	{
		c = sexp_getc(ctx);
		switch(c)
		{
		case 't':
			return sexp_make_boolean(alloc, 1);
		case 'f':
			return sexp_make_boolean(alloc, 0);
		case '\\':
			// TODO: read more than just simple chars
			c = sexp_getc(ctx);
			return sexp_make_character(alloc, c);
		default:
			printf("read: bad boolean or character\n");
			exit(1);
		}
	}
	else if(c == '"')
	{
		size_t i = 0;

		char buffer[BUFFER_SIZE];
		while((c = sexp_getc(ctx)) != '"')
		{
			// TODO: read escaped stuff
			if(c == '\0')
			{
				printf("read: bad string\n");
				exit(1);
			}

			if(i < BUFFER_SIZE)
				buffer[i++] = c;
			else
			{
				printf("read: string too long to parse\n");
				exit(1);
			}
		}
		return sexp_make_string(alloc, buffer, i);
	}
	else if(c == '(')
	{
		return sexp_read_list(alloc, ctx);
	}
	else if(c == '\'' || c == ':')
	{
		sexp_t* v = 0x0;
		c = sexp_getc(ctx);
		if(c == '(')
		{
			v = sexp_read_list(alloc, ctx);
		}
		else
		{
			sexp_ungetc(ctx);
			v = sexp_read_symbol(alloc, ctx);
		}
		return sexp_make_quote(alloc, v);
	}
	else
	{
		sexp_ungetc(ctx);
		return sexp_read_symbol(alloc, ctx);
	}
}

sexp_t* sexp_read(allocator_t* alloc, const char* data)
{
	sexp_read_ctx ctx;
	ctx.start = data;
	ctx.curr = data;

	return sexp_read_internal(alloc, &ctx);
}
