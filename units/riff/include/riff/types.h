#ifndef RIFF_TYPES_H
#define RIFF_TYPES_H

#include <core/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct riff_header_s
{
	uint8_t id[4];
	uint32_t size;
} riff_header_t;

FORCE_INLINE bool riff_test_id(const riff_header_t* header, uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3)
{
	return header->id[0] == b0 &&
		   header->id[1] == b1 &&
		   header->id[2] == b2 &&
		   header->id[3] == b3;
}

#ifdef __cplusplus
}
#endif

#endif // RIFF_TYPES_H
