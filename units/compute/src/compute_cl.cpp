#if 0

#if defined(PLATFORM_OSX)
#	include <OpenCL/cl.h>
#	include <OpenCL/cl_gl.h>
#	include <OpenCL/cl_gl_ext.h>
#	include <OpenGL/gl.h>
#	include <OpenGL/OpenGL.h>
#else
#	if defined(FAMILY_WINDOWS)
#		define WIN32_LEAN_AND_MEAN 
#		define _WIN32_WINNT 0x0600
#		include <windows.h>
#	endif
#	include <CL/cl.h>
#	include <CL/cl_gl.h>
#	include <CL/cl_gl_ext.h>
#	include <GL/gl.h>
#endif

#include <cstdio>

#include <core/assert.h>
#include <core/allocator.h>
#include <compute/compute.h>

#define MAX_PLATFORMS 32
#define MAX_DEVICES 32

struct compute_device
{
	cl_platform_id platform_id;
	cl_device_id device_id;
	cl_context context;
	cl_command_queue queue;
	allocator_t allocator;
};

struct compute_kernel
{
	cl_program program;
	cl_kernel kernel;
	//size_t work_group_size;
};

struct compute_device* compute_create(const struct compute_create_params* params)
{
	cl_platform_id platforms[MAX_PLATFORMS];
	size_t num_platforms = 0;

	cl_device_id devices[MAX_DEVICES];
	size_t num_devices = 0;

	cl_int res;
#if !defined(PLATFORM_MACOSX)
	cl_context_properties properties[] = { 0 };
#endif

	struct compute_device* device = (struct compute_device*)ALLOC(params->allocator, sizeof(struct compute_device), 16);
	device->allocator = params->allocator;

#if defined(PLATFORM_MACOSX)
	CGLContextObj context = CGLGetCurrentContext();              
	CGLShareGroupObj share_group = CGLGetShareGroup(context);
	
	cl_context_properties properties[] = { 
		CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE, 
		(cl_context_properties)share_group, 0 
	};
#endif

	STATIC_ASSERT(sizeof(cl_mem) <= sizeof(void*));
	STATIC_ASSERT(sizeof(cl_event) <= sizeof(void*));

	res = clGetPlatformIDs(ARRAY_LENGTH(platforms), platforms, &num_platforms);
	ASSERT(res == CL_SUCCESS, "CL error");
	ASSERT(num_platforms > 0, "CL error");

	res = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, ARRAY_LENGTH(devices), devices, &num_devices);
	ASSERT(res == CL_SUCCESS, "CL error");
	ASSERT(num_devices > 0, "CL error");

	device->platform_id = platforms[0];
	device->device_id = devices[0];

	device->context = clCreateContext(properties, 1, &device->device_id, NULL, NULL, &res);
	ASSERT(res == CL_SUCCESS, "CL error");

	device->queue = clCreateCommandQueue(device->context, device->device_id, 0, &res);	
	ASSERT(res == CL_SUCCESS, "CL error");

	return device;
};

void compute_destroy(struct compute_device* device)
{
	allocator_t allocator = device->allocator;

	clReleaseCommandQueue(device->queue);
	clReleaseContext(device->context);

	FREE(allocator, device);
}

struct compute_kernel* compute_create_kernel(struct compute_device* device, const char* src){
	cl_int res;
	struct compute_kernel* kernel = (struct compute_kernel*)ALLOC(device->allocator, sizeof(struct compute_kernel), 16);

	kernel->program = clCreateProgramWithSource(device->context, 1, &src, NULL, &res);
	res = clBuildProgram(kernel->program, 0, NULL, NULL, NULL, NULL);
	if(res != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];
		clGetProgramBuildInfo(kernel->program, device->device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer),	buffer,	&len);
		printf("%s\n", buffer);
	}
	ASSERT(res == CL_SUCCESS, "CL error");

	kernel->kernel = clCreateKernel(kernel->program, "main", &res);
	ASSERT(res == CL_SUCCESS, "CL error");

	//res = clGetKernelWorkGroupInfo(kernel->kernel, device->device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &kernel->work_group_size, NULL);
	//ASSERT(res == CL_SUCCESS, "CL error");

	return kernel;
}

void compute_destroy_kernel(struct compute_device* device, struct compute_kernel* kernel)
{
	clReleaseProgram(kernel->program);
	clReleaseKernel(kernel->kernel);
	FREE(device->allocator, kernel);
}

void compute_set_kernel_arg(struct compute_kernel* kernel, int index, size_t size, const void* data)
{
	cl_int res;
	res = clSetKernelArg(kernel->kernel, index, size, data);
	ASSERT(res == CL_SUCCESS, "CL error");
}

void compute_set_kernel_buffer(struct compute_kernel* kernel, int index, struct compute_buffer* buffer)
{
	cl_int res;
	cl_mem mem = (cl_mem)buffer;
	res = clSetKernelArg(kernel->kernel, index, sizeof(cl_mem), &mem);
	ASSERT(res == CL_SUCCESS, "CL error");
}

struct compute_buffer* compute_create_buffer(struct compute_device* device, enum compute_access access, size_t size)
{
	static const int access_translate[] = {
		CL_MEM_READ_ONLY,
		CL_MEM_WRITE_ONLY,
	};

	cl_int res;
	cl_mem mem = clCreateBuffer(device->context, access_translate[access], size, NULL, &res);
	ASSERT(res == CL_SUCCESS, "CL error");

	return (struct compute_buffer*)mem;
}

void compute_destroy_buffer(struct compute_device* device, struct compute_buffer* buffer)
{
	cl_mem mem = (cl_mem)buffer;
	clReleaseMemObject(mem);
}

GLuint renderapi_texture_get_gl_id(struct renderapi_texture* texture);

struct compute_image* compute_create_shared_image(struct compute_device* device, struct renderapi_texture* texture)
{
	cl_int res;
	cl_mem mem = clCreateFromGLTexture2D(device->context, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, renderapi_texture_get_gl_id(texture), &res); 
	ASSERT(res == CL_SUCCESS, "CL error");

	return (struct compute_image*)mem;
}

void compute_destroy_image(struct compute_device* device, struct compute_image* image)
{
	cl_mem mem = (cl_mem)image;
	clReleaseMemObject(mem);
}

void compute_enqueue_write(struct compute_device* device, struct compute_buffer* buffer, size_t offset, size_t size, const void* data)
{
	cl_int res;
	res = clEnqueueWriteBuffer(device->queue, (cl_mem)buffer, CL_TRUE, offset, size, data, 0, NULL, NULL);
	ASSERT(res == CL_SUCCESS, "CL error");
}

void compute_enqueue_read(struct compute_device* device, struct compute_buffer* buffer, size_t offset, size_t size, void* data)
{
	cl_int res;
	res = clEnqueueReadBuffer(device->queue, (cl_mem)buffer, CL_TRUE, offset, size, data, 0, NULL, NULL);
	ASSERT(res == CL_SUCCESS, "CL error");
}

void compute_enqueue_task(struct compute_device* device, struct compute_kernel* kernel, size_t work_size)
{
	cl_int res;
	//res = clEnqueueNDRangeKernel(device->queue, kernel->kernel, 1, NULL, &work_size, &kernel->work_group_size, 0, NULL, NULL);
	res = clEnqueueTask(device->queue, kernel->kernel, 0, NULL, NULL);
	ASSERT(res == CL_SUCCESS, "CL error");
}

void compute_finish(struct compute_device* device)
{
	clFinish(device->queue);
}

#endif //#ifdef ARCH_PPC_32
