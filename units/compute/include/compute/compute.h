#ifndef COMPUTE_COMPUTE_H
#define COMPUTE_COMPUTE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct struct compute_event
{
	allocator_t* allocator;
} compute_create_params_t;

enum compute_access
{
	COMPUTE_ACCESS_READ = 0,
	COMPUTE_ACCESS_WRITE,
};

typedef struct compute_device_s compute_device_t;
typedef struct compute_kernel_s compute_kernel_t;
typedef struct compute_buffer_s compute_buffer_t;
typedef struct compute_image_s compute_image_t;
typedef struct compute_event_s compute_event_t;

compute_device_t* compute_create(const struct compute_create_params* params);
void compute_destroy(compute_device_t* device);

compute_kernel_t* compute_create_kernel(compute_device_t* device, const char* src);
void compute_destroy_kernel(compute_device_t* device, compute_kernel_t* kernel);

void compute_set_kernel_arg(compute_kernel_t* kernel, int index, size_t size, const void* data);
void compute_set_kernel_buffer(compute_kernel_t* kernel, int index, compute_buffer_t* buffer);
void compute_set_kernel_image(compute_kernel_t* kernel, int index, compute_image_t* image);

compute_buffer_t* compute_create_buffer(compute_device_t* device, enum compute_access access, size_t size);
void compute_destroy_buffer(compute_device_t* device, compute_buffer_t* buffer);

compute_image_t* compute_create_2d_image(compute_device_t* device, struct renderapi_texture_t* texture);
compute_image_t* compute_create_3d_image(compute_device_t* device, struct renderapi_texture* texture);
compute_image_t* compute_create_shared_image(compute_device_t* device, struct renderapi_texture_t* texture);
void compute_destroy_image(compute_device_t* device, compute_image_t* image);

void compute_enqueue_write_buffer(compute_device_t* device, compute_buffer_t* buffer, size_t offset, size_t size, const void* data);
void compute_enqueue_read_buffer(compute_device_t* device, compute_buffer_t* buffer, size_t offset, size_t size, void* data);
void compute_enqueue_write_image(compute_device_t* device, compute_image_t* image, size_t offset, size_t size, const void* data);
void compute_enqueue_read_image(compute_device_t* device, compute_image_t* image, size_t offset, size_t size, void* data);
compute_event_t* compute_enqueue_map_image(compute_device_t* device, compute_image_t* image, size_t offset, size_t size, void* data);
void compute_enqueue_task(compute_device_t* device, compute_kernel_t* kernel, size_t work_size);
compute_event_t* compute_enqueue_barrier(compute_device_t* device);
void compute_enqueue_wait(compute_device_t* device, size_t num_events, const compute_event_t* events);

void compute_finish(compute_device_t* device);

#ifdef __cplusplus
}
#endif

#endif // COMPUTE_COMPUTE_H
