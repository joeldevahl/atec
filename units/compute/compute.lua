Unit:Using("core")
Unit:Using("render")

function Unit.Patch(self, other_unit)
	self:DefaultPatch(other_unit)
	if target.platform == "macosx" and target.arch ~= "ppc" then
		other_unit.settings.link.frameworks:Add("OpenCL")
	elseif target.family == "windows" then
		other_unit.settings.cc.includes:Add("C:\\Program Files (x86)\\Intel\\OpenCL SDK\\2.0\\include")
		if target.bits == 64 then
			other_unit.settings.link.libpath:Add("C:\\Program Files (x86)\\Intel\\OpenCL SDK\\2.0\\lib\\x64")
		else
			other_unit.settings.link.libpath:Add("C:\\Program Files (x86)\\Intel\\OpenCL SDK\\2.0\\lib\\x86")
		end
		other_unit.settings.link.libs:Add("OpenCL")
	end
end
