struct VP_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD0;
};

struct FP_OUTPUT
{
	float4 color : SV_TARGET;
};

SamplerState TextureSampler
{
    Filter = LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

Texture2D source : register(t0);

void main(out FP_OUTPUT output, in VP_OUTPUT input)
{
	output.color = source.Sample(TextureSampler, input.texcoord);
}