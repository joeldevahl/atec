#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <memory/memory.h>
#include <io/file.h>
#include <getopt/getopt.h>
#include <vgpu.h>

#include <cstring>
#include <cstdio>

#if defined(FAMILY_WINDOWS)
#	include "SPIRV/GlslangToSpv.h"
#	include <core/windows/windows.h>
#	include <d3dcompiler.h>
#endif

static const char program_defines_gl[] = {
	#include <program_defines_gl.h.hex>
};

#if defined(FAMILY_WINDOWS)
static const char program_defines_dx11[] = {
	#include <program_defines_dx11.h.hex>
};
#endif

#define ERROR_AND_QUIT(fmt, ...) { fprintf(stderr, "Error: " fmt "\n", ##__VA_ARGS__); return 0x0; }
#define ERROR_AND_FAIL(fmt, ...) { fprintf(stderr, "Error: " fmt "\n", ##__VA_ARGS__); return 1; }

int compile_program_null(const char* outfilename, const char* infilename, vgpu_program_type_t type)
{
	char* shader_src = file_open_read_all_text(infilename, &allocator_default);
	const size_t shader_src_size = strlen(shader_src) + 1;

	// NULL backend, just copy the shader

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	file_write(outfile, shader_src, shader_src_size);
	file_close(outfile);

	ALLOCATOR_FREE(&allocator_default, shader_src);

	return 0;
}

int compile_program_gl(const char* outfilename, const char* infilename, vgpu_program_type_t type)
{
	char* shader_src = file_open_read_all_text(infilename, &allocator_default);
	const size_t shader_src_size = strlen(shader_src) + 1;

	// GL shader, concatenate with header and return text data
	size_t size_gl = sizeof(program_defines_gl) + shader_src_size;
	char* prog_data_gl = ALLOCATOR_ALLOC_ARRAY(&allocator_default, size_gl, char);
	memcpy(prog_data_gl, program_defines_gl, sizeof(program_defines_gl));
	// TODO: add a line 0 preprocessor command
	memcpy(prog_data_gl + sizeof(program_defines_gl), shader_src, shader_src_size);

	ALLOCATOR_FREE(&allocator_default, shader_src);

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	file_write(outfile, prog_data_gl, size_gl);
	file_close(outfile);

	ALLOCATOR_FREE(&allocator_default, prog_data_gl);

	return 0;
}

#if defined(FAMILY_WINDOWS)
static const EShLanguage translate_vk_shader_type[] =
{
	EShLangVertex,
	EShLangFragment,
};

static void init_vk_resources(TBuiltInResource &Resources) {
	Resources.maxLights = 32;
	Resources.maxClipPlanes = 6;
	Resources.maxTextureUnits = 32;
	Resources.maxTextureCoords = 32;
	Resources.maxVertexAttribs = 64;
	Resources.maxVertexUniformComponents = 4096;
	Resources.maxVaryingFloats = 64;
	Resources.maxVertexTextureImageUnits = 32;
	Resources.maxCombinedTextureImageUnits = 80;
	Resources.maxTextureImageUnits = 32;
	Resources.maxFragmentUniformComponents = 4096;
	Resources.maxDrawBuffers = 32;
	Resources.maxVertexUniformVectors = 128;
	Resources.maxVaryingVectors = 8;
	Resources.maxFragmentUniformVectors = 16;
	Resources.maxVertexOutputVectors = 16;
	Resources.maxFragmentInputVectors = 15;
	Resources.minProgramTexelOffset = -8;
	Resources.maxProgramTexelOffset = 7;
	Resources.maxClipDistances = 8;
	Resources.maxComputeWorkGroupCountX = 65535;
	Resources.maxComputeWorkGroupCountY = 65535;
	Resources.maxComputeWorkGroupCountZ = 65535;
	Resources.maxComputeWorkGroupSizeX = 1024;
	Resources.maxComputeWorkGroupSizeY = 1024;
	Resources.maxComputeWorkGroupSizeZ = 64;
	Resources.maxComputeUniformComponents = 1024;
	Resources.maxComputeTextureImageUnits = 16;
	Resources.maxComputeImageUniforms = 8;
	Resources.maxComputeAtomicCounters = 8;
	Resources.maxComputeAtomicCounterBuffers = 1;
	Resources.maxVaryingComponents = 60;
	Resources.maxVertexOutputComponents = 64;
	Resources.maxGeometryInputComponents = 64;
	Resources.maxGeometryOutputComponents = 128;
	Resources.maxFragmentInputComponents = 128;
	Resources.maxImageUnits = 8;
	Resources.maxCombinedImageUnitsAndFragmentOutputs = 8;
	Resources.maxCombinedShaderOutputResources = 8;
	Resources.maxImageSamples = 0;
	Resources.maxVertexImageUniforms = 0;
	Resources.maxTessControlImageUniforms = 0;
	Resources.maxTessEvaluationImageUniforms = 0;
	Resources.maxGeometryImageUniforms = 0;
	Resources.maxFragmentImageUniforms = 8;
	Resources.maxCombinedImageUniforms = 8;
	Resources.maxGeometryTextureImageUnits = 16;
	Resources.maxGeometryOutputVertices = 256;
	Resources.maxGeometryTotalOutputComponents = 1024;
	Resources.maxGeometryUniformComponents = 1024;
	Resources.maxGeometryVaryingComponents = 64;
	Resources.maxTessControlInputComponents = 128;
	Resources.maxTessControlOutputComponents = 128;
	Resources.maxTessControlTextureImageUnits = 16;
	Resources.maxTessControlUniformComponents = 1024;
	Resources.maxTessControlTotalOutputComponents = 4096;
	Resources.maxTessEvaluationInputComponents = 128;
	Resources.maxTessEvaluationOutputComponents = 128;
	Resources.maxTessEvaluationTextureImageUnits = 16;
	Resources.maxTessEvaluationUniformComponents = 1024;
	Resources.maxTessPatchComponents = 120;
	Resources.maxPatchVertices = 32;
	Resources.maxTessGenLevel = 64;
	Resources.maxViewports = 16;
	Resources.maxVertexAtomicCounters = 0;
	Resources.maxTessControlAtomicCounters = 0;
	Resources.maxTessEvaluationAtomicCounters = 0;
	Resources.maxGeometryAtomicCounters = 0;
	Resources.maxFragmentAtomicCounters = 8;
	Resources.maxCombinedAtomicCounters = 8;
	Resources.maxAtomicCounterBindings = 1;
	Resources.maxVertexAtomicCounterBuffers = 0;
	Resources.maxTessControlAtomicCounterBuffers = 0;
	Resources.maxTessEvaluationAtomicCounterBuffers = 0;
	Resources.maxGeometryAtomicCounterBuffers = 0;
	Resources.maxFragmentAtomicCounterBuffers = 1;
	Resources.maxCombinedAtomicCounterBuffers = 1;
	Resources.maxAtomicCounterBufferSize = 16384;
	Resources.maxTransformFeedbackBuffers = 4;
	Resources.maxTransformFeedbackInterleavedComponents = 64;
	Resources.maxCullDistances = 8;
	Resources.maxCombinedClipAndCullDistances = 8;
	Resources.maxSamples = 4;
	Resources.limits.nonInductiveForLoops = 1;
	Resources.limits.whileLoops = 1;
	Resources.limits.doWhileLoops = 1;
	Resources.limits.generalUniformIndexing = 1;
	Resources.limits.generalAttributeMatrixVectorIndexing = 1;
	Resources.limits.generalVaryingIndexing = 1;
	Resources.limits.generalSamplerIndexing = 1;
	Resources.limits.generalVariableIndexing = 1;
	Resources.limits.generalConstantMatrixVectorIndexing = 1;
}

int compile_program_vk(const char* outfilename, const char* infilename, vgpu_program_type_t type)
{
	char* shader_src = file_open_read_all_text(infilename, &allocator_default);
	const size_t shader_src_size = strlen(shader_src) + 1;

	glslang::InitializeProcess();

	glslang::TProgram program;
	TBuiltInResource resources;
	init_vk_resources(resources);

	EShMessages messages = (EShMessages)(EShMsgSpvRules | EShMsgVulkanRules);
	EShLanguage stage = translate_vk_shader_type[type];
	glslang::TShader shader(stage);

	const char *shader_strings[] =
	{
		program_defines_gl,
		shader_src,
	};
	shader.setStrings(shader_strings, ARRAY_LENGTH(shader_strings));

	if (!shader.parse(&resources, 100, false, messages)) {
		puts(shader.getInfoLog());
		puts(shader.getInfoDebugLog());
		return false; // something didn't work
	}

	program.addShader(&shader);

	//
	// Program-level processing...
	//

	if (!program.link(messages)) {
		puts(shader.getInfoLog());
		puts(shader.getInfoDebugLog());
		return false;
	}

	std::vector<unsigned int> spirv;
	glslang::GlslangToSpv(*program.getIntermediate(stage), spirv);

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if (outfile == NULL)
		return -1;

	file_write(outfile, spirv.data(), spirv.size() * sizeof(unsigned int));
	file_close(outfile);

	glslang::FinalizeProcess();

	ALLOCATOR_FREE(&allocator_default, shader_src);

	return 0;
}
#endif

#if defined(FAMILY_WINDOWS)
int compile_program_d3d(const char* outfilename, const char* infilename, vgpu_program_type_t type, vgpu_device_type_t device_type)
{
	char* shader_src = file_open_read_all_text(infilename, &allocator_default);
	const size_t shader_src_size = strlen(shader_src) + 1;

	// DX11 shader, concatenate with header and compile using the d3d compiler
    DWORD flags = D3DCOMPILE_ENABLE_STRICTNESS;
    flags |= D3DCOMPILE_DEBUG;

    ID3DBlob* error = NULL;
    ID3DBlob* shader = NULL;
	const char* entry = "main";
	const char* shader_model[MAX_VGPU_PROGRAM_TYPES] = {
		"vs_5_0",
		"ps_5_0",
	};
	D3D_SHADER_MACRO defines_dx12[] = {
		{ "DX12", "1" },
		{ NULL, NULL }
	};
	D3D_SHADER_MACRO defines_dx11[] = {
		{ "DX12", "1" },
		{ NULL, NULL }
	};
	D3D_SHADER_MACRO* defines = nullptr;
	switch(device_type)
	{
		case VGPU_DEVICE_DX11:
			defines = defines_dx11;
			break;
		case VGPU_DEVICE_DX12:
			defines = defines_dx12;
			break;
		default:
			BREAKPOINT();
	}
	size_t tmp_buf_size = sizeof(program_defines_dx11) + shader_src_size;
	char* tmp_buf = (char*)alloca(tmp_buf_size);
	memcpy(tmp_buf, program_defines_dx11, sizeof(program_defines_dx11));
	// TODO: add a line 0 preprocessor command
	memcpy(tmp_buf + sizeof(program_defines_dx11), shader_src, shader_src_size);
    HRESULT hr = D3DCompile(tmp_buf,
							tmp_buf_size,
							infilename,
							defines,
							NULL, // includes
							"main",
							shader_model[type],
							flags,
							0, // flags2
							&shader,
							&error);
    if(FAILED(hr))
    {
        if(error != NULL)
		{
			printf("HLSL compiler failed with message:\n%s", (char*)error->GetBufferPointer());
			error->Release();
		}
		else
		{
			printf("No Error :(\n");
		}
        return hr;
    }


	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	file_write(outfile, shader->GetBufferPointer(), shader->GetBufferSize());
	file_close(outfile);

	shader->Release();

	ALLOCATOR_FREE(&allocator_default, shader_src);

	return 0;
}
#endif

#if defined(PLATFORM_OSX)
int compile_program_mtl(const char* outfilename, const char* infilename, vgpu_program_type_t type)
{
	char* shader_src = file_open_read_all_text(infilename, &allocator_default);
	const size_t shader_src_size = strlen(shader_src) + 1;

	// TODO: implement this

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	file_write(outfile, shader_src, shader_src_size);
	file_close(outfile);

	ALLOCATOR_FREE(&allocator_default, shader_src);

	return 0;
}
#endif

void print_help(getopt_context_t* ctx)
{
	char buffer[2048];
	printf("usage: gpuc [options] file\n\n");
	printf("%s", getopt_create_help_string(ctx, buffer, sizeof(buffer)));
}

int main(int argc, const char** argv)
{
	static const getopt_option_t option_list[] =
	{
		{ "help",   'h', GETOPT_OPTION_TYPE_NO_ARG,   0x0, 'h', "displays this message", 0x0 },
		{ "output", 'o', GETOPT_OPTION_TYPE_REQUIRED, 0x0, 'o', "output to file", "file" },
		{ "type",   't', GETOPT_OPTION_TYPE_REQUIRED, 0x0, 't', "program type", "{vp, fp}" },
#if defined(FAMILY_WINDOWS)
		{ "device", 'd', GETOPT_OPTION_TYPE_REQUIRED, 0x0, 'd', "device type", "{gl, vk, dx11, dx12, null}" },
#elif defined(PLATFORM_OSX)
		{ "device", 'd', GETOPT_OPTION_TYPE_REQUIRED, 0x0, 'd', "device type", "{gl, mtl, null}" },
#endif
		GETOPT_OPTIONS_END
	};

	getopt_context_t go_ctx;
	getopt_create_context(&go_ctx, argc, argv, option_list);

	vgpu_program_type_t type = MAX_VGPU_PROGRAM_TYPES;
	vgpu_device_type_t device_type = MAX_VGPU_DEVICE_TYPES;
	const char* infilename = NULL;
	const char* outfilename = NULL;

	int opt;
	while( (opt = getopt_next( &go_ctx ) ) != -1 )
	{
		switch(opt)
		{
			case 'h': print_help(&go_ctx); return 0;
			case 'o':
				if(outfilename != NULL && outfilename[0] != '\0')
				{
					ERROR_AND_FAIL("output-file already set to: \"%s\", trying to set it to \"%s\"", outfilename, go_ctx.current_opt_arg);
				}

				outfilename = go_ctx.current_opt_arg;
				break;
			case 't':
				if(type != MAX_VGPU_PROGRAM_TYPES)
				{
					ERROR_AND_FAIL("type already set");
				}
				else if(strcmp(go_ctx.current_opt_arg, "vp") == 0)
					type = VGPU_VERTEX_PROGRAM;
				else if(strcmp(go_ctx.current_opt_arg, "fp") == 0)
					type = VGPU_FRAGMENT_PROGRAM;
				break;
			case 'd':
				if(device_type != MAX_VGPU_DEVICE_TYPES)
				{
					ERROR_AND_FAIL("device type already set");
				}
				else if(strcmp(go_ctx.current_opt_arg, "null") == 0)
					device_type = VGPU_DEVICE_NULL;
				else if(strcmp(go_ctx.current_opt_arg, "gl") == 0)
					device_type = VGPU_DEVICE_GL;
#if defined(FAMILY_WINDOWS)
				else if (strcmp(go_ctx.current_opt_arg, "vk") == 0)
					device_type = VGPU_DEVICE_VK;
				else if(strcmp(go_ctx.current_opt_arg, "dx11") == 0)
					device_type = VGPU_DEVICE_DX11;
				else if(strcmp(go_ctx.current_opt_arg, "dx12") == 0)
					device_type = VGPU_DEVICE_DX12;
#elif defined(PLATFORM_OSX)
				else if(strcmp(go_ctx.current_opt_arg, "mtl") == 0)
					device_type = VGPU_DEVICE_MTL;
#endif
				else
				{
					printf("unsupported device type %s\n", go_ctx.current_opt_arg);
					return -1;
				}
				break;
			case '+':
				if(infilename != NULL && infilename[0] != '\0')
				{
					ERROR_AND_FAIL("input-file already set to: \"%s\", trying to set it to \"%s\"", infilename, go_ctx.current_opt_arg);
				}

				infilename = go_ctx.current_opt_arg;
				break;
			case 0: break; // ignore, flag was set!
		}
	}

	if(device_type == MAX_VGPU_DEVICE_TYPES)
	{
#if defined(FAMILY_WINDOWS)
		printf("Please specify a device type using -d {dx11, dx12, gl, vk, null}\n");
#elif defined(PLATFORM_OSX)
		printf("Please specify a device type using -d {gl, mtl, null}\n");
#endif
		return -1;
	}

	if(type == MAX_VGPU_PROGRAM_TYPES)
	{
		printf("Please specify a program type using -t {vp, fp}\n");
		return -1;
	}

	int res = -1;
	switch(device_type)
	{
		case VGPU_DEVICE_NULL:
			res = compile_program_null(outfilename, infilename, type);
			break;
		case VGPU_DEVICE_GL:
			res = compile_program_gl(outfilename, infilename, type);
			break;
#if defined(FAMILY_WINDOWS)
		case VGPU_DEVICE_VK:
			res = compile_program_vk(outfilename, infilename, type);
			break;
		case VGPU_DEVICE_DX11:
		case VGPU_DEVICE_DX12:
			res = compile_program_d3d(outfilename, infilename, type, device_type);
			break;
#elif defined(PLATFORM_OSX)
		case VGPU_DEVICE_MTL:
			res = compile_program_mtl(outfilename, infilename, type);
			break;
#endif
		default:
			printf("Device type not supported on this platform\n");
			break;
	}
	return res;
}
