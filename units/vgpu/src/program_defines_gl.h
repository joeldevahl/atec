#version 430 core
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define float4x4 mat4
#define float2 vec2
#define float3 vec3
#define float4 vec4

#define mul(a, b) ((a) * (b))

float saturate(float f) { return clamp(f, 0.0f, 1.0f); }
float2 saturate(float2 f) { return clamp(f, 0.0f, 1.0f); }
float3 saturate(float3 f) { return clamp(f, 0.0f, 1.0f); }
float4 saturate(float4 f) { return clamp(f, 0.0f, 1.0f); }

float2 splat2(float f) { return float2(f, f); }
float3 splat3(float f) { return float3(f, f, f); }
float4 splat4(float f) { return float4(f, f, f, f); }

#define CONSTANT_BUFFER_BEGIN(name, loc) layout(std430, binding = loc) buffer name {
#define CONSTANT_BUFFER_END };

#define BUFFER(type, name, loc) layout(std430, binding = loc) buffer buffer_##name##loc { type name; };
