float2 splat2(float f) { return float2(f, f); }
float3 splat3(float f) { return float3(f, f, f); }
float4 splat4(float f) { return float4(f, f, f, f); }

#define CONSTANT_BUFFER_BEGIN(name, loc) cbuffer name : register( b##loc ) {
#define CONSTANT_BUFFER_END };

#define BUFFER(type, name, loc) StructuredBuffer<type> name : register( t##loc );
