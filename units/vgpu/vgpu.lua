Unit:Using("core")
Unit:Using("container")
Unit:Using("io") -- used by compiler, should not be transitive
Unit:Using("sexp") -- used by compiler, should not be transitive
Unit:Using("riff") -- used by compiler, should not be transitive
Unit:Using("getopt") -- used by compiler, should not be transitive
Unit:Using("txt2hex") -- used by internally during compile, should not be transitive

function Unit.GetDefaultVariant(self)
	local backend = "null"

	if target.platform == "osx" then
		backend = "gl"
		backend = "mtl"
	elseif target.family == "windows" then
		--backend = "gl"
		--backend = "dx11"
		backend = "dx12"
		--backend = "vk"
	end

	return backend
end

function Unit.GetDefaultLib(self, settings)
	local backend = self:GetDefaultVariant()

	if backend == "gl" then
		return "vgpu_gl" .. settings.config_ext
	elseif backend == "vk" then
		return "vgpu_vk" .. settings.config_ext
	elseif backend == "dx11" then
		return "vgpu_dx11" .. settings.config_ext
	elseif backend == "dx12" then
		return "vgpu_dx12" .. settings.config_ext
	else
		return "vgpu_null" .. settings.config_ext
	end
end

function Unit.Init(self)
	self:DefaultInit()

	self.ignore_application_lib = true -- hack to link vgpuprogc for now
end

function Unit.Patch(self, other_unit)
	DefaultApplyConfig(self, other_unit)
	DefaultPatchHeaders(self, other_unit)

	local backend = self:GetDefaultVariant()
	local libname = self:GetDefaultLib(other_unit.settings)
	other_unit.settings.link.libs:Add(libname)



	if backend == "gl" then
		other_unit.settings.cc.defines:Add("VGPU_GL")
		if target.platform == "osx"then
			other_unit.settings.link.frameworks:Add("OpenGL")
		elseif target.family == "windows" then
			other_unit.settings.link.libs:Add("opengl32")
			other_unit.settings.link.libs:Add("glu32")
			other_unit.settings.link.libs:Add("gdi32")
		end
	elseif backend == "vk" then
		other_unit.settings.cc.defines:Add("VGPU_VULKAN")
		other_unit.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\Source\\lib")
		other_unit.settings.link.libs:Add("vulkan-1")
	elseif backend == "dx11" then
		other_unit.settings.cc.defines:Add("VGPU_DX11")
		if target.family == "windows" then
			other_unit.settings.link.libs:Add("d3d11")
		end
	elseif backend == "dx12" then
		other_unit.settings.cc.defines:Add("VGPU_DX12")
		if target.family == "windows" then
			other_unit.settings.link.libs:Add("d3d12")
			other_unit.settings.link.libs:Add("dxguid")
			other_unit.settings.link.libs:Add("dxgi")
		end
	else
		other_unit.settings.cc.defines:Add("VGPU_NULL")
	end
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.vgpuprogc = {}
		settings.vgpuprogc.exe = GetHostBinary("vgpuprogc")
		if family == "windows" then
			settings.vgpuprogc.device_types = { "dx11", "dx12", "gl", "vk", "null" }
		else
			settings.vgpuprogc.device_types = { "gl", "null" }
		end

		local gen_compile_func = function(settings, var)
			settings.compile.mappings[var] = function (settings, infile)
				local results = {}
				for _, dt in ipairs(settings.vgpuprogc.device_types) do
					local outfile = PathJoin(target.outdir, PathBase(infile)) .. "_" .. dt .. "." .. var
					local vgpuprogc = settings.vgpuprogc
					local cmd = vgpuprogc.exe .. " -t " .. var .. " -d " .. dt .. " -o " .. outfile .. " " .. infile

					AddJob(outfile, var .. " " .. outfile, cmd, vgpuprogc.exe, infile)

					table.insert(results, outfile)
				end

				return results
			end
		end

		gen_compile_func(settings, "vp")
		gen_compile_func(settings, "fp")
	end)
end

function Unit.BuildTarget(self)
	Txt2Hex(self.settings, PathJoin(self.path, "src/program_defines_gl.h"))
	Txt2Hex(self.settings, PathJoin(self.path, "src/program_defines_dx11.h"))

	Txt2Hex(self.settings, Compile(self.settings, Collect(PathJoin(self.path, "*.vp"))))
	Txt2Hex(self.settings, Compile(self.settings, Collect(PathJoin(self.path, "*.fp"))))
end

function Unit.Build(self)
	local api_common_src = self.path .. "/src/vgpu.cpp"
	local api_common_obj = Compile(self.settings, api_common_src)

	local api_null_src = self.path .. "/src/vgpu_null.cpp"
	local api_null_obj = Compile(self.settings, api_null_src)
	local api_null_bin = StaticLibrary(self.settings, "vgpu_null", api_null_obj, api_common_obj)
	self:AddProduct(api_null_bin)

	if target.family == "windows" then
		local api_dx11_src = self.path .. "/src/vgpu_dx11.cpp"
		local api_dx11_obj = Compile(self.settings, api_dx11_src)
		local api_dx11_bin = StaticLibrary(self.settings, "vgpu_dx11", api_dx11_obj, api_common_obj)
		self:AddProduct(api_dx11_bin)

		local api_dx12_src = self.path .. "/src/vgpu_dx12.cpp"
		local api_dx12_obj = Compile(self.settings, api_dx12_src)
		local api_dx12_bin = StaticLibrary(self.settings, "vgpu_dx12", api_dx12_obj, api_common_obj)
		self:AddProduct(api_dx12_bin)
	end

	local api_gl_src = self.path .. "/src/vgpu_gl.cpp"
	local api_gl_src_platform = ""
	if target.family == "windows" then
		api_gl_src_platform = self.path .. "/src/vgpu_gl_win.cpp"
	else
		api_gl_src_platform = self.path .. "/src/vgpu_gl_osx.m"
	end
	local api_gl_obj = Compile(self.settings, api_gl_src, api_gl_src_platform)
	local api_gl_bin = StaticLibrary(self.settings, "vgpu_gl", api_gl_obj, api_common_obj)
	self:AddProduct(api_gl_bin)

	if target.family == "windows" then
		self.settings.cc.includes:Add("C:\\VulkanSDK\\1.0.13.0\\Include")
		local api_vk_src = self.path .. "/src/vgpu_vk.cpp"
		local api_vk_obj = Compile(self.settings, api_vk_src)
		local api_vk_bin = StaticLibrary(self.settings, "vgpu_vk", api_vk_obj, api_common_obj)
		self:AddProduct(api_vk_bin)
	end

	if target.platform == "osx" then
		local api_mtl_src = self.path .. "/src/vgpu_mtl.m"
		local api_mtl_obj = Compile(self.settings, api_mtl_src)
		local api_mtl_bin = StaticLibrary(self.settings, "vgpu_mtl", api_mtl_obj, api_common_obj)
		self:AddProduct(api_mtl_bin)
	end

	if target.family == "windows" then
		self.settings.cc.includes:Add("C:\\VulkanSDK\\1.0.13.0\\glslang")
		self.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\glslang\\x64\\Debug")
		self.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\glslang\\glslang\\Debug")
		self.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\glslang\\glslang\\OSDependent\\Windows\\Debug")
		self.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\glslang\\OGLCompilersDLL\\Debug")
		self.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\glslang\\SPIRV\\Debug")
		self.settings.link.libpath:Add("C:\\VulkanSDK\\1.0.13.0\\glslang\\HLSL\\Debug")
		self.settings.link.libs:Add("glslang")
		self.settings.link.libs:Add("OGLCompiler")
		self.settings.link.libs:Add("OSDependent")
		self.settings.link.libs:Add("SPIRV")
		self.settings.link.libs:Add("HLSL")
		self.settings.link.libs:Add("d3dcompiler")
		self.settings.link.libs:Add("dxguid")
	end
	local vgpuprogc_obj = Compile(self.settings, PathJoin(self.path, "src/vgpuprogc.cpp"))
	local vgpuprogc = Link(self.settings, "vgpuprogc", vgpuprogc_obj)
	self:AddProduct(vgpuprogc)
end


