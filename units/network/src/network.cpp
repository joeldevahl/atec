#include <core/assert.h>

#include <network/network.h>
#include <memory/memory.h>

#ifdef FAMILY_WINDOWS

#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0600
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#define ssize_t SSIZE_T
#define TOSIZE(x) (int)(x)

void network_init()
{
	WSADATA wsa_data;
	int err = WSAStartup(MAKEWORD(1, 1), &wsa_data);
	ASSERT(err == 0, "network initialization failed.");
}

extern "C" void network_shutdown()
{
	WSACleanup();
}

#else

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <fcntl.h>

#define TOSIZE(x) (x)

void network_init()
{
}

#endif

#define AS_PTR(i) ((network_socket_t*)((uintptr_t)i))
#define FROM_PTR(p) ((int)((uintptr_t)p))

static void netaddr_to_sockaddr(const network_address_t* src, struct sockaddr_in *dest)
{
	memory_zero(dest, sizeof(*dest));
	dest->sin_family = AF_INET;
	dest->sin_port = htons(src->port);
	dest->sin_addr.s_addr = htonl(src->ip[0]<<24 | src->ip[1]<<16 | src->ip[2]<<8 | src->ip[3]);
}

static void sockaddr_to_netaddr(const struct sockaddr_in *src, network_address_t* dst)
{
	unsigned int ip = htonl(src->sin_addr.s_addr);
	memory_zero(dst, sizeof(network_address_t));
	dst->port = htons(src->sin_port);
	dst->ip[0] = (uint8_t)((ip>>24) & 0xFF);
	dst->ip[1] = (uint8_t)((ip>>16) & 0xFF);
	dst->ip[2] = (uint8_t)((ip>>8) & 0xFF);
	dst->ip[3] = (uint8_t)(ip & 0xFF);
}

network_result_t network_host_lookup(network_address_t* address, const char *hostname)
{
	addrinfo hints;
	memory_zero(&hints, sizeof(hints));
	hints.ai_family = AF_INET;

	addrinfo *result;
	int e = getaddrinfo(hostname, 0x0, &hints, &result);
	if(e != 0 || !result)
		return NETWORK_RESULT_GENERAL_ERROR;

	sockaddr_to_netaddr((const sockaddr_in*)result->ai_addr, address);
	freeaddrinfo(result);
	address->port = 0;

	return NETWORK_RESULT_OK;
}

network_socket_t* network_udp_create(const network_address_t* bind_address)
{
	int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sock < 0)
		return NULL;

	sockaddr_in addr;
	netaddr_to_sockaddr(bind_address, &addr);

	int res = bind(sock, (const sockaddr*)&addr, sizeof(addr));
	if(res < 0)
	{
		network_udp_destroy(AS_PTR(sock));
		return NULL;
	}

	unsigned long mode = 1;
#if defined(FAMILY_WINDOWS)
	ioctlsocket(sock, FIONBIO, (unsigned long *)&mode);
#else
	fcntl(sock, F_SETFL, O_NONBLOCK, &mode);
#endif

	int broadcast = 1;
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (const char*)&broadcast, sizeof(broadcast));

	return AS_PTR(sock);
}

void network_udp_destroy(network_socket_t* sock)
{
#if defined(FAMILY_WINDOWS)
	closesocket(FROM_PTR(sock));
#else
	close(FROM_PTR(sock));
#endif
}

network_result_t network_udp_send(network_socket_t* sock, const network_address_t* address, const void* data, size_t num_bytes, size_t *num_bytes_sent)
{
	sockaddr_in sa;
	memory_zero(&sa, sizeof(sa));
	netaddr_to_sockaddr(address, &sa);
	ssize_t d = sendto(FROM_PTR(sock), (const char*)data, TOSIZE(num_bytes), 0, (sockaddr*)&sa, (socklen_t)sizeof(sa));
	if(d > 0)
	{
		*num_bytes_sent = d;
		return NETWORK_RESULT_OK;
	}
	return NETWORK_RESULT_GENERAL_ERROR;
}

network_result_t network_udp_receive(network_socket_t* sock, network_address_t* address, void* data, size_t max_bytes, size_t *num_bytes_recv)
{
	sockaddr_in from;
	socklen_t fromlen = sizeof(from);
	ssize_t bytes = recvfrom(FROM_PTR(sock), (char*)data, TOSIZE(max_bytes), 0, (sockaddr*)&from, &fromlen);
	if(bytes > 0)
	{
		sockaddr_to_netaddr(&from, address);
		*num_bytes_recv = bytes;
		return NETWORK_RESULT_OK;
	}
	else if(bytes == 0)
	{
		*num_bytes_recv = bytes;
		return NETWORK_RESULT_OK;
	}

	return NETWORK_RESULT_GENERAL_ERROR;
}

network_socket_t* network_tcp_create(const network_address_t* address)
{
    int sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock < 0)
        return NULL;

    sockaddr_in addr;
    netaddr_to_sockaddr(address, &addr);
    bind(sock, (sockaddr*)&addr, sizeof(addr));

    return AS_PTR(sock);
}

void network_tcp_destroy(network_socket_t* sock)
{
#if defined(FAMILY_WINDOWS)
	closesocket(FROM_PTR(sock));
#else
	close(FROM_PTR(sock));
#endif
}

void network_tcp_set_non_blocking(network_socket_t* sock)
{
	unsigned long mode = 1;
#if defined(FAMILY_WINDOWS)
	ioctlsocket(FROM_PTR(sock), FIONBIO, &mode);
#else
	ioctl(FROM_PTR(sock), FIONBIO, &mode);
#endif
}

void network_tcp_set_blocking(network_socket_t* sock)
{
	unsigned long mode = 0;
#if defined(FAMILY_WINDOWS)
	ioctlsocket(FROM_PTR(sock), FIONBIO, &mode);
#else
	ioctl(FROM_PTR(sock), FIONBIO, &mode);
#endif
}

void network_tcp_listen(network_socket_t* sock, int backlog)
{
	listen(FROM_PTR(sock), backlog);
}

network_result_t network_tcp_accept(network_socket_t* listen_sock, network_socket_t* *dst_sock, network_address_t* address)
{
	sockaddr_in addr;
	socklen_t sockaddr_len = sizeof(addr);
	int s = accept(FROM_PTR(listen_sock), (sockaddr*)&addr, &sockaddr_len);

	if (s >= 0)
	{
		sockaddr_to_netaddr(&addr, address);
		*dst_sock = AS_PTR(s);
		return NETWORK_RESULT_OK;
	}

	return NETWORK_RESULT_GENERAL_ERROR;
}

network_result_t network_tcp_connect(network_socket_t* sock, const network_address_t* address)
{
	sockaddr_in addr;
	netaddr_to_sockaddr(address, &addr);
	int res = connect(FROM_PTR(sock), (sockaddr*)&addr, sizeof(addr));
	return res == 0 ? NETWORK_RESULT_OK : NETWORK_RESULT_GENERAL_ERROR;
}

network_result_t network_tcp_connect_non_blocking(network_socket_t* sock, const network_address_t* address)
{
	sockaddr_in addr;
	netaddr_to_sockaddr(address, &addr);
	network_tcp_set_non_blocking(sock);
	int res = connect(FROM_PTR(sock), (sockaddr*)&addr, sizeof(addr));
	network_tcp_set_blocking(sock);

	return res == 0 ? NETWORK_RESULT_OK : NETWORK_RESULT_GENERAL_ERROR;
}

network_result_t network_tcp_send(network_socket_t* sock, const void *data, size_t num_bytes, size_t *num_bytes_sent)
{
	ssize_t n = send(FROM_PTR(sock), (const char*)data, TOSIZE(num_bytes), 0);
	if(n >= 0)
	{
		*num_bytes_sent = n;
		return NETWORK_RESULT_OK;
	}

	return NETWORK_RESULT_GENERAL_ERROR;
}

network_result_t network_tcp_receive(network_socket_t* sock, void *data, size_t max_bytes, size_t *num_bytes_recv)
{
	int n = recv(FROM_PTR(sock), (char*)data, TOSIZE(max_bytes), 0);

	if(n == 0)
	{
		*num_bytes_recv = 0;
		return NETWORK_RESULT_CONNECTION_LOST;
	}
	else if(n != -1)
	{
		*num_bytes_recv = n;
		return NETWORK_RESULT_OK;
	}

	return NETWORK_RESULT_GENERAL_ERROR;
}
