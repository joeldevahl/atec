#ifndef NETWORK_NETWORK_H
#define NETWORK_NETWORK_H

#include <core/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	NETWORK_RESULT_OK = 0,
	NETWORK_RESULT_CONNECTION_LOST,
	NETWORK_RESULT_GENERAL_ERROR,
} network_result_t;

typedef struct network_address_s
{
	uint8_t ip[4];
	uint16_t port;
} network_address_t;

typedef struct network_socket_s network_socket_t;

void network_init();
void network_shutdown();

network_result_t network_host_lookup(network_address_t* address, const char *hostname);

network_socket_t* network_udp_create(const network_address_t* bind_address);
void network_udp_destroy(network_socket_t* sock);
network_result_t network_udp_send(network_socket_t* sock, const network_address_t* address, const void* data, size_t num_bytes, size_t *num_bytes_sent);
network_result_t network_udp_receive(network_socket_t* sock, network_address_t* address, void* data, size_t max_bytes, size_t *num_bytes_recv);

network_socket_t* network_tcp_create(const network_address_t* address);
void network_tcp_destroy(network_socket_t* sock);
void network_tcp_set_non_blocking(network_socket_t* sock);
void network_tcp_set_blocking(network_socket_t* sock);
void network_tcp_listen(network_socket_t* sock, int32_t backlog);
network_result_t network_tcp_accept(network_socket_t* listen_sock, network_socket_t** dst_sock, network_address_t* address);
network_result_t network_tcp_connect(network_socket_t* sock, const network_address_t* address);
network_result_t network_tcp_connect_non_blocking(network_socket_t* sock, const network_address_t* address);
network_result_t network_tcp_send(network_socket_t* sock, const void *data, size_t num_bytes, size_t *num_bytes_sent);
network_result_t network_tcp_receive(network_socket_t* sock, void *data, size_t max_bytes, size_t *num_bytes_recv);

#ifdef __cplusplus
}
#endif

#endif // NETWORK_NETWORK_H
