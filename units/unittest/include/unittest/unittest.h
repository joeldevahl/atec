#ifndef UNITTEST_UNITTEST_H
#define UNITTEST_UNITTEST_H

#include <core/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	UNITTEST_TYPE_FATAL = 0,
	UNITTEST_TYPE_NONFATAL
} unittest_type_t;

#define UNITTEST(domain, name) \
void _unittest_ ## domain ## _ ## name ()

void unittest_test(const char* file, int line, unittest_type_t type, int result);

#define TEST(exp) unittest_test(__FILE__, __LINE__, UNITTEST_TYPE_FATAL, exp);

#ifdef __cplusplus
}
#endif

#endif // UNITTEST_UNITTEST_H
