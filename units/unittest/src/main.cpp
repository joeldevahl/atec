#include <setjmp.h>
#include <stdio.h>
#include <string.h>

#include <core/defines.h>
#include <unittest/unittest.h>
#undef UNITTEST

#define UNITTEST(domain, name) \
void _unittest_ ## domain ## _ ## name ();
#include "testdef.h"
#undef UNITTEST

enum unittest_status
{
	UNITTEST_STATUS_NONE = 0,
	UNITTEST_STATUS_SUCCEDED,
	UNITTEST_STATUS_FAILED,
	UNITTEST_STATUS_FAILED_HORRIBLY,
};

struct unittest
{
	const char* domain;
	const char* name;
	enum unittest_status status;
	void (*func)();
	jmp_buf jmp;
};

static struct unittest* g_unittest_current = NULL;

void unittest_test(const char* file, int line, unittest_type_t type, int result)
{
	unittest* ctx = g_unittest_current;
	if(result == 0)
	{
		printf("\ntest failed at %s:(%d)\n", file, line);
		ctx->status = UNITTEST_STATUS_FAILED;
		if(type == UNITTEST_TYPE_FATAL)
			longjmp(ctx->jmp, 1);
	}
	else
	{
		printf(".");
		ctx->status = UNITTEST_STATUS_SUCCEDED;
	}
}

#define UNITTEST(domain, name) \
void _unittest_ ## domain ## _ ## name ();
#include "testdef.h"
#undef UNITTEST

int main(int argc, char **argv)
{
#define UNITTEST(domain, name) \
{ #domain, #name, UNITTEST_STATUS_NONE, _unittest_ ## domain ## _ ## name },
	static struct unittest unittest_info[] =
	{
		#include "testdef.h"
	};
#undef UNITTEST

	const char* last_domain = NULL;

	printf("running unittests:\n");

	for(size_t i = 0; i < ARRAY_LENGTH(unittest_info); ++i)
	{
		struct unittest* test = unittest_info + i;
		g_unittest_current = test;
		if(last_domain == NULL || strcmp(last_domain, test->domain) == 0)
		{
			if(last_domain != 0)
				printf("\n");
			printf("\t%s:\n", test->domain);
			last_domain = test->domain;
		}
			printf("\t\t%s: ", test->name);

		if(setjmp(test->jmp) == 0)
		{
			test->func();
		}
		else
		{
			return 1;
		}
	}

	printf("\n");

	return 0;
}
