Import("build.lua")

Init()
AddATECDefault()
engine.build_tests = true
AddUnitsInDir(PathJoin(engine.path, "units"))
AddUnitsInDir(PathJoin(engine.path, "externals"))
Build()
